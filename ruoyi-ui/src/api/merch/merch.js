import request from '@/utils/request'

// 查询商户列表列表
export function listMerch(query) {
  return request({
    url: '/merch/merch/list',
    method: 'get',
    params: query
  })
}

// 查询商户列表详细
export function getMerch(mchNo) {
  return request({
    url: '/merch/merch/' + mchNo,
    method: 'get'
  })
}

// 新增商户列表
export function addMerch(data) {
  return request({
    url: '/merch/merch',
    method: 'post',
    data: data
  })
}

// 修改商户列表
export function updateMerch(data) {
  return request({
    url: '/merch/merch',
    method: 'put',
    data: data
  })
}

// 删除商户列表
export function delMerch(mchNo) {
  return request({
    url: '/merch/merch/' + mchNo,
    method: 'delete'
  })
}
