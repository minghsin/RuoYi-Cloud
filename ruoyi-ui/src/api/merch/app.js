import request from '@/utils/request'

// 查询商户应用列表
export function listApp(query) {
  return request({
    url: '/merch/app/list',
    method: 'get',
    params: query
  })
}

// 查询商户应用详细
export function getApp(appId) {
  return request({
    url: '/merch/app/' + appId,
    method: 'get'
  })
}

// 新增商户应用
export function addApp(data) {
  return request({
    url: '/merch/app',
    method: 'post',
    data: data
  })
}

// 修改商户应用
export function updateApp(data) {
  return request({
    url: '/merch/app',
    method: 'put',
    data: data
  })
}

// 删除商户应用
export function delApp(appId) {
  return request({
    url: '/merch/app/' + appId,
    method: 'delete'
  })
}
