import request from '@/utils/request'

// 查询支付类型列表
export function listPayType(query) {
  return request({
    url: '/pay/payType/list',
    method: 'get',
    params: query
  })
}

// 查询支付类型详细
export function getPayType(wayType) {
  return request({
    url: '/pay/payType/' + wayType,
    method: 'get'
  })
}

// 新增支付类型
export function addPayType(data) {
  return request({
    url: '/pay/payType',
    method: 'post',
    data: data
  })
}

// 修改支付类型
export function updatePayType(data) {
  return request({
    url: '/pay/payType',
    method: 'put',
    data: data
  })
}

// 删除支付类型
export function delPayType(wayType) {
  return request({
    url: '/pay/payType/' + wayType,
    method: 'delete'
  })
}
