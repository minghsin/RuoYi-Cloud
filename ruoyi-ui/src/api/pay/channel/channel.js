import request from '@/utils/request'

// 修改渠道支付方式
export function updateChannelWay(data) {
  return request({
    url: '/pay/config/way',
    method: 'put',
    data: data
  })
}

// 新增渠道支付方式
export function addChannelWay(data) {
  return request({
    url: '/pay/config/way',
    method: 'post',
    data: data
  })
}

// 查询渠道支付方式列表
export function listChannelWay(query) {
  return request({
    url: '/pay/config/way/list',
    method: 'get',
    params: query
  })
}

// 渠道基础配置
export function settleBaseChannel(data) {
  return request({
    url: '/pay/config/base/settle' ,
    method: 'post',
    data: data
  })
}

// 查询渠道信息列表
export function queryConfigByChnlId(query) {
  return request({
    url: '/pay/config/base/info/' + query ,
    method: 'get'
  })
}

// 查询渠道信息列表
export function listChannel(query) {
  return request({
    url: '/pay/channel/list',
    method: 'get',
    params: query
  })
}

// 查询渠道信息详细
export function getChannel(id) {
  return request({
    url: '/pay/channel/' + id,
    method: 'get'
  })
}

// 新增渠道信息
export function addChannel(data) {
  return request({
    url: '/pay/channel',
    method: 'post',
    data: data
  })
}

// 修改渠道信息
export function updateChannel(data) {
  return request({
    url: '/pay/channel',
    method: 'put',
    data: data
  })
}

// 删除渠道信息
export function delChannel(id) {
  return request({
    url: '/pay/channel/' + id,
    method: 'delete'
  })
}
