import request from '@/utils/request'

// 查询账户记录历史列表
export function listHistory(query) {
  return request({
    url: '/pay/history/list',
    method: 'get',
    params: query
  })
}

// 查询账户记录历史详细
export function getHistory(id) {
  return request({
    url: '/pay/history/' + id,
    method: 'get'
  })
}

// 新增账户记录历史
export function addHistory(data) {
  return request({
    url: '/pay/history',
    method: 'post',
    data: data
  })
}

// 修改账户记录历史
export function updateHistory(data) {
  return request({
    url: '/pay/history',
    method: 'put',
    data: data
  })
}

// 删除账户记录历史
export function delHistory(id) {
  return request({
    url: '/pay/history/' + id,
    method: 'delete'
  })
}
