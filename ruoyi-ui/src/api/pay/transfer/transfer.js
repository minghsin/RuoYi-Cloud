import request from '@/utils/request'

// 申请转账订单审核通过
export function transferPass(data) {
  return request({
    url: '/pay/transfer/audit/pass',
    method: 'put',
    data: data
  })
}

// 申请转账订单审核拒绝
export function transferRefusal(data) {
  return request({
    url: '/pay/transfer/audit/refusal',
    method: 'put',
    data: data
  })
}

// 申请转账订单
export function addMchTransfer(data) {
  return request({
    url: '/pay/transfer/addMchTransfer',
    method: 'put',
    data: data
  })
}

// 查询转账订单列表
export function listTransfer(query) {
  return request({
    url: '/pay/transfer/list',
    method: 'get',
    params: query
  })
}

// 查询转账订单详细
export function getTransfer(transferId) {
  return request({
    url: '/pay/transfer/' + transferId,
    method: 'get'
  })
}

// 新增转账订单
export function addTransfer(data) {
  return request({
    url: '/pay/transfer',
    method: 'post',
    data: data
  })
}

// 修改转账订单
export function updateTransfer(data) {
  return request({
    url: '/pay/transfer',
    method: 'put',
    data: data
  })
}

// 删除转账订单
export function delTransfer(transferId) {
  return request({
    url: '/pay/transfer/' + transferId,
    method: 'delete'
  })
}
