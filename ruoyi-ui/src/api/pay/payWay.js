import request from '@/utils/request'

// 查询支付方式列表
export function listPayWay(query) {
  return request({
    url: '/pay/payWay/list',
    method: 'get',
    params: query
  })
}

// 查询支付方式详细
export function getPayWay(wayCode) {
  return request({
    url: '/pay/payWay/' + wayCode,
    method: 'get'
  })
}

// 新增支付方式
export function addPayWay(data) {
  return request({
    url: '/pay/payWay',
    method: 'post',
    data: data
  })
}

// 修改支付方式
export function updatePayWay(data) {
  return request({
    url: '/pay/payWay',
    method: 'put',
    data: data
  })
}

// 删除支付方式
export function delPayWay(wayCode) {
  return request({
    url: '/pay/payWay/' + wayCode,
    method: 'delete'
  })
}
