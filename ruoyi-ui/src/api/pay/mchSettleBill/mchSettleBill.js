import request from '@/utils/request'

// 生成商户对账单
export function genMchSettleBill(data) {
  return request({
    url: '/pay/mchSettleBill/gen/date',
    method: 'get',
    params: data
  })
}

// 查询商户对账单列表
export function listMchSettleBill(query) {
  return request({
    url: '/pay/mchSettleBill/list',
    method: 'get',
    params: query
  })
}

// 查询商户对账单详细
export function getMchSettleBill(id) {
  return request({
    url: '/pay/mchSettleBill/' + id,
    method: 'get'
  })
}

// 新增商户对账单
export function addMchSettleBill(data) {
  return request({
    url: '/pay/mchSettleBill',
    method: 'post',
    data: data
  })
}

// 修改商户对账单
export function updateMchSettleBill(data) {
  return request({
    url: '/pay/mchSettleBill',
    method: 'put',
    data: data
  })
}

// 删除商户对账单
export function delMchSettleBill(id) {
  return request({
    url: '/pay/mchSettleBill/' + id,
    method: 'delete'
  })
}
