import request from '@/utils/request'

// 查询商户支付费率列表
export function listRate(query) {
  return request({
    url: '/pay/rate/list',
    method: 'get',
    params: query
  })
}


// 查询商户支付费率列表
export function listRateByMchNo(mchNo) {
  return request({
    url: '/pay/rate/mch/' + mchNo,
    method: 'get'
  })
}

// 新增商户支付费率
export function saveMchRate(data) {
  return request({
    url: '/pay/rate/saveMchRate',
    method: 'post',
    data: data
  })
}

// 查询商户支付费率详细
export function getRate(id) {
  return request({
    url: '/pay/rate/' + id,
    method: 'get'
  })
}

// 新增商户支付费率
export function addRate(data) {
  return request({
    url: '/pay/rate',
    method: 'post',
    data: data
  })
}

// 修改商户支付费率
export function updateRate(data) {
  return request({
    url: '/pay/rate',
    method: 'put',
    data: data
  })
}

// 删除商户支付费率
export function delRate(id) {
  return request({
    url: '/pay/rate/' + id,
    method: 'delete'
  })
}
