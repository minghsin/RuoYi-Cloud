import request from '@/utils/request'


// 通知商户
export function notifyMerchant(data) {
  return request({
    url: '/pay/order/notifyMerch/' + data.payOrderId,
    method: 'get'
  })
}

// 查询支付订单列表
export function manualReplenishment(data) {
  return request({
    url: '/pay/order/manualReplenishment',
    method: 'post',
    data: data
  })
}

// 查询支付订单列表
export function statisticOrders(query) {
  return request({
    url: '/pay/order/statistic',
    method: 'get',
    params: query
  })
}

// 查询支付订单列表
export function listOrder(query) {
  return request({
    url: '/pay/order/list',
    method: 'get',
    params: query
  })
}

// 查询支付订单详细
export function getOrder(payOrderId) {
  return request({
    url: '/pay/order/' + payOrderId,
    method: 'get'
  })
}

// 新增支付订单
export function addOrder(data) {
  return request({
    url: '/pay/order',
    method: 'post',
    data: data
  })
}

// 修改支付订单
export function updateOrder(data) {
  return request({
    url: '/pay/order',
    method: 'put',
    data: data
  })
}

// 删除支付订单
export function delOrder(payOrderId) {
  return request({
    url: '/pay/order/' + payOrderId,
    method: 'delete'
  })
}
