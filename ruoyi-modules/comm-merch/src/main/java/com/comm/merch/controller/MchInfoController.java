package com.comm.merch.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.comm.merch.domain.MchInfo;
import com.ruoyi.common.security.annotation.InnerAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.merch.service.IMchInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 商户列表Controller
 * 
 * @author test
 * @date 2022-05-29
 */
@RestController
@RequestMapping("/merch")
public class MchInfoController extends BaseController
{
    @Autowired
    private IMchInfoService mchInfoService;


    /**
     * 查询商户列表列表
     */
    @RequiresPermissions("merch:merch:list")
    @GetMapping("/list")
    public TableDataInfo list(MchInfo mchInfo)
    {
        startPage();
        List<MchInfo> list = mchInfoService.selectMchInfoList(mchInfo);
        return getDataTable(list);
    }

    /**
     * 导出商户列表列表
     */
    @RequiresPermissions("merch:merch:export")
    @Log(title = "商户列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchInfo mchInfo)
    {
        List<MchInfo> list = mchInfoService.selectMchInfoList(mchInfo);
        ExcelUtil<MchInfo> util = new ExcelUtil<MchInfo>(MchInfo.class);
        util.exportExcel(response, list, "商户列表数据");
    }

    /**
     * 获取商户列表详细信息
     */
    @InnerAuth
    @RequiresPermissions("merch:merch:query")
    @GetMapping(value = "/{mchNo}")
    public AjaxResult getInfo(@PathVariable("mchNo") String mchNo)
    {
        return AjaxResult.success(mchInfoService.selectMchInfoByMchNo(mchNo));
    }

    /**
     * 获取商户列表详细信息
     */
    @InnerAuth
    @GetMapping(value = "/base/{mchNo}")
    public AjaxResult getMchByNo(@PathVariable("mchNo") String mchNo)
    {
        return AjaxResult.success(mchInfoService.selectMchInfoByMchNo(mchNo));
    }

    /**
     * 新增商户列表
     */
    @RequiresPermissions("merch:merch:add")
    @Log(title = "商户列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchInfo mchInfo)
    {
        return toAjax(mchInfoService.insertMchInfo(mchInfo));
    }

    /**
     * 修改商户列表
     */
    @RequiresPermissions("merch:merch:edit")
    @Log(title = "商户列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchInfo mchInfo)
    {
        return toAjax(mchInfoService.updateMchInfo(mchInfo));
    }

    /**
     * 删除商户列表
     */
    @RequiresPermissions("merch:merch:remove")
    @Log(title = "商户列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{mchNos}")
    public AjaxResult remove(@PathVariable String[] mchNos)
    {
        return toAjax(mchInfoService.deleteMchInfoByMchNos(mchNos));
    }
}
