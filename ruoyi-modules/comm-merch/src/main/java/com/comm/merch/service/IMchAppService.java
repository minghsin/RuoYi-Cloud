package com.comm.merch.service;

import java.util.List;
import com.comm.merch.domain.MchApp;

/**
 * 商户应用Service接口
 * 
 * @author ruoyi
 * @date 2022-05-30
 */
public interface IMchAppService 
{
    /**
     * 查询商户应用
     * 
     * @param appId 商户应用主键
     * @return 商户应用
     */
    public MchApp selectMchAppByAppId(String appId);

    /**
     * 查询商户应用列表
     * 
     * @param mchApp 商户应用
     * @return 商户应用集合
     */
    public List<MchApp> selectMchAppList(MchApp mchApp);

    /**
     * 新增商户应用
     * 
     * @param mchApp 商户应用
     * @return 结果
     */
    public int insertMchApp(MchApp mchApp);

    /**
     * 修改商户应用
     * 
     * @param mchApp 商户应用
     * @return 结果
     */
    public int updateMchApp(MchApp mchApp);

    /**
     * 批量删除商户应用
     * 
     * @param appIds 需要删除的商户应用主键集合
     * @return 结果
     */
    public int deleteMchAppByAppIds(String[] appIds);

    /**
     * 删除商户应用信息
     * 
     * @param appId 商户应用主键
     * @return 结果
     */
    public int deleteMchAppByAppId(String appId);
}
