package com.comm.merch.controller;

import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.merch.domain.MchApp;
import com.comm.merch.service.IMchAppService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 商户应用Controller
 * 
 * @author ruoyi
 * @date 2022-05-30
 */
@RestController
@RequestMapping("/app")
public class MchAppController extends BaseController
{
    @Autowired
    private IMchAppService mchAppService;

    /**
     * 查询商户应用列表
     */
    @RequiresPermissions("merch:app:list")
    @GetMapping("/list")
    public TableDataInfo list(MchApp mchApp)
    {
        startPage();
        List<MchApp> list = mchAppService.selectMchAppList(mchApp);
        return getDataTable(list);
    }

    /**
     * 导出商户应用列表
     */
    @RequiresPermissions("merch:app:export")
    @Log(title = "商户应用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MchApp mchApp)
    {
        List<MchApp> list = mchAppService.selectMchAppList(mchApp);
        ExcelUtil<MchApp> util = new ExcelUtil<MchApp>(MchApp.class);
        util.exportExcel(response, list, "商户应用数据");
    }

    /**
     * 获取商户应用详细信息
     */
    @RequiresPermissions("merch:app:query")
    @GetMapping(value = "/{appId}")
    public AjaxResult getInfo(@PathVariable("appId") String appId)
    {
        return AjaxResult.success(mchAppService.selectMchAppByAppId(appId));
    }

    /**
     * 获取商户应用详细信息
     */
    @InnerAuth
    @GetMapping(value = "/innerAuth/{appId}")
    public AjaxResult getMerchAppByAppid(@PathVariable("appId") String appId)
    {
        return AjaxResult.success(mchAppService.selectMchAppByAppId(appId));
    }

    /**
     * 新增商户应用
     */
    @RequiresPermissions("merch:app:add")
    @Log(title = "商户应用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MchApp mchApp)
    {
        mchApp.setAppId(RandomUtil.randomString(25));
        mchApp.setCreatedBy(SecurityUtils.getUsername());
        mchApp.setCreatedUid(SecurityUtils.getUserId());

        return toAjax(mchAppService.insertMchApp(mchApp));
    }

    /**
     * 修改商户应用
     */
    @RequiresPermissions("merch:app:edit")
    @Log(title = "商户应用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MchApp mchApp)
    {
        return toAjax(mchAppService.updateMchApp(mchApp));
    }

    /**
     * 删除商户应用
     */
    @RequiresPermissions("merch:app:remove")
    @Log(title = "商户应用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appIds}")
    public AjaxResult remove(@PathVariable String[] appIds)
    {
        return toAjax(mchAppService.deleteMchAppByAppIds(appIds));
    }
}
