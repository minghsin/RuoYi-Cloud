package com.comm.merch.service.impl;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.system.UserInfo;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.comm.merch.constant.MerchantConstant;
import com.comm.merch.domain.MchApp;
import com.comm.merch.domain.MchInfo;
import com.comm.merch.service.IMchAppService;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.UserConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.enums.UserStatus;
import com.ruoyi.common.datascope.annotation.DataScope;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.annotation.InnerAuth;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysDept;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.factory.RemoteFileFallbackFactory;
import com.ruoyi.system.api.model.LoginUser;
import com.sun.org.apache.xpath.internal.operations.Bool;
import io.seata.spring.annotation.GlobalTransactional;
import org.bouncycastle.pqc.math.linearalgebra.RandUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.merch.mapper.MchInfoMapper;
import com.comm.merch.service.IMchInfoService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商户列表Service业务层处理
 * 
 * @author test
 * @date 2022-05-29
 */
@Service
public class MchInfoServiceImpl implements IMchInfoService 
{
    private static final Logger log = LoggerFactory.getLogger(MchInfoServiceImpl.class);
    @Autowired
    private MchInfoMapper mchInfoMapper;

    @Autowired
    private IMchAppService mchAppService;

    @Autowired
    private RemoteUserService remoteUserService;

    @Autowired
    private RedisService redisService;

    /**
     * 查询商户列表
     * 
     * @param mchNo 商户列表主键
     * @return 商户列表
     */
    @Override
    @DataScope(dataType="BUS")
    public MchInfo selectMchInfoByMchNo(String mchNo)
    {
        MchInfo mchInfo = redisService.getCacheObject(CacheConstants.MCH_INFO + mchNo);
        if(mchInfo == null){
            mchInfo = mchInfoMapper.selectMchInfoByMchNo(mchNo);
            redisService.setCacheObject(CacheConstants.MCH_INFO + mchInfo.getMchNo(), mchInfo, CacheConstants.EXPIRATION, TimeUnit.MINUTES);
        }
        return mchInfo;
    }

    /**
     * 查询商户列表列表
     * 
     * @param mchInfo 商户列表
     * @return 商户列表
     */
    @Override
    @DataScope(dataType="BUS")
    public List<MchInfo> selectMchInfoList(MchInfo mchInfo)
    {
        return mchInfoMapper.selectMchInfoList(mchInfo);
    }

    /**
     * 新增商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    @Override
//    @DS("mchinfo")
    @Transactional(rollbackFor = Exception.class)
    @GlobalTransactional
    public boolean insertMchInfo(MchInfo mchInfo)
    {

        //生成商户号
        String mchNo = MerchantConstant.MCH_NO_PREFIX + RandomUtil.randomString(8).toUpperCase();
        mchInfo.setMchNo(mchNo);
        mchInfo.setCreateTime(new Date());
        mchInfo.setUpdateTime(new Date());
        mchInfo.setCreatedBy(SecurityUtils.getUsername());
        mchInfo.setCreatedUid(SecurityUtils.getUserId());

        //设置默认的商户登录用户
        SysUser sysUser = new SysUser();
        sysUser.setUserName(mchInfo.getInitUserName());
        sysUser.setNickName(mchInfo.getMchName());
        sysUser.setEmail(mchInfo.getContactEmail());
        sysUser.setPhonenumber(mchInfo.getContactTel());
        sysUser.setPassword(MerchantConstant.MERCHANT_ADMIN_PASSWORD);
        sysUser.setStatus(UserStatus.OK.getCode());
        sysUser.setCreateBy(mchInfo.getCreateBy());
        sysUser.setCreateTime(new Date());
        sysUser.setUpdateTime(new Date());
        sysUser.setMchNo(mchNo);
        sysUser.setMchName(mchInfo.getMchName());

        R<SysUser> result = remoteUserService.insertMerchUser(sysUser,SecurityConstants.INNER);
        log.info("调用系统微服务返回数据:{}" , JSONUtil.toJsonStr(result));
        if(result.getCode() == R.FAIL){
            throw new RuntimeException(result.getMsg());
        }
        mchInfo.setDeptId(result.getData().getDept().getDeptId());
        mchInfo.setDeptName(result.getData().getDept().getDeptName());

        int rows = mchInfoMapper.insertMchInfo(mchInfo);

        //创建默认应用
        MchApp defaultMchApp = new MchApp();
        defaultMchApp.setAppId(RandomUtil.randomString(26));
        defaultMchApp.setAppName("默认应用");
        defaultMchApp.setMchNo(mchInfo.getMchNo());
        defaultMchApp.setMchName(mchInfo.getMchName());
        defaultMchApp.setState(0L);
        defaultMchApp.setAppDefault(Constants.YES);
        defaultMchApp.setSecretMd5("0");
        defaultMchApp.setSecretMd5Key(RandomUtil.randomString(100));
        defaultMchApp.setCreatedUid(SecurityUtils.getUserId());
        defaultMchApp.setCreatedBy(SecurityUtils.getUsername());

        mchAppService.insertMchApp(defaultMchApp);

        return rows > 0 ;
    }

    /**
     * 修改商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    @Override
    public int updateMchInfo(MchInfo mchInfo)
    {
        return mchInfoMapper.updateMchInfo(mchInfo);
    }

    /**
     * 批量删除商户列表
     * 
     * @param mchNos 需要删除的商户列表主键
     * @return 结果
     */
    @Override
    public int deleteMchInfoByMchNos(String[] mchNos)
    {
        return mchInfoMapper.deleteMchInfoByMchNos(mchNos);
    }

    /**
     * 删除商户列表信息
     * 
     * @param mchNo 商户列表主键
     * @return 结果
     */
    @Override
    public int deleteMchInfoByMchNo(String mchNo)
    {
        return mchInfoMapper.deleteMchInfoByMchNo(mchNo);
    }


}
