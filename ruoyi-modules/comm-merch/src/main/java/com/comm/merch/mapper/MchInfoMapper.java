package com.comm.merch.mapper;

import java.util.List;

import com.comm.merch.domain.MchInfo;

/**
 * 商户列表Mapper接口
 * 
 * @author test
 * @date 2022-05-29
 */
public interface MchInfoMapper 
{
    /**
     * 查询商户列表
     * 
     * @param mchNo 商户列表主键
     * @return 商户列表
     */
    public MchInfo selectMchInfoByMchNo(String mchNo);

    /**
     * 查询商户列表列表
     * 
     * @param mchInfo 商户列表
     * @return 商户列表集合
     */
    public List<MchInfo> selectMchInfoList(MchInfo mchInfo);

    /**
     * 新增商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    public int insertMchInfo(MchInfo mchInfo);

    /**
     * 修改商户列表
     * 
     * @param mchInfo 商户列表
     * @return 结果
     */
    public int updateMchInfo(MchInfo mchInfo);

    /**
     * 删除商户列表
     * 
     * @param mchNo 商户列表主键
     * @return 结果
     */
    public int deleteMchInfoByMchNo(String mchNo);

    /**
     * 批量删除商户列表
     * 
     * @param mchNos 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMchInfoByMchNos(String[] mchNos);
}
