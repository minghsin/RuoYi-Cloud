package com.comm.merch.constant;

/**
 * @author zhengmingxin
 * @Description
 * @time 2022-05-29 6:38 下午
 */
public class MerchantConstant {

    /**
     * 商户号前缀
     */
    public final static String MCH_NO_PREFIX = "MCH";

    /**
     * 商户管理员默认密码
     */
    public final static String MERCHANT_ADMIN_PASSWORD = "123456";

}
