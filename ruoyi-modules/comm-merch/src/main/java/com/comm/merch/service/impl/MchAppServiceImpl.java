package com.comm.merch.service.impl;

import java.util.List;

import com.ruoyi.common.datascope.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.merch.mapper.MchAppMapper;
import com.comm.merch.domain.MchApp;
import com.comm.merch.service.IMchAppService;

/**
 * 商户应用Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-30
 */
@Service
public class MchAppServiceImpl implements IMchAppService 
{
    @Autowired
    private MchAppMapper mchAppMapper;

    /**
     * 查询商户应用
     * 
     * @param appId 商户应用主键
     * @return 商户应用
     */
    @Override
    public MchApp selectMchAppByAppId(String appId)
    {
        return mchAppMapper.selectMchAppByAppId(appId);
    }

    /**
     * 查询商户应用列表
     * 
     * @param mchApp 商户应用
     * @return 商户应用
     */
    @Override
    @DataScope(dataType = "BUS")
    public List<MchApp> selectMchAppList(MchApp mchApp)
    {
        return mchAppMapper.selectMchAppList(mchApp);
    }

    /**
     * 新增商户应用
     * 
     * @param mchApp 商户应用
     * @return 结果
     */
    @Override
    public int insertMchApp(MchApp mchApp)
    {
        return mchAppMapper.insertMchApp(mchApp);
    }

    /**
     * 修改商户应用
     * 
     * @param mchApp 商户应用
     * @return 结果
     */
    @Override
    public int updateMchApp(MchApp mchApp)
    {
        return mchAppMapper.updateMchApp(mchApp);
    }

    /**
     * 批量删除商户应用
     * 
     * @param appIds 需要删除的商户应用主键
     * @return 结果
     */
    @Override
    public int deleteMchAppByAppIds(String[] appIds)
    {
        return mchAppMapper.deleteMchAppByAppIds(appIds);
    }

    /**
     * 删除商户应用信息
     * 
     * @param appId 商户应用主键
     * @return 结果
     */
    @Override
    public int deleteMchAppByAppId(String appId)
    {
        return mchAppMapper.deleteMchAppByAppId(appId);
    }
}
