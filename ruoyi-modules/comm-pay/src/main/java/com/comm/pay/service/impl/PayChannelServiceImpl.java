package com.comm.pay.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayChannelMapper;
import com.comm.pay.domain.PayChannel;
import com.comm.pay.service.IPayChannelService;

/**
 * 渠道信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
@Service
public class PayChannelServiceImpl implements IPayChannelService 
{
    @Autowired
    private PayChannelMapper payChannelMapper;

    /**
     * 查询渠道信息
     * 
     * @param id 渠道信息主键
     * @return 渠道信息
     */
    @Override
    public PayChannel selectPayChannelById(Long id)
    {
        return payChannelMapper.selectPayChannelById(id);
    }

    /**
     * 查询渠道信息列表
     * 
     * @param payChannel 渠道信息
     * @return 渠道信息
     */
    @Override
    public List<PayChannel> selectPayChannelList(PayChannel payChannel)
    {
        return payChannelMapper.selectPayChannelList(payChannel);
    }

    /**
     * 新增渠道信息
     * 
     * @param payChannel 渠道信息
     * @return 结果
     */
    @Override
    public int insertPayChannel(PayChannel payChannel)
    {
        payChannel.setCreateTime(DateUtils.getNowDate());
        return payChannelMapper.insertPayChannel(payChannel);
    }

    /**
     * 修改渠道信息
     * 
     * @param payChannel 渠道信息
     * @return 结果
     */
    @Override
    public int updatePayChannel(PayChannel payChannel)
    {
        payChannel.setUpdateTime(DateUtils.getNowDate());
        return payChannelMapper.updatePayChannel(payChannel);
    }

    /**
     * 批量删除渠道信息
     * 
     * @param ids 需要删除的渠道信息主键
     * @return 结果
     */
    @Override
    public int deletePayChannelByIds(Long[] ids)
    {
        return payChannelMapper.deletePayChannelByIds(ids);
    }

    /**
     * 删除渠道信息信息
     * 
     * @param id 渠道信息主键
     * @return 结果
     */
    @Override
    public int deletePayChannelById(Long id)
    {
        return payChannelMapper.deletePayChannelById(id);
    }
}
