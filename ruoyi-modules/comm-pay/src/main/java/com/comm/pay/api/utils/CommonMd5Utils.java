package com.comm.pay.api.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 * Md5加密方法
 * 
 * @author ruoyi
 */
public class CommonMd5Utils
{
    private static final Logger log = LoggerFactory.getLogger(CommonMd5Utils.class);

    private static byte[] md5(String s)
    {
        MessageDigest algorithm;
        try
        {
            algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(s.getBytes("UTF-8"));
            byte[] messageDigest = algorithm.digest();
            return messageDigest;
        }
        catch (Exception e)
        {
            log.error("MD5 Error...", e);
        }
        return null;
    }

    private static final String toHex(byte hash[])
    {
        if (hash == null)
        {
            return null;
        }
        StringBuffer buf = new StringBuffer(hash.length * 2);
        int i;

        for (i = 0; i < hash.length; i++)
        {
            if ((hash[i] & 0xff) < 0x10)
            {
                buf.append("0");
            }
            buf.append(Long.toString(hash[i] & 0xff, 16));
        }
        return buf.toString();
    }

    public static String hash(String s)
    {
        try
        {
            return new String(toHex(md5(s)).getBytes("UTF-8"), "UTF-8");
        }
        catch (Exception e)
        {
            log.error("not supported charset...{}", e);
            return s;
        }
    }

    public static String signature(TreeMap<String, Object> params, String appSecret,boolean isNull) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, s2) -> {
            if(isNull){
                if (s2 != null && !StrUtil.isEmpty(s2.toString())) {
                    sb.append(s).append("=").append(s2).append("&");
                }
            }else{
                sb.append(s).append("=").append(s2).append("&");
            }


        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("&key="+appSecret);
        log.info(sb.toString());
        return hash(sb.toString());
    }

    public static String signature(TreeMap<String, Object> params, String appSecret,boolean isNull,boolean appendKey) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, s2) -> {
            if(isNull){
                if (s2 != null && !StrUtil.isEmpty(s2.toString())) {
                    sb.append(s).append("=").append(s2).append("&");
                }
            }else{
                sb.append(s).append("=").append(s2).append("&");
            }


        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        if(appendKey){
            sb.append("&key="+appSecret);
        }else{
            sb.append("&"+appSecret);
        }

        log.info(sb.toString());
        return hash(sb.toString());
    }


    public static String signature(TreeMap<String, Object> params, String appSecret,boolean isNull,boolean appendKey,boolean lastChar) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, s2) -> {
            if(isNull){
                if (s2 != null && !StrUtil.isEmpty(s2.toString())) {
                    sb.append(s).append("=").append(s2).append("&");
                }
            }else{
                sb.append(s).append("=").append(s2).append("&");
            }


        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        if(appendKey){
            sb.append("&key="+appSecret);
        }else{
            if(lastChar){
                sb.append("&"+appSecret);
            }else{
                sb.append(appSecret);
            }

        }

        log.info(sb.toString());
        return hash(sb.toString());
    }


    public static String signature(TreeMap<String, Object> params, String appSecret) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, s2) -> {
            if (s2 != null && ! StrUtil.isEmpty(s2.toString())) {
                sb.append(s).append("=").append(s2).append("&");
            }
        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("&key="+appSecret);
        log.info(sb.toString());
        return hash(sb.toString());
    }

    public static String createSignature(Object object, String appSecret) {
        TreeMap<String, Object> signParamMap = new TreeMap<>();

        JSONObject jsonObject = new JSONObject(object);
        for (String key : jsonObject.keySet()) {
            if (jsonObject.get(key) instanceof Collection) {
                continue;
            }
            if (jsonObject.get(key) instanceof Map) {
                continue;
            }
            if (StrUtil.isEmpty(jsonObject.getStr(key))) {
                continue;
            }
            if (key.equals("sign")) {
                continue;
            }
            signParamMap.put(key, jsonObject.get(key));
        }
        String sign2 = signature(signParamMap, appSecret);
        return sign2;
    }

    /**
     * 验证签名
     *
     * @param object
     * @param secret 密钥
     * @return
     */
    public static boolean validSign(Object object, String secret, String sign) {
        String sign2 = createSignature(object, secret);
        if (sign.equals(sign2)) {
            return true;
        }
        return false;
    }


    public static String getStrSignature(TreeMap<String, Object> params, String appSecret,boolean isNull,boolean appendKey,boolean lastChar) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, s2) -> {
            if(isNull){
                if (s2 != null && !StrUtil.isEmpty(s2.toString())) {
                    sb.append(s).append("=").append(s2).append("&");
                }
            }else{
                sb.append(s).append("=").append(s2).append("&");
            }


        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        if(appendKey){
            sb.append("&key="+appSecret);
        }else{
            if(lastChar){
                sb.append("&"+appSecret);
            }else{
                sb.append(appSecret);
            }

        }

        log.info(sb.toString());
        return sb.toString();
    }

}
