package com.comm.pay.domain;

import java.math.BigDecimal;

/**
 * 订单统计
 */
public class PayOrderSum {

    /**
     * 实际收款金额
     */
    private BigDecimal actualAmount = BigDecimal.ZERO;

    /**
     * 成交金额
     */
    private BigDecimal orderAmount = BigDecimal.ZERO;

    /**
     * 订单笔数
     */
    private int orderNum;

    /**
     * 手续费金额
     */
    private BigDecimal orderFee = BigDecimal.ZERO;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount = BigDecimal.ZERO;


    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public BigDecimal getOrderFee() {
        return orderFee;
    }

    public void setOrderFee(BigDecimal orderFee) {
        this.orderFee = orderFee;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }
}
