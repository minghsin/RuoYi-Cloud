package com.comm.pay.api.channel;


import com.comm.pay.domain.PayChannelConfig;
import com.comm.pay.domain.PayChannelWay;
import com.comm.pay.service.IPayChannelConfigService;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 渠道bean工厂
 */
@Configuration
public class ChannelFactory {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private IPayChannelConfigService channelConfigService;

    private final String CHNL_NAME = "CHNL_NAME_";

    private final String BEAN_PROP = "config";

    @PostConstruct
    private void registerChnl() throws Exception
    {

        PayChannelConfig queryConfig = new PayChannelConfig();
        queryConfig.setActive("01");
        List<PayChannelConfig> payChannelConfigList = channelConfigService.selectPayChannelConfigList(queryConfig);
        payChannelConfigList.stream().filter(item -> StringUtils.isNotEmpty( item.getBeanClass())).forEach(item -> {

            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(item.getBeanClass());

            beanDefinitionBuilder.addPropertyValue(BEAN_PROP, item);

            BeanDefinition beanDefinition = null;
            beanDefinition = beanDefinitionBuilder.getBeanDefinition();
            String chnlBeanName = CHNL_NAME + item.getChannelId();
            ((DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory())
                    .registerBeanDefinition(chnlBeanName, beanDefinition);
            log.info( "{}注册完成，bean: {}",item.getChnlName() , chnlBeanName);
        });
    }


    public void registerChnlByConfig(PayChannelConfig payChannelConfig){

        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(payChannelConfig.getBeanClass());

        beanDefinitionBuilder.addPropertyValue(BEAN_PROP, payChannelConfig);

        BeanDefinition beanDefinition = null;
        beanDefinition = beanDefinitionBuilder.getBeanDefinition();
        String chnlBeanName = CHNL_NAME + payChannelConfig.getChannelId();
        ((DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory())
                .registerBeanDefinition(chnlBeanName, beanDefinition);
        log.info( "{}注册完成，bean: {}",payChannelConfig.getChnlName() , chnlBeanName);

    }

    public IPayChnlService getPayChnl(Long channelId){
        IPayChnlService payChnlService;
        try{
            payChnlService = (IPayChnlService)applicationContext.getBean(CHNL_NAME + channelId);
        }catch (NoSuchBeanDefinitionException noSuchBeanDefinitionException){
            PayChannelConfig channelConfig = channelConfigService.selectPayChnlConfigByChnlId(channelId);
            registerChnlByConfig(channelConfig);
            payChnlService = (IPayChnlService)applicationContext.getBean(CHNL_NAME + channelId);
        }
       return payChnlService;
    }

    public void setChnlConfig(PayChannelConfig config){
        AbstractPayChnlService abstractPayChnlService = (AbstractPayChnlService)applicationContext.
                getBean(CHNL_NAME + config.getChannelId());
        abstractPayChnlService.setConfig(config);
    }


}
