package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 商户对账单对象 t_pay_mch_settle_bill
 * 
 * @author ruoyi
 * @date 2022-07-05
 */
public class PayMchSettleBill extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 昨日余额 */
    @Excel(name = "昨日余额")
    private Long yestBalance;

    /** 昨日交易额 */
    @Excel(name = "昨日交易额")
    private Long yestOrderAmount;

    /** 昨日转账额 */
    @Excel(name = "昨日转账额")
    private Long yestTransferAmount;

    private String settleDate;

    public String getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(String settleDate) {
        this.settleDate = settleDate;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setYestBalance(Long yestBalance) 
    {
        this.yestBalance = yestBalance;
    }

    public Long getYestBalance() 
    {
        return yestBalance;
    }
    public void setYestOrderAmount(Long yestOrderAmount) 
    {
        this.yestOrderAmount = yestOrderAmount;
    }

    public Long getYestOrderAmount() 
    {
        return yestOrderAmount;
    }
    public void setYestTransferAmount(Long yestTransferAmount) 
    {
        this.yestTransferAmount = yestTransferAmount;
    }

    public Long getYestTransferAmount() 
    {
        return yestTransferAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mchNo", getMchNo())
            .append("mchName", getMchName())
            .append("yestBalance", getYestBalance())
            .append("yestOrderAmount", getYestOrderAmount())
            .append("yestTransferAmount", getYestTransferAmount())
            .append("createTime", getCreateTime())
            .toString();
    }
}
