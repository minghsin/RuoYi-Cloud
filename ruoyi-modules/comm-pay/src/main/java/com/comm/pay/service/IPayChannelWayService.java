package com.comm.pay.service;

import java.math.BigDecimal;
import java.util.List;
import com.comm.pay.domain.PayChannelWay;

/**
 * 渠道支付配置Service接口
 * 
 * @author ruoyi
 * @date 2022-06-16
 */
public interface IPayChannelWayService 
{

    /**
     * 查询可用的支付方式
     * @param wayCode
     * @param amount
     * @return
     */
    List<PayChannelWay> getChannelWayCacheByAmount(String wayCode , BigDecimal amount);

    /**
     * 查询渠道支付配置
     * 
     * @param id 渠道支付配置主键
     * @return 渠道支付配置
     */
    public PayChannelWay selectPayChannelWayById(Long id);

    /**
     * 查询渠道支付配置列表
     * 
     * @param payChannelWay 渠道支付配置
     * @return 渠道支付配置集合
     */
    public List<PayChannelWay> selectPayChannelWayList(PayChannelWay payChannelWay);

    /**
     * 新增渠道支付配置
     * 
     * @param payChannelWay 渠道支付配置
     * @return 结果
     */
    public int insertPayChannelWay(PayChannelWay payChannelWay);

    /**
     * 修改渠道支付配置
     * 
     * @param payChannelWay 渠道支付配置
     * @return 结果
     */
    public int updatePayChannelWay(PayChannelWay payChannelWay);

    /**
     * 批量删除渠道支付配置
     * 
     * @param ids 需要删除的渠道支付配置主键集合
     * @return 结果
     */
    public int deletePayChannelWayByIds(Long[] ids);

    /**
     * 删除渠道支付配置信息
     * 
     * @param id 渠道支付配置主键
     * @return 结果
     */
    public int deletePayChannelWayById(Long id);
}
