package com.comm.pay.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 支付方式对象 t_pay_way
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
public class PayWay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long wayId;

    /** 支付代码 */
    @Excel(name = "支付代码")
    private String wayCode;

    /** 支付名称 */
    @Excel(name = "支付名称")
    private String wayName;

    /** 创建时间 */
    private Date createdAt;

    /** 更新时间 */
    private Date updatedAt;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private String wayType;

    public void setWayId(Long wayId) 
    {
        this.wayId = wayId;
    }

    public Long getWayId() 
    {
        return wayId;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setWayName(String wayName) 
    {
        this.wayName = wayName;
    }

    public String getWayName() 
    {
        return wayName;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }
    public void setWayType(String wayType) 
    {
        this.wayType = wayType;
    }

    public String getWayType() 
    {
        return wayType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wayId", getWayId())
            .append("wayCode", getWayCode())
            .append("wayName", getWayName())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .append("wayType", getWayType())
            .toString();
    }
}
