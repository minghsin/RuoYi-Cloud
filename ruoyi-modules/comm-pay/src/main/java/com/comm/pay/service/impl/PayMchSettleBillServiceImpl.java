package com.comm.pay.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayMchSettleBillMapper;
import com.comm.pay.domain.PayMchSettleBill;
import com.comm.pay.service.IPayMchSettleBillService;

/**
 * 商户对账单Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-05
 */
@Service
public class PayMchSettleBillServiceImpl implements IPayMchSettleBillService 
{
    @Autowired
    private PayMchSettleBillMapper payMchSettleBillMapper;

    /**
     * 生成商户对账单
     * @return
     */
    @Override
    public boolean createMchSettleBill(String date){
        return payMchSettleBillMapper.genSettleBillByDate(date) > 0;
    }

    /**
     * 查询商户对账单
     * 
     * @param id 商户对账单主键
     * @return 商户对账单
     */
    @Override
    public PayMchSettleBill selectPayMchSettleBillById(Long id)
    {
        return payMchSettleBillMapper.selectPayMchSettleBillById(id);
    }

    /**
     * 查询商户对账单列表
     * 
     * @param payMchSettleBill 商户对账单
     * @return 商户对账单
     */
    @Override
    public List<PayMchSettleBill> selectPayMchSettleBillList(PayMchSettleBill payMchSettleBill)
    {
        return payMchSettleBillMapper.selectPayMchSettleBillList(payMchSettleBill);
    }

    /**
     * 新增商户对账单
     * 
     * @param payMchSettleBill 商户对账单
     * @return 结果
     */
    @Override
    public int insertPayMchSettleBill(PayMchSettleBill payMchSettleBill)
    {
        payMchSettleBill.setCreateTime(DateUtils.getNowDate());
        return payMchSettleBillMapper.insertPayMchSettleBill(payMchSettleBill);
    }

    /**
     * 修改商户对账单
     * 
     * @param payMchSettleBill 商户对账单
     * @return 结果
     */
    @Override
    public int updatePayMchSettleBill(PayMchSettleBill payMchSettleBill)
    {
        return payMchSettleBillMapper.updatePayMchSettleBill(payMchSettleBill);
    }

    /**
     * 批量删除商户对账单
     * 
     * @param ids 需要删除的商户对账单主键
     * @return 结果
     */
    @Override
    public int deletePayMchSettleBillByIds(Long[] ids)
    {
        return payMchSettleBillMapper.deletePayMchSettleBillByIds(ids);
    }

    /**
     * 删除商户对账单信息
     * 
     * @param id 商户对账单主键
     * @return 结果
     */
    @Override
    public int deletePayMchSettleBillById(Long id)
    {
        return payMchSettleBillMapper.deletePayMchSettleBillById(id);
    }
}
