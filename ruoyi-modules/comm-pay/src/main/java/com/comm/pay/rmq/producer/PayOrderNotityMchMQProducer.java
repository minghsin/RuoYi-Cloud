package com.comm.pay.rmq.producer;


import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

/**
 * 生产MQ消息
 * 业务： 支付订单分账处理逻辑
 * @author terrfly
 * @date 2021/8/22 8:23
 */
@Configuration
public class PayOrderNotityMchMQProducer {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private String groupName = "ORDER_NOTIFY_GROUP";
    private String topic = "ORDER_NOTIFY_MERCHANT_TOPTIC";

    @Value("${rocketmq.name-server}")
    private String namesrvAddr;

    private DefaultMQProducer defaultMQProducer;

    @PostConstruct
    public void start(){
        try{
            defaultMQProducer = new DefaultMQProducer(groupName);
            defaultMQProducer.setSendMsgTimeout(10000);
            defaultMQProducer.setNamesrvAddr(namesrvAddr);
            defaultMQProducer.setVipChannelEnabled(false);
            defaultMQProducer.start();
            log.info("记账消费启动成功");
        }catch (Exception e){
            log.info("记账消费启动失败:{}", e);
        }

    }

    public void send(String msg){
        log.info("发送消息：{}", msg);
        try{
            Message message = new Message();
            message.setBody(msg.getBytes(StandardCharsets.UTF_8));
            message.setTopic(topic);

            defaultMQProducer.send(message);

        }catch (Exception e){
            log.info("发送消息失败:{}",e);
        }
    }

}
