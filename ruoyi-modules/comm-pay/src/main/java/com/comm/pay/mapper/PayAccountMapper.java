package com.comm.pay.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.comm.pay.domain.PayAccount;
import org.apache.ibatis.annotations.Param;

/**
 * 账户信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
public interface PayAccountMapper 
{

    PayAccount selectPayAccountByAccountUid(@Param("accountUid") String accountUid, @Param("uidType") String uidType);

    /**
     * 将代付中余额变更到已结算金额
     * @param id
     * @param amount
     * @return
     */
    int subtractPayforToSettle(@Param("id") Long id, @Param("amount") BigDecimal amount);

    /**
     * 将代付中余额变更到可用余额
     * @param id
     * @param amount
     * @return
     */
    int subtractPayforToBalance(@Param("id") Long id, @Param("amount") BigDecimal amount);

    int subtractBalanceTopayfor(@Param("id") Long id, @Param("amount") BigDecimal amount);

    /**
     * 添加余额
     * @param amount
     * @param id
     * @return
     */
    int addBalance(@Param("id") Long id, @Param("amount") BigDecimal amount);

    /**
     * 查询账户信息
     * 
     * @param id 账户信息主键
     * @return 账户信息
     */
    public PayAccount selectPayAccountById(Long id);

    /**
     * 查询账户信息列表
     * 
     * @param payAccount 账户信息
     * @return 账户信息集合
     */
    public List<PayAccount> selectPayAccountList(PayAccount payAccount);

    /**
     * 新增账户信息
     * 
     * @param payAccount 账户信息
     * @return 结果
     */
    public int insertPayAccount(PayAccount payAccount);

    /**
     * 修改账户信息
     * 
     * @param payAccount 账户信息
     * @return 结果
     */
    public int updatePayAccount(PayAccount payAccount);

    /**
     * 删除账户信息
     * 
     * @param id 账户信息主键
     * @return 结果
     */
    public int deletePayAccountById(Long id);

    /**
     * 批量删除账户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayAccountByIds(Long[] ids);
}
