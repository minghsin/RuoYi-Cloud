package com.comm.pay.service;

import java.util.List;
import com.comm.pay.domain.PayRate;

/**
 * 商户支付费率Service接口
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
public interface IPayRateService 
{

    /**
     * 按商户号查询费率
     * @param mchNo
     * @return
     */
    PayRate selectPayRateListByMchNo(String mchNo, String wayCode);

    /**
     * 修改或者新增商户费率
     * @param payRateList
     * @return
     */
    boolean saveMchRate(List<PayRate> payRateList);

    /**
     * 按商户号查询费率
     * @param mchNo
     * @return
     */
    List<PayRate> selectPayRateListByMchNo(String mchNo);

    /**
     * 查询商户支付费率
     * 
     * @param id 商户支付费率主键
     * @return 商户支付费率
     */
    public PayRate selectPayRateById(Long id);

    /**
     * 查询商户支付费率列表
     * 
     * @param payRate 商户支付费率
     * @return 商户支付费率集合
     */
    public List<PayRate> selectPayRateList(PayRate payRate);

    /**
     * 新增商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    public int insertPayRate(PayRate payRate);

    /**
     * 修改商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    public int updatePayRate(PayRate payRate);

    /**
     * 批量删除商户支付费率
     * 
     * @param ids 需要删除的商户支付费率主键集合
     * @return 结果
     */
    public int deletePayRateByIds(Long[] ids);

    /**
     * 删除商户支付费率信息
     * 
     * @param id 商户支付费率主键
     * @return 结果
     */
    public int deletePayRateById(Long id);
}
