package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 支付类型对象 t_pay_type
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
public class PayType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 支付类型 */
    private String wayType;

    /** 支付类型名称 */
    @Excel(name = "支付类型名称")
    private String name;

    /** 类型图标 */
    @Excel(name = "类型图标")
    private String wayIcon;

    public void setWayType(String wayType) 
    {
        this.wayType = wayType;
    }

    public String getWayType() 
    {
        return wayType;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setWayIcon(String wayIcon) 
    {
        this.wayIcon = wayIcon;
    }

    public String getWayIcon() 
    {
        return wayIcon;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wayType", getWayType())
            .append("name", getName())
            .append("wayIcon", getWayIcon())
            .toString();
    }
}
