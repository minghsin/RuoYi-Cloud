package com.comm.pay.api.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.comm.merch.api.RemoteMerchAppService;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchApp;
import com.comm.pay.api.channel.ChannelFactory;
import com.comm.pay.api.channel.IPayChnlService;
import com.comm.pay.api.dto.PayOrderNotifyMerchDto;
import com.comm.pay.api.dto.TransferRespDto;
import com.comm.pay.api.utils.CommonMd5Utils;
import com.comm.pay.domain.PayAccount;
import com.comm.pay.domain.PayChannelConfig;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.TransferOrder;
import com.comm.pay.service.IPayAccountService;
import com.comm.pay.service.IPayChannelConfigService;
import com.comm.pay.service.IPayOrderService;
import com.comm.pay.service.ITransferOrderService;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.enums.*;
import com.ruoyi.common.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.TreeMap;

@Service
public class TransferNotifyServiceImpl {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IPayChannelConfigService channelConfigService;

    @Autowired
    private ChannelFactory channelFactory;

    @Autowired
    private IPayAccountService payAccountService;


    @Autowired
    private RemoteMerchService merchService;

    @Autowired
    private RemoteMerchAppService merchAppService;

    @Autowired
    private ITransferOrderService transferOrderService;


    /**
     *
     * @param chnlId 渠道编码
     * @param transferId 系统订单号
     * @param requestIp 请求IP
     * @param requestBody 请求参数
     */
    public boolean channelNotifyHandle(Long chnlId, String transferId, String requestIp, String requestBody){

        PayChannelConfig channelConfig = channelConfigService.selectPayChnlConfigByChnlId(chnlId);
        TransferOrder transferOrder = transferOrderService.selectTransferOrderByTransferId(transferId);
        //验证百名单
        String[] ipWhiteList = channelConfig.getCallbackIpWhitelist().split(",");
        if(!Arrays.asList(ipWhiteList).contains(requestIp)){
            throw new RuntimeException("非法请求");
        }

        //验证签名
        IPayChnlService payChnlService = channelFactory.getPayChnl(chnlId);
        payChnlService.transferNotifyCallback(transferOrder,requestBody);

        PayAccount payAccount = payAccountService.selectPayAccountByAccountUid(transferOrder.getMchNo(), AccountUidType.MCH_TYPE.getCode());
        BigDecimal amount = new BigDecimal(String.valueOf(transferOrder.getAmount())).add(transferOrder.getMchFeeAmount());
        if(StringUtils.equals(transferOrder.getState(), TransferState.SUCCESS.getCode())){

            payAccountService.subtractPayforToSettle(payAccount.getAccountUid(), payAccount.getUidType(),
                    transferOrder.getTransferId(), AccountOrderType.WITH_SETTLE_ORDER.getCode(), amount);

            transferOrder.setSuccessTime(new Date());

        }else if(StringUtils.equals(transferOrder.getState(), TransferState.FAIL.getCode())){
            // 退回代付中金额
            payAccountService.subtractPayforToBalance(payAccount.getAccountUid(), payAccount.getUidType(),
                    transferOrder.getTransferId(), AccountOrderType.WITH_FAIL_BACK_ORDER.getCode(), amount);
        }
        //更新订单
        transferOrderService.updateTransferOrder(transferOrder);
        return Boolean.TRUE;
    }

    /**
     * 通知商户操作
     * @param transferId
     * @return
     */
    public boolean notityMerchant(String transferId){

        TransferOrder transferOrder = transferOrderService.selectTransferOrderByTransferId(transferId);
        R<MchApp> appResult = merchAppService.getMchAppByAppid(transferOrder.getAppId(), SecurityConstants.INNER);
        if(!appResult.ifSuccess()){
            log.error("转账订单{}创建访问应用信息失败：{}", transferId, appResult.getMsg());
            throw new RuntimeException("转账订单"+transferId+"创建访问商户信息失败："+ appResult.getMsg());
        }

        MchApp mchApp = appResult.getData();

        TransferRespDto respDto = new TransferRespDto();

        BeanUtils.copyProperties(transferOrder,respDto);

        TreeMap<String,Object> signMap = JSONObject.parseObject(JSONObject.toJSONString(respDto),TreeMap.class);
        String sign = CommonMd5Utils.signature(signMap, mchApp.getSecretMd5Key());


        respDto.setSign(sign);
        boolean notifyFlag = Boolean.FALSE;
        try{
            log.info("转账订单{}通知商户：{}", transferId,JSONUtil.toJsonStr(respDto));
            String result = HttpUtil.post(transferOrder.getNotifyUrl(), JSONUtil.toJsonStr(respDto), Constants.HTTP_SEND_TIMEOUT);
            log.info("转账订单{}通知结果{}", transferId, result);
            notifyFlag = StringUtils.equals(result, Constants.MERCH_NOTIFY_RETURN_TEXT);
            if(notifyFlag){
                //通知成功
                transferOrder.setNotifyState(NotifyState.SUCCESS.getCode());
                transferOrderService.updateTransferOrder(transferOrder);
            }else{
                //通知失败
                transferOrder.setNotifyState(NotifyState.FAIL.getCode());
                transferOrderService.updateTransferOrder(transferOrder);
            }
        }catch (Exception e){
            log.error("转账订单{}通知商户发送异常:{}", transferId, e);
        }
        return notifyFlag;
    }

}
