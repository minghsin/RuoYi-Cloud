package com.comm.pay.service.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayChannelWayMapper;
import com.comm.pay.domain.PayChannelWay;
import com.comm.pay.service.IPayChannelWayService;

/**
 * 渠道支付配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-16
 */
@Service
public class PayChannelWayServiceImpl implements IPayChannelWayService 
{
    @Autowired
    private PayChannelWayMapper payChannelWayMapper;

    @Autowired
    private RedisService redisService;

    /**
     *
     * @param wayCode 支付方式
     * @param amount 金额，单位：元
     * @return
     */
    @Override
    public List<PayChannelWay> getChannelWayCacheByAmount(String wayCode , BigDecimal amount){

        List<PayChannelWay> allChnlPayWay =
                JSON.parseArray(JSONObject.toJSONString(redisService.getCacheObject(CacheConstants.CHNL_ALL_PAYTYPE + wayCode)), PayChannelWay.class);

        if(CollectionUtil.isEmpty(allChnlPayWay)){
            refreshCacheChnlWay(wayCode);
        }

        if(CollectionUtil.isNotEmpty(allChnlPayWay)){

            return allChnlPayWay.stream()
                    .filter(item -> item.getMinAmount().compareTo(amount) <= 0
                            && item.getMaxAmount().compareTo(amount) >= 0
                            && StringUtils.equals(item.getActive(),Constants.YES))
                    .collect(Collectors.toList());
        }
        return null;
    }


    public void refreshCacheChnlWay(String wayCode){
        redisService.deleteObject(CacheConstants.CHNL_ALL_PAYTYPE + wayCode);
    }

    /**
     * 查询渠道支付配置
     * 
     * @param id 渠道支付配置主键
     * @return 渠道支付配置
     */
    @Override
    public PayChannelWay selectPayChannelWayById(Long id)
    {
        return payChannelWayMapper.selectPayChannelWayById(id);
    }

    /**
     * 查询渠道支付配置列表
     * 
     * @param payChannelWay 渠道支付配置
     * @return 渠道支付配置
     */
    @Override
    public List<PayChannelWay> selectPayChannelWayList(PayChannelWay payChannelWay)
    {
        return payChannelWayMapper.selectPayChannelWayList(payChannelWay);
    }

    /**
     * 新增渠道支付配置
     * 
     * @param payChannelWay 渠道支付配置
     * @return 结果
     */
    @Override
    public int insertPayChannelWay(PayChannelWay payChannelWay)
    {
        payChannelWay.setCreateTime(DateUtils.getNowDate());
        int rows = payChannelWayMapper.insertPayChannelWay(payChannelWay);
        if(rows > 0){
            this.refreshCacheChnlWay(payChannelWay.getWayCode());
        }
        return rows;
    }

    /**
     * 修改渠道支付配置
     * 
     * @param payChannelWay 渠道支付配置
     * @return 结果
     */
    @Override
    public int updatePayChannelWay(PayChannelWay payChannelWay)
    {
        payChannelWay.setUpdateTime(DateUtils.getNowDate());
        int rows = payChannelWayMapper.updatePayChannelWay(payChannelWay);
        if(rows > 0){
            this.refreshCacheChnlWay(payChannelWay.getWayCode());
        }
        return rows;
    }

    /**
     * 批量删除渠道支付配置
     * 
     * @param ids 需要删除的渠道支付配置主键
     * @return 结果
     */
    @Override
    public int deletePayChannelWayByIds(Long[] ids)
    {
        return payChannelWayMapper.deletePayChannelWayByIds(ids);
    }

    /**
     * 删除渠道支付配置信息
     * 
     * @param id 渠道支付配置主键
     * @return 结果
     */
    @Override
    public int deletePayChannelWayById(Long id)
    {
        return payChannelWayMapper.deletePayChannelWayById(id);
    }
}
