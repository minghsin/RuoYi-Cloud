package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.PayOrderSum;

/**
 * 支付订单Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
public interface PayOrderMapper 
{

    /**
     * 统计支付成功订单
     * @param payOrder
     * @return
     */
    PayOrderSum statisticOrders(PayOrder payOrder);

    /**
     * 查询支付订单
     *
     * @param payOrderId 支付订单主键
     * @return 支付订单
     */
    PayOrder selectPayOrderByMchOrderId(String mchOrderNo);

    /**
     * 查询商户订单号是否存在
     * @param mchOrderNo
     * @return
     */
    int ifExistByMchOrderNo(String mchOrderNo);

    /**
     * 查询支付订单
     * 
     * @param payOrderId 支付订单主键
     * @return 支付订单
     */
    public PayOrder selectPayOrderByPayOrderId(String payOrderId);

    /**
     * 查询支付订单列表
     * 
     * @param payOrder 支付订单
     * @return 支付订单集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 删除支付订单
     * 
     * @param payOrderId 支付订单主键
     * @return 结果
     */
    public int deletePayOrderByPayOrderId(String payOrderId);

    /**
     * 批量删除支付订单
     * 
     * @param payOrderIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayOrderByPayOrderIds(String[] payOrderIds);
}
