package com.comm.pay.api.dto;

import javax.validation.constraints.NotNull;

public class BaseReqDto {

    /**
     * 商户号
     */
    @NotNull(message = "商户号不能为空")
    private String mchNo;

    /**
     * 应用ID
     */
    @NotNull(message = "应用ID不能为空")
    private String appId;

    @NotNull(message = "签名不能为空")
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getMchNo() {
        return mchNo;
    }

    public void setMchNo(String mchNo) {
        this.mchNo = mchNo;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
