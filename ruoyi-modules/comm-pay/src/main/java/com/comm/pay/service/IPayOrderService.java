package com.comm.pay.service;

import java.util.List;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.PayOrderSum;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付订单Service接口
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
public interface IPayOrderService
{

    /**
     * 创建预支付订单
     * @param payOrder
     * @return
     */
    PayOrder repayOrder(PayOrder payOrder);

    /**
     * 手工处理订单
     * @param payOrder
     * @return
     */
    boolean manualReplenishment(PayOrder payOrder);

    /**
     * 创建订单
     * @param payOrder
     * @return
     */
    PayOrder createOrder(PayOrder payOrder);

    /**
     * 根据商户订单号查询支付订单
     *
     * @param mchOrderNo 商户订单号
     * @return 支付订单
     */
    PayOrder selectPayOrderByMchOrderId(String mchOrderNo);

    /**
     * 根据商户订单号判断是否存在
     * @param mchOrderNo
     * @return
     */
    boolean ifExistByMchOrderNo(String mchOrderNo);

    /**
     * 统计支付成功订单
     * @param payOrder
     * @return
     */
    PayOrderSum statisticOrders(PayOrder payOrder);

    /**
     * 查询支付订单
     * 
     * @param payOrderId 支付订单主键
     * @return 支付订单
     */
    public PayOrder selectPayOrderByPayOrderId(String payOrderId);

    /**
     * 查询支付订单列表
     * 
     * @param payOrder 支付订单
     * @return 支付订单集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改支付订单
     * 
     * @param payOrder 支付订单
     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 批量删除支付订单
     * 
     * @param payOrderIds 需要删除的支付订单主键集合
     * @return 结果
     */
    public int deletePayOrderByPayOrderIds(String[] payOrderIds);

    /**
     * 删除支付订单信息
     * 
     * @param payOrderId 支付订单主键
     * @return 结果
     */
    public int deletePayOrderByPayOrderId(String payOrderId);
}
