package com.comm.pay.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchInfo;
import com.comm.pay.domain.PayAccount;
import com.comm.pay.domain.PayRate;
import com.comm.pay.service.IPayAccountService;
import com.comm.pay.service.IPayRateService;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.enums.*;
import com.ruoyi.common.datascope.annotation.DataScope;
import com.ruoyi.common.security.annotation.InnerAuth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.TransferOrderMapper;
import com.comm.pay.domain.TransferOrder;
import com.comm.pay.service.ITransferOrderService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 转账订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-03
 */
@Service
public class TransferOrderServiceImpl implements ITransferOrderService 
{

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private TransferOrderMapper transferOrderMapper;

    @Autowired
    private IPayAccountService payAccountService;

    @Autowired
    private RemoteMerchService merchService;

    @Autowired
    private IPayRateService payRateService;

    @Override
    public boolean auditPass(TransferOrder auditTransfer){
        auditTransfer.setState(TransferState.PAYING.getCode());
        auditTransfer.setAuditState(TransferAuditState.SUCCESS.getCode());
        auditTransfer.setUpdatedAt(new Date());
        return updateTransferOrder(auditTransfer) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean auditRefusal(TransferOrder auditTransfer){
        auditTransfer.setState(TransferState.REVOKED.getCode());
        auditTransfer.setAuditState(TransferAuditState.FAIL.getCode());
        auditTransfer.setUpdatedAt(new Date());
        boolean flag = updateTransferOrder(auditTransfer) > 0;
        if(flag){
            PayAccount payAccount = payAccountService.selectPayAccountByAccountUid(auditTransfer.getMchNo(), AccountUidType.MCH_TYPE.getCode());
            // 退回代付中金额
            payAccountService.subtractPayforToBalance(payAccount.getAccountUid(), payAccount.getUidType(),
                    auditTransfer.getTransferId(), AccountOrderType.WITH_ORDER.getCode(), new BigDecimal(String.valueOf(auditTransfer.getAmount())));
        }

        return flag;
    }

    /**
     * 商户转账
     * @param transferOrder
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean mchTransferHandle(TransferOrder transferOrder){

        //计算手续费
        PayRate payRate = payRateService.selectPayRateListByMchNo(transferOrder.getMchNo(), "TRANSFER");
        if(payRate == null){
            throw new RuntimeException("商户未开通转账功能，请联系运营");
        }

        if(StrUtil.equals(payRate.getRateType(), Constants.RATE_FIX_TYPE)){
            transferOrder.setMchFeeAmount(payRate.getRate());
        }else{
            BigDecimal amount = new BigDecimal(String.valueOf(transferOrder.getAmount())).divide(new BigDecimal(100));
            BigDecimal feeRate = payRate.getRate().divide(new BigDecimal(100));
            BigDecimal feeAmount = amount.multiply(feeRate).multiply(new BigDecimal(100));
            transferOrder.setMchFeeAmount(feeAmount);
        }
        transferOrder.setMchFeeRate(payRate.getRate());

        BigDecimal transferAmount = new BigDecimal(String.valueOf(transferOrder.getAmount())).add(transferOrder.getMchFeeAmount());
        //判断商户余额
        R<MchInfo> result = merchService.getMchInfoByNo(transferOrder.getMchNo(), SecurityConstants.INNER);
        if(!result.ifSuccess()){
            logger.error("订单{}创建访问商户信息失败：{}", transferOrder.getMchNo(), result.getMsg());
            throw new RuntimeException("网络目前拥堵，请稍后重试");
        }
        MchInfo mchInfo = result.getData();
        transferOrder.setMchType(mchInfo.getType());
        PayAccount payAccount = payAccountService.selectPayAccountByAccountUid(transferOrder.getMchNo(), AccountUidType.MCH_TYPE.getCode());
        if(transferAmount.longValue() > payAccount.getBalance()){
            throw new RuntimeException("余额不足");
        }


        payAccountService.subtractBalanceTopayfor(payAccount.getAccountUid(), payAccount.getUidType(),
                transferOrder.getTransferId(), AccountOrderType.WITH_ORDER.getCode(),transferAmount);

        int rows = transferOrderMapper.insertTransferOrder(transferOrder);
        if(rows > 0){
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }


    /**
     * 查询转账订单
     * 
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    @Override
    public TransferOrder selectTransferOrderByTransferId(String transferId)
    {
        return transferOrderMapper.selectTransferOrderByTransferId(transferId);
    }

    /**
     * 查询转账订单列表
     * 
     * @param transferOrder 转账订单
     * @return 转账订单
     */
    @Override
    @DataScope(dataType = "BUS")
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder)
    {
        return transferOrderMapper.selectTransferOrderList(transferOrder);
    }

    /**
     * 新增转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    @Override
    public int insertTransferOrder(TransferOrder transferOrder)
    {
        return transferOrderMapper.insertTransferOrder(transferOrder);
    }

    /**
     * 修改转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    @Override
    public int updateTransferOrder(TransferOrder transferOrder)
    {
        return transferOrderMapper.updateTransferOrder(transferOrder);
    }

    /**
     * 批量删除转账订单
     * 
     * @param transferIds 需要删除的转账订单主键
     * @return 结果
     */
    @Override
    public int deleteTransferOrderByTransferIds(String[] transferIds)
    {
        return transferOrderMapper.deleteTransferOrderByTransferIds(transferIds);
    }

    /**
     * 删除转账订单信息
     * 
     * @param transferId 转账订单主键
     * @return 结果
     */
    @Override
    public int deleteTransferOrderByTransferId(String transferId)
    {
        return transferOrderMapper.deleteTransferOrderByTransferId(transferId);
    }
}
