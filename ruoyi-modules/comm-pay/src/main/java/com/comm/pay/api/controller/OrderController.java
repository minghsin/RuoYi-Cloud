package com.comm.pay.api.controller;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONObject;
import com.comm.merch.api.RemoteMerchAppService;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.domain.MchInfo;
import com.comm.pay.api.dto.CreateOrderReqDto;
import com.comm.pay.api.dto.CreateOrderRespDto;
import com.comm.pay.api.dto.QueryOrderReqDto;
import com.comm.pay.api.dto.QueryOrderRespDto;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.service.IPayOrderService;
import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.enums.PayState;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.system.api.RemoteUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/api/order")
public class OrderController extends ApiBaseController  {

    @Autowired
    private IPayOrderService payOrderService;

    @Autowired
    private RedisService redisService;



    @PostMapping("/createOrder")
    public AjaxResult createOrder(@RequestBody CreateOrderReqDto reqDto){

        AjaxResult validResult = validCreateOrderParams(reqDto);
        if(validResult != null){
            return validResult;
        }

        MchInfo mchInfo = new MchInfo();
        mchInfo.setMchNo(reqDto.getMchNo());
        MchApp mchApp = new MchApp();
        mchApp.setAppId(reqDto.getAppId());

        //验证签名
        validSign(reqDto,mchInfo, mchApp);

        if(payOrderService.ifExistByMchOrderNo(reqDto.getMchOrderNo())){
            return error("商户订单号已存在，不允许重复创建");
        }

        PayOrder payOrder = new PayOrder();
        payOrder.setMchNo(reqDto.getMchNo());
        payOrder.setMchName(mchInfo.getMchName());
        payOrder.setMchType(mchInfo.getType());
        payOrder.setAppId(reqDto.getAppId());
        payOrder.setMchOrderNo(reqDto.getMchOrderNo());
        String payOrderNo = DateUtils.parseDateToStr(DatePattern.PURE_DATETIME_PATTERN,new Date()) + RandomUtil.randomString(5);
        payOrder.setPayOrderId(payOrderNo);
        payOrder.setChannelUser(reqDto.getUserid());
        payOrder.setClientIp(reqDto.getClientIp());
        payOrder.setWayCode(reqDto.getWayCode());
        payOrder.setAmount(new BigDecimal(String.valueOf(reqDto.getAmount())));
        payOrder.setCurrency(reqDto.getCurrency());
        payOrder.setNotifyUrl(reqDto.getNotifyUrl());
        payOrder.setReturnUrl(reqDto.getReturnUrl());
        payOrder.setSubject(reqDto.getSubject());
        payOrder.setBody(reqDto.getBody());
        payOrder.setState(PayState.CREATED.getCode());


        PayOrder createOrder = payOrderService.createOrder(payOrder);
        if(createOrder == null){

        }
        String payUrl = "http://www.baidu.com";

        CreateOrderRespDto respDto = new CreateOrderRespDto();
        BeanUtils.copyProperties(payOrder,respDto);
        respDto.setPayUrl(payUrl);
        respDto.setPayOrderId(respDto.getPayOrderId());
        respDto.setSign(createSign(mchApp.getSecretMd5Key(),respDto));

        return AjaxResult.success(respDto);
    }


    @GetMapping("/repay/{payOrderId}")
    public AjaxResult payOrder(@PathVariable("payOrderId") String payOrderId){

        PayOrder payOrder = payOrderService.selectPayOrderByPayOrderId(payOrderId);
        if(payOrder == null || (!StringUtils.equals(payOrder.getState(), PayState.CREATED.getCode()) && !StringUtils.equals(payOrder.getState(), PayState.FAIL.getCode()))){
            return error("订单已过期或者已被支付");
        }

        payOrderService.repayOrder(payOrder);

        return success();
    }

    @PostMapping("/query")
    public AjaxResult queryOrder(@RequestBody QueryOrderReqDto reqDto){

        if(StringUtils.isEmpty(reqDto.getMchOrderNo()) && StringUtils.isEmpty(reqDto.getPayOrderId())){
            return error("商户订单号与系统订单号请至少一个作为条件");
        }

        MchApp mchApp = new MchApp();
        validSign(reqDto,null,mchApp);

        PayOrder payOrder = payOrderService.selectPayOrderByPayOrderId(reqDto.getPayOrderId());
        if(payOrder == null){
            payOrder = payOrderService.selectPayOrderByMchOrderId(reqDto.getMchOrderNo());
        }

        if(payOrder == null){
            return success("订单不存在");
        }

        QueryOrderRespDto respDto = new QueryOrderRespDto();
        BeanUtils.copyProperties(payOrder,respDto);
        respDto.setSign(createSign(mchApp.getSecretMd5Key(),respDto));
        return  AjaxResult.success(respDto);
    }


    public AjaxResult validCreateOrderParams(CreateOrderReqDto reqDto){

        if(StringUtils.isEmpty(reqDto.getMchNo())  ){
            return AjaxResult.error("商户号不能为空");
        }

        if(StringUtils.isEmpty(reqDto.getAppId())  ){
            return AjaxResult.error("Appid不能为空");
        }

        if(StringUtils.isEmpty(reqDto.getMchOrderNo())  || reqDto.getMchOrderNo().length() > 20  ){
            return AjaxResult.error("商户订单号不能为空或者长度不能超过20位");
        }

        if(reqDto.getAmount() < 10){
            return AjaxResult.error("订单金额不能低于0.1");
        }

        if(StringUtils.isEmpty(reqDto.getWayCode())){
            return AjaxResult.error("支付编码不能为空");
        }
        if(StringUtils.isEmpty(reqDto.getCurrency())){
            reqDto.setCurrency("CNY");
        }

        if(StringUtils.isEmpty(reqDto.getSubject())){
            return AjaxResult.error("商品标题不能为空");
        }

        if(StringUtils.isEmpty(reqDto.getBody())){
            return AjaxResult.error("商品描述不能为空");
        }

        if(StringUtils.isEmpty(reqDto.getNotifyUrl())){
            return AjaxResult.error("通知地址不能为空");
        }

        if(StringUtils.isEmpty(reqDto.getSignType())){
            reqDto.setSignType("md5");
        }

        if(StringUtils.isEmpty(reqDto.getSign())){
            return AjaxResult.error("签名不能为空");
        }
        return null;
    }

}
