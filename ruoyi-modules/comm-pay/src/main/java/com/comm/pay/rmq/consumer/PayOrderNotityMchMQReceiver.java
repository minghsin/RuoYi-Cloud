package com.comm.pay.rmq.consumer;


import cn.hutool.core.thread.ThreadUtil;
import com.comm.pay.api.service.OrderNotifyServiceImpl;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 接收MQ消息
 * 业务： 通知商户支付成功
 * @author terrfly
 * @date 2021/8/22 8:23
 */
@Component
@RocketMQMessageListener(topic = "ORDER_NOTIFY_MERCHANT_TOPTIC",
        consumerGroup = "ORDER_NOTIFY_GROUP",
        selectorExpression = "*")
public class PayOrderNotityMchMQReceiver implements RocketMQListener<String> {

    @Autowired
    private OrderNotifyServiceImpl orderNotifyService;

    @Override
    public void onMessage(String s) {
        String payOrderId = s;
        ThreadUtil.execute(new Runnable() {
            @Override
            public void run() {
                orderNotifyService.notityMerchant(payOrderId);
            }
        });
    }
}
