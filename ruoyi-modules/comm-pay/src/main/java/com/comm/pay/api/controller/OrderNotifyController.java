package com.comm.pay.api.controller;

import cn.hutool.core.thread.ThreadUtil;
import com.comm.pay.api.channel.ChannelFactory;
import com.comm.pay.api.service.OrderNotifyServiceImpl;
import com.comm.pay.api.service.TransferNotifyServiceImpl;
import com.comm.pay.rmq.producer.PayOrderNotityMchMQProducer;
import com.comm.pay.service.IPayChannelConfigService;
import com.ruoyi.common.core.utils.ip.IpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/chnl")
public class OrderNotifyController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderNotifyServiceImpl notifyService;

    @Autowired
    private TransferNotifyServiceImpl transferNotifyService;

    private PayOrderNotityMchMQProducer mqProducer;

    @Autowired
    private IPayChannelConfigService payChannelConfigService;

    @PostMapping("/order/notify/{chnlId}/{payOrderId}")
    public String payNotify(@PathVariable("chnlId") Long chnlId, @PathVariable("payOrderId") String payOrderId, HttpServletRequest request){
        String requestBodyStr = getRequestBodyStr(request);
        String requestIp = IpUtils.getIpAddr(request);
        logger.info("渠道[{}]{}订单号：[{}]回调内容是: {}",requestIp,chnlId,payOrderId,requestBodyStr);
        try{
            boolean flag = notifyService.channelNotifyHandle(chnlId, payOrderId, requestIp, requestBodyStr);
            if(flag){
//                mqProducer.send(payOrderId);
                ThreadUtil.execute(new Runnable() {
                    @Override
                    public void run() {
                        notifyService.notityMerchant(payOrderId);
                    }
                });

            }
            return payChannelConfigService.selectPayChnlConfigByChnlId(chnlId).getReturnText();
        }catch (Exception e){
            logger.error("处理渠道回调发生异常:", e);
        }
        return "FAIL";
    }


    @RequestMapping("/transfer/notify/{chnlId}/{transferId}")
    public String transferNotify(@PathVariable("chnlId") Long chnlId, @PathVariable("transferId") String transferId, HttpServletRequest request){
        String requestBodyStr = getRequestBodyStr(request);
        String requestIp = IpUtils.getIpAddr(request);
        logger.info("渠道[{}]{}转账订单号：[{}]回调内容是: {}",requestIp,chnlId,transferId,requestBodyStr);
        try{
            boolean flag = transferNotifyService.channelNotifyHandle(chnlId, transferId, requestIp, requestBodyStr);
            if(flag){
                ThreadUtil.execute(new Runnable() {
                    @Override
                    public void run() {
                        transferNotifyService.notityMerchant(transferId);
                    }
                });
            }


            return payChannelConfigService.selectPayChnlConfigByChnlId(chnlId).getReturnText();
        }catch (Exception e){
            logger.error("处理渠道回调发生异常:", e);
        }
        return "FAIL";
    }



    private String getRequestBodyStr(HttpServletRequest request){
        try{
            ServletInputStream inputStream = request.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader bufferedReader = new BufferedReader(reader);
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();
        }catch (Exception e){
            logger.error("获取渠道回调参数发生异常：{}",e);
        }
        return null;
    }

}
