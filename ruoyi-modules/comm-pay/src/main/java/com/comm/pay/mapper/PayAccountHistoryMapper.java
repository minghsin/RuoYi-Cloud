package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayAccountHistory;

/**
 * 账户记录历史Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
public interface PayAccountHistoryMapper 
{
    /**
     * 查询账户记录历史
     * 
     * @param id 账户记录历史主键
     * @return 账户记录历史
     */
    public PayAccountHistory selectPayAccountHistoryById(Long id);

    /**
     * 查询账户记录历史列表
     * 
     * @param payAccountHistory 账户记录历史
     * @return 账户记录历史集合
     */
    public List<PayAccountHistory> selectPayAccountHistoryList(PayAccountHistory payAccountHistory);

    /**
     * 新增账户记录历史
     * 
     * @param payAccountHistory 账户记录历史
     * @return 结果
     */
    public int insertPayAccountHistory(PayAccountHistory payAccountHistory);

    /**
     * 修改账户记录历史
     * 
     * @param payAccountHistory 账户记录历史
     * @return 结果
     */
    public int updatePayAccountHistory(PayAccountHistory payAccountHistory);

    /**
     * 删除账户记录历史
     * 
     * @param id 账户记录历史主键
     * @return 结果
     */
    public int deletePayAccountHistoryById(Long id);

    /**
     * 批量删除账户记录历史
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayAccountHistoryByIds(Long[] ids);
}
