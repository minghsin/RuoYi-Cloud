package com.comm.pay.api.channel;

import com.comm.pay.domain.PayChannelConfig;

public abstract class AbstractPayChnlService implements IPayChnlService {


    private String orderNotifyUrl = "http://localhost:8080/pay/api/chnl//order/notify/{chnlId}/{payOrderId}";
    private String transferNotityUrl = "http://localhost:8080/pay/api/chnl/transfer/notify/{chnlId}/{payOrderId}";

    private PayChannelConfig config;

    public PayChannelConfig getConfig() {
        return config;
    }

    public void setConfig(PayChannelConfig config) {
        this.config = config;
    }


    public String getOrderNotifyUrl(String payOrderId) {
        return orderNotifyUrl.replace("{chnlId}",this.getConfig().getChannelId()+"").replace("{payOrderId}", payOrderId);
    }

    public String getTransferNotityUrl(String transferId) {
        return transferNotityUrl.replace("{chnlId}",this.getConfig().getChannelId()+"").replace("{payOrderId}", transferId);
    }
}
