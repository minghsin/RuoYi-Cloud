package com.comm.pay.api.service;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.comm.merch.api.RemoteMerchAppService;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.domain.MchInfo;
import com.comm.pay.api.channel.ChannelFactory;
import com.comm.pay.api.channel.IPayChnlService;
import com.comm.pay.api.dto.PayOrderNotifyMerchDto;
import com.comm.pay.api.utils.CommonMd5Utils;
import com.comm.pay.domain.PayChannelConfig;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.service.IPayAccountService;
import com.comm.pay.service.IPayChannelConfigService;
import com.comm.pay.service.IPayOrderService;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.enums.AccountOrderType;
import com.ruoyi.common.core.enums.AccountUidType;
import com.ruoyi.common.core.enums.NotifyState;
import com.ruoyi.common.core.enums.PayState;
import com.ruoyi.common.core.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.TreeMap;

@Service
public class OrderNotifyServiceImpl {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IPayChannelConfigService channelConfigService;

    @Autowired
    private ChannelFactory channelFactory;

    @Autowired
    private IPayAccountService accountService;

    @Autowired
    private IPayOrderService payOrderService;

    @Autowired
    private RemoteMerchService merchService;

    @Autowired
    private RemoteMerchAppService merchAppService;

    /**
     *
     * @param chnlId 渠道编码
     * @param payOrderId 系统订单号
     * @param requestIp 请求IP
     * @param requestBody 请求参数
     */
    public boolean channelNotifyHandle(Long chnlId, String payOrderId, String requestIp, String requestBody){

        PayChannelConfig channelConfig = channelConfigService.selectPayChnlConfigByChnlId(chnlId);
        PayOrder payOrder = payOrderService.selectPayOrderByPayOrderId(payOrderId);
        //验证百名单
        String[] ipWhiteList = channelConfig.getCallbackIpWhitelist().split(",");
        if(!Arrays.asList(ipWhiteList).contains(requestIp)){
            throw new RuntimeException("非法请求");
        }

        //验证签名
        IPayChnlService payChnlService = channelFactory.getPayChnl(chnlId);
        payChnlService.notityCallback(payOrder,requestBody);

        if(StringUtils.equals(payOrder.getState(), PayState.SUCCESS.getCode())){
            BigDecimal amount = payOrder.getRealAmount().subtract(payOrder.getMchFeeAmount());
            accountService.addBalance(payOrder.getMchNo(), AccountUidType.MCH_TYPE.getCode(), payOrder.getPayOrderId(), AccountOrderType.PAY_ORDER.getCode(),amount);
        }
        //更新订单
        payOrderService.updatePayOrder(payOrder);
        return Boolean.TRUE;
    }

    /**
     * 通知商户操作
     * @param payOrderId
     * @return
     */
    public boolean notityMerchant(String payOrderId){

        PayOrder payOrder = payOrderService.selectPayOrderByPayOrderId(payOrderId);
        if(!StringUtils.equals(payOrder.getState(), PayState.SUCCESS.getCode())){
            return Boolean.FALSE;
        }
        R<MchApp> appResult = merchAppService.getMchAppByAppid(payOrder.getAppId(), SecurityConstants.INNER);
        if(!appResult.ifSuccess()){
            log.error("订单{}创建访问应用信息失败：{}", payOrderId, appResult.getMsg());
            throw new RuntimeException("订单"+payOrderId+"创建访问商户信息失败："+ appResult.getMsg());
        }

        MchApp mchApp = appResult.getData();

        PayOrderNotifyMerchDto notifyDto = new PayOrderNotifyMerchDto();
        BeanUtils.copyProperties(payOrder,notifyDto);
        TreeMap<String,Object> signMap = JSONObject.parseObject(JSONObject.toJSONString(notifyDto),TreeMap.class);
        String sign = CommonMd5Utils.signature(signMap, mchApp.getSecretMd5Key());
        notifyDto.setSign(sign);
        boolean notifyFlag = Boolean.FALSE;
        try{
            String result = HttpUtil.post(payOrder.getNotifyUrl(), JSONUtil.toJsonStr(notifyDto), Constants.HTTP_SEND_TIMEOUT);
            notifyFlag = StringUtils.equals(result, Constants.MERCH_NOTIFY_RETURN_TEXT);
            if(notifyFlag){
                //通知成功
                payOrder.setNotifyState(NotifyState.SUCCESS.getCode());
                payOrderService.updatePayOrder(payOrder);
            }else{
                //通知失败
                payOrder.setNotifyState(NotifyState.FAIL.getCode());
                payOrderService.updatePayOrder(payOrder);
            }
        }catch (Exception e){
            log.error("订单{}通知商户发送异常:{}", payOrderId, e);
        }
        return notifyFlag;
    }

}
