package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayMchSettleBill;
import org.apache.ibatis.annotations.Param;

/**
 * 商户对账单Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-05
 */
public interface PayMchSettleBillMapper 
{
    int genSettleBillByDate(@Param("settleDate")String settleDate);

    /**
     * 查询商户对账单
     * 
     * @param id 商户对账单主键
     * @return 商户对账单
     */
    public PayMchSettleBill selectPayMchSettleBillById(Long id);

    /**
     * 查询商户对账单列表
     * 
     * @param payMchSettleBill 商户对账单
     * @return 商户对账单集合
     */
    public List<PayMchSettleBill> selectPayMchSettleBillList(PayMchSettleBill payMchSettleBill);

    /**
     * 新增商户对账单
     * 
     * @param payMchSettleBill 商户对账单
     * @return 结果
     */
    public int insertPayMchSettleBill(PayMchSettleBill payMchSettleBill);

    /**
     * 修改商户对账单
     * 
     * @param payMchSettleBill 商户对账单
     * @return 结果
     */
    public int updatePayMchSettleBill(PayMchSettleBill payMchSettleBill);

    /**
     * 删除商户对账单
     * 
     * @param id 商户对账单主键
     * @return 结果
     */
    public int deletePayMchSettleBillById(Long id);

    /**
     * 批量删除商户对账单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayMchSettleBillByIds(Long[] ids);
}
