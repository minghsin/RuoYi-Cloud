package com.comm.pay.api.channel.impl;

import com.comm.pay.api.channel.AbstractPayChnlService;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.TransferOrder;
import com.ruoyi.common.core.enums.PayState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试演示类
 */
public class DemoChnlServiceImpl extends AbstractPayChnlService {

    private Logger log = LoggerFactory.getLogger(this.getClass());


    @Override
    public PayOrder createOrder(PayOrder payOrder) {
        log.info("{}开始下单,商户号是：{}" ,getConfig().getChnlName(),getConfig().getChnlMchNo());

        payOrder.setState(PayState.PAYING.getCode());

        return null;
    }

    @Override
    public PayOrder queryOrder(PayOrder payOrder) {
        return null;
    }

    @Override
    public PayOrder notityCallback(PayOrder payOrder,String notityStr) {
        return null;
    }

    @Override
    public TransferOrder createTransfer(TransferOrder transferOrder) {
        return null;
    }

    @Override
    public TransferOrder queryTransfer(TransferOrder transferOrder) {
        return null;
    }

    @Override
    public TransferOrder transferNotifyCallback(TransferOrder transferOrder, String notifyStr) {
        return null;
    }
}
