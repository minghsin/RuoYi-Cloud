package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayRate;
import org.apache.ibatis.annotations.Param;

/**
 * 商户支付费率Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
public interface PayRateMapper 
{

    PayRate selectPayRateListByMchWayCode(@Param("mchNo") String mchNo,@Param("wayCode") String wayCode);

    List<PayRate> selectPayRateListByMchNo(String mchNo);

    /**
     * 查询商户支付费率
     * 
     * @param id 商户支付费率主键
     * @return 商户支付费率
     */
    public PayRate selectPayRateById(Long id);

    /**
     * 查询商户支付费率列表
     * 
     * @param payRate 商户支付费率
     * @return 商户支付费率集合
     */
    public List<PayRate> selectPayRateList(PayRate payRate);

    /**
     * 新增商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    public int insertPayRate(PayRate payRate);

    /**
     * 修改商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    public int updatePayRate(PayRate payRate);

    /**
     * 删除商户支付费率
     * 
     * @param id 商户支付费率主键
     * @return 结果
     */
    public int deletePayRateById(Long id);

    /**
     * 批量删除商户支付费率
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayRateByIds(Long[] ids);
}
