package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 账户信息对象 t_pay_account
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
public class PayAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 用户编号，商户或者代理编号 */
    @Excel(name = "用户编号，商户或者代理编号")
    private String accountUid;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String accountName;

    /** 用户类型 01 商户 02 代理 */
    @Excel(name = "用户类型 01 商户 02 代理")
    private String uidType;

    /** 用户余额 */
    @Excel(name = "用户余额")
    private Long balance;

    /** 已结算金额 */
    @Excel(name = "已结算金额")
    private Long settleAmount;

    /** 待结算金额 */
    @Excel(name = "待结算金额")
    private Long waitAmount;

    /** 冻结金额 */
    @Excel(name = "冻结金额")
    private Long freezeAmount;

    /** 代付中的金额 */
    @Excel(name = "代付中的金额")
    private Long payforAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getAccountUid() {
        return accountUid;
    }

    public void setAccountUid(String accountUid) {
        this.accountUid = accountUid;
    }

    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setUidType(String uidType) 
    {
        this.uidType = uidType;
    }

    public String getUidType() 
    {
        return uidType;
    }
    public void setBalance(Long balance) 
    {
        this.balance = balance;
    }

    public Long getBalance() 
    {
        return balance;
    }
    public void setSettleAmount(Long settleAmount) 
    {
        this.settleAmount = settleAmount;
    }

    public Long getSettleAmount() 
    {
        return settleAmount;
    }
    public void setWaitAmount(Long waitAmount) 
    {
        this.waitAmount = waitAmount;
    }

    public Long getWaitAmount() 
    {
        return waitAmount;
    }
    public void setFreezeAmount(Long freezeAmount) 
    {
        this.freezeAmount = freezeAmount;
    }

    public Long getFreezeAmount() 
    {
        return freezeAmount;
    }
    public void setPayforAmount(Long payforAmount) 
    {
        this.payforAmount = payforAmount;
    }

    public Long getPayforAmount() 
    {
        return payforAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("accountUid", getAccountUid())
            .append("accountName", getAccountName())
            .append("uidType", getUidType())
            .append("balance", getBalance())
            .append("settleAmount", getSettleAmount())
            .append("waitAmount", getWaitAmount())
            .append("freezeAmount", getFreezeAmount())
            .append("payforAmount", getPayforAmount())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .toString();
    }
}
