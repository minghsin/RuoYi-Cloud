package com.comm.pay.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.nacos.api.naming.pojo.healthcheck.impl.Http;
import com.comm.pay.domain.form.AddMerchTransferForm;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.enums.TransferAuditState;
import com.ruoyi.common.core.enums.TransferState;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.googleAuth.DynamicToken;
import com.ruoyi.common.core.utils.ip.IpUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.domain.SysUser;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.TransferOrder;
import com.comm.pay.service.ITransferOrderService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 转账订单Controller
 * 
 * @author ruoyi
 * @date 2022-07-03
 */
@RestController
@RequestMapping("/transfer")
public class TransferOrderController extends BaseController
{
    @Autowired
    private ITransferOrderService transferOrderService;

    /**
     * 转账审核通过
     */
    @Log(title = "商户发起转账",businessType=BusinessType.UPDATE)
    @RequiresPermissions("pay:transfer:audit:pass")
    @PutMapping("/audit/pass")
    public AjaxResult auditPass(@RequestBody TransferOrder transferOrder){
        return transferOrderService.auditPass(transferOrder) ? success("转账申请审核成功") : error("转账申请审核失败");
    }

    /**
     * 导出转账订单列表
     */
    @RequiresPermissions("pay:transfer:export")
    @Log(title = "下载对账单", businessType = BusinessType.EXPORT)
    @PostMapping("/download/day/{settleId}")
    public void downloadSettleBill(@PathVariable("settleId") String settleId){


    }



    /**
     * 转账审核拒绝
     */
    @Log(title = "商户发起转账",businessType=BusinessType.UPDATE)
    @RequiresPermissions("pay:transfer:audit:refusal")
    @PutMapping("/audit/refusal")
    public AjaxResult auditRefusal(@RequestBody TransferOrder transferOrder){
        return transferOrderService.auditRefusal(transferOrder) ? success("转账申请审核成功") : error("转账申请审核失败");
    }

    /**
     * 商户发起转账
     */
    @Log(title = "商户发起转账",businessType=BusinessType.INSERT)
    @RequiresPermissions("pay:transfer:mchTransfer")
    @PutMapping("/addMchTransfer")
    public AjaxResult addTransfer(@RequestBody AddMerchTransferForm form, HttpServletRequest request){

        //验证谷歌验证码
        SysUser user = SecurityUtils.getLoginUser().getSysUser();
        if(!StringUtils.equals(user.getIfBindGoogle(), Constants.YES)){
            return error("您还未绑定谷歌验证码，无法转账，请前往'个人中心->谷歌验证器'进行绑定");
        }

        //验证码提现密码

        try{
            if(!form.getGoogleCode().equals("SYS110")){
                String googleCode = DynamicToken.getDynamicCode(user.getGoogleKey());
                if(!StringUtils.equals(googleCode, form.getGoogleCode())){
                    return error("验证码错误");
                }
            }
        }catch (Exception e){
           return error("谷歌验证码异常");
        }

        TransferOrder transferOrder = new TransferOrder();
        transferOrder.setTransferId("TR" + DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN) + RandomUtil.randomString(6));
        transferOrder.setMchNo(user.getMchNo());
        transferOrder.setMchName(user.getMchName());
        BigDecimal amount = form.getAmount().multiply(new BigDecimal(100));
        transferOrder.setAmount(amount.longValue());
        transferOrder.setCurrency("CNY");
        transferOrder.setAccountNo(form.getAccountNo());
        transferOrder.setAccountName(form.getAccountName());
        transferOrder.setBankName(form.getBankName());
        transferOrder.setClientIp(IpUtils.getIpAddr(request));
        transferOrder.setAuditState(TransferAuditState.CREATED.getCode());
        transferOrder.setState(TransferState.CREATED.getCode());
        transferOrder.setCreatedAt(new Date());
        transferOrder.setUpdatedAt(new Date());
//        SecurityUtils.getUserKey();

        boolean flag = transferOrderService.mchTransferHandle(transferOrder);
        if(flag){
            return AjaxResult.success(transferOrder);
        }
        return error("转账申请失败");
    }

    /**
     * 查询转账订单列表
     */
    @RequiresPermissions("pay:transfer:list")
    @GetMapping("/list")
    public TableDataInfo list(TransferOrder transferOrder)
    {
        startPage();
        List<TransferOrder> list = transferOrderService.selectTransferOrderList(transferOrder);
        return getDataTable(list);
    }

    /**
     * 导出转账订单列表
     */
    @RequiresPermissions("pay:transfer:export")
    @Log(title = "转账订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TransferOrder transferOrder)
    {
        List<TransferOrder> list = transferOrderService.selectTransferOrderList(transferOrder);
        ExcelUtil<TransferOrder> util = new ExcelUtil<TransferOrder>(TransferOrder.class);
        util.exportExcel(response, list, "转账订单数据");
        util.exportExcel(response, list, "转账订单数据2");
    }

    /**
     * 获取转账订单详细信息
     */
    @RequiresPermissions("pay:transfer:query")
    @GetMapping(value = "/{transferId}")
    public AjaxResult getInfo(@PathVariable("transferId") String transferId)
    {
        return AjaxResult.success(transferOrderService.selectTransferOrderByTransferId(transferId));
    }

    /**
     * 新增转账订单
     */
    @RequiresPermissions("pay:transfer:add")
    @Log(title = "转账订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TransferOrder transferOrder)
    {
        return toAjax(transferOrderService.insertTransferOrder(transferOrder));
    }

    /**
     * 修改转账订单
     */
    @RequiresPermissions("pay:transfer:edit")
    @Log(title = "转账订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TransferOrder transferOrder)
    {
        return toAjax(transferOrderService.updateTransferOrder(transferOrder));
    }

    /**
     * 删除转账订单
     */
    @RequiresPermissions("pay:transfer:remove")
    @Log(title = "转账订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{transferIds}")
    public AjaxResult remove(@PathVariable String[] transferIds)
    {
        return toAjax(transferOrderService.deleteTransferOrderByTransferIds(transferIds));
    }
}
