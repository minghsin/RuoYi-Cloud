package com.comm.pay.service;

import java.util.List;
import com.comm.pay.domain.TransferOrder;
import org.springframework.transaction.annotation.Transactional;

/**
 * 转账订单Service接口
 * 
 * @author ruoyi
 * @date 2022-07-03
 */
public interface ITransferOrderService 
{

    /**
     * 审核拒绝
     * @param auditTransfer
     * @return
     */
    boolean auditRefusal(TransferOrder auditTransfer);
    boolean auditPass(TransferOrder auditTransfer);

    /**
     * 商户转账
     * @param transferOrder
     * @return
     */
    boolean mchTransferHandle(TransferOrder transferOrder);

    /**
     * 查询转账订单
     * 
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    public TransferOrder selectTransferOrderByTransferId(String transferId);

    /**
     * 查询转账订单列表
     * 
     * @param transferOrder 转账订单
     * @return 转账订单集合
     */
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder);

    /**
     * 新增转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int insertTransferOrder(TransferOrder transferOrder);

    /**
     * 修改转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int updateTransferOrder(TransferOrder transferOrder);

    /**
     * 批量删除转账订单
     * 
     * @param transferIds 需要删除的转账订单主键集合
     * @return 结果
     */
    public int deleteTransferOrderByTransferIds(String[] transferIds);

    /**
     * 删除转账订单信息
     * 
     * @param transferId 转账订单主键
     * @return 结果
     */
    public int deleteTransferOrderByTransferId(String transferId);
}
