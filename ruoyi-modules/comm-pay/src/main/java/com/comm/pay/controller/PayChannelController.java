package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayChannel;
import com.comm.pay.service.IPayChannelService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 渠道信息Controller
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
@RestController
@RequestMapping("/channel")
public class PayChannelController extends BaseController
{
    @Autowired
    private IPayChannelService payChannelService;

    /**
     * 查询渠道信息列表
     */
    @RequiresPermissions("pay:channel:list")
    @GetMapping("/list")
    public TableDataInfo list(PayChannel payChannel)
    {
        startPage();
        List<PayChannel> list = payChannelService.selectPayChannelList(payChannel);
        return getDataTable(list);
    }

    /**
     * 导出渠道信息列表
     */
    @RequiresPermissions("pay:channel:export")
    @Log(title = "渠道信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayChannel payChannel)
    {
        List<PayChannel> list = payChannelService.selectPayChannelList(payChannel);
        ExcelUtil<PayChannel> util = new ExcelUtil<PayChannel>(PayChannel.class);
        util.exportExcel(response, list, "渠道信息数据");
    }

    /**
     * 获取渠道信息详细信息
     */
    @RequiresPermissions("pay:channel:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payChannelService.selectPayChannelById(id));
    }

    /**
     * 新增渠道信息
     */
    @RequiresPermissions("pay:channel:add")
    @Log(title = "渠道信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayChannel payChannel)
    {
        return toAjax(payChannelService.insertPayChannel(payChannel));
    }

    /**
     * 修改渠道信息
     */
    @RequiresPermissions("pay:channel:edit")
    @Log(title = "渠道信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayChannel payChannel)
    {
        return toAjax(payChannelService.updatePayChannel(payChannel));
    }

    /**
     * 删除渠道信息
     */
    @RequiresPermissions("pay:channel:remove")
    @Log(title = "渠道信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payChannelService.deletePayChannelByIds(ids));
    }
}
