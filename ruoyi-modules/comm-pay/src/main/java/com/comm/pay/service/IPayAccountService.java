package com.comm.pay.service;

import java.math.BigDecimal;
import java.util.List;
import com.comm.pay.domain.PayAccount;
import org.springframework.transaction.annotation.Transactional;

/**
 * 账户信息Service接口
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
public interface IPayAccountService 
{

    /**
     * 将代付中余额变更为已结清
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    boolean subtractPayforToSettle(String accountUid, String accountType,
                                   String orderNo, String orderType, BigDecimal amount);

    /**
     * 将代付中余额退回至可用余额
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    public boolean subtractPayforToBalance(String accountUid, String accountType,
                                           String orderNo, String orderType, BigDecimal amount);

    /**
     * 减少余额到代付中余额
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    public boolean subtractBalanceTopayfor(String accountUid, String accountType,
                                           String orderNo, String orderType, BigDecimal amount);

    /**
     * 查询账户信息
     * @param uid
     * @param accountType
     * @return
     */
    PayAccount selectPayAccountByAccountUid(String uid,String accountType);

    /**
     *  账户操作
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    boolean addBalance(String accountUid, String accountType,
                              String orderNo, String orderType, BigDecimal amount);
    /**
     * 查询账户信息
     * 
     * @param id 账户信息主键
     * @return 账户信息
     */
    public PayAccount selectPayAccountById(Long id);

    /**
     * 查询账户信息列表
     * 
     * @param payAccount 账户信息
     * @return 账户信息集合
     */
    public List<PayAccount> selectPayAccountList(PayAccount payAccount);

    /**
     * 新增账户信息
     * 
     * @param payAccount 账户信息
     * @return 结果
     */
    public int insertPayAccount(PayAccount payAccount);

    /**
     * 修改账户信息
     * 
     * @param payAccount 账户信息
     * @return 结果
     */
    public int updatePayAccount(PayAccount payAccount);

    /**
     * 批量删除账户信息
     * 
     * @param ids 需要删除的账户信息主键集合
     * @return 结果
     */
    public int deletePayAccountByIds(Long[] ids);

    /**
     * 删除账户信息信息
     * 
     * @param id 账户信息主键
     * @return 结果
     */
    public int deletePayAccountById(Long id);
}
