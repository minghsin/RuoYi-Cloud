package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayAccountHistory;
import com.comm.pay.service.IPayAccountHistoryService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 账户记录历史Controller
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
@RestController
@RequestMapping("/history")
public class PayAccountHistoryController extends BaseController
{
    @Autowired
    private IPayAccountHistoryService payAccountHistoryService;

    /**
     * 查询账户记录历史列表
     */
    @RequiresPermissions("pay:history:list")
    @GetMapping("/list")
    public TableDataInfo list(PayAccountHistory payAccountHistory)
    {
        startPage();
        List<PayAccountHistory> list = payAccountHistoryService.selectPayAccountHistoryList(payAccountHistory);
        return getDataTable(list);
    }

    /**
     * 导出账户记录历史列表
     */
    @RequiresPermissions("pay:history:export")
    @Log(title = "账户记录历史", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayAccountHistory payAccountHistory)
    {
        List<PayAccountHistory> list = payAccountHistoryService.selectPayAccountHistoryList(payAccountHistory);
        ExcelUtil<PayAccountHistory> util = new ExcelUtil<PayAccountHistory>(PayAccountHistory.class);
        util.exportExcel(response, list, "账户记录历史数据");
    }

    /**
     * 获取账户记录历史详细信息
     */
    @RequiresPermissions("pay:history:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payAccountHistoryService.selectPayAccountHistoryById(id));
    }

    /**
     * 新增账户记录历史
     */
    @RequiresPermissions("pay:history:add")
    @Log(title = "账户记录历史", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayAccountHistory payAccountHistory)
    {
        return toAjax(payAccountHistoryService.insertPayAccountHistory(payAccountHistory));
    }

    /**
     * 修改账户记录历史
     */
    @RequiresPermissions("pay:history:edit")
    @Log(title = "账户记录历史", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayAccountHistory payAccountHistory)
    {
        return toAjax(payAccountHistoryService.updatePayAccountHistory(payAccountHistory));
    }

    /**
     * 删除账户记录历史
     */
    @RequiresPermissions("pay:history:remove")
    @Log(title = "账户记录历史", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payAccountHistoryService.deletePayAccountHistoryByIds(ids));
    }
}
