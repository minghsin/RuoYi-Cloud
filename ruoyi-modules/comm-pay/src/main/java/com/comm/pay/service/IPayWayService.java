package com.comm.pay.service;

import java.util.List;
import com.comm.pay.domain.PayWay;

/**
 * 支付方式Service接口
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
public interface IPayWayService 
{
    /**
     * 查询支付方式
     * 
     * @param wayId 支付方式主键
     * @return 支付方式
     */
    public PayWay selectPayWayByWayId(Long wayId);

    /**
     * 查询支付方式
     *
     * @param wayCode 支付方式主键
     * @return 支付方式
     */
    PayWay selectPayWayByWayCode(String wayCode);

    /**
     * 查询支付方式列表
     * 
     * @param payWay 支付方式
     * @return 支付方式集合
     */
    public List<PayWay> selectPayWayList(PayWay payWay);

    /**
     * 新增支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    public int insertPayWay(PayWay payWay);

    /**
     * 修改支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    public int updatePayWay(PayWay payWay);

    /**
     * 批量删除支付方式
     * 
     * @param wayIds 需要删除的支付方式主键集合
     * @return 结果
     */
    public int deletePayWayByWayIds(Long[] wayIds);

    /**
     * 删除支付方式信息
     * 
     * @param wayId 支付方式主键
     * @return 结果
     */
    public int deletePayWayByWayId(Long wayId);
}
