package com.comm.pay.api.controller;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.domain.MchInfo;
import com.comm.pay.api.dto.CreateOrderReqDto;
import com.comm.pay.api.dto.TransferReqDto;
import com.comm.pay.api.dto.TransferRespDto;
import com.comm.pay.domain.TransferOrder;
import com.comm.pay.service.IPayOrderService;
import com.comm.pay.service.ITransferOrderService;
import com.ruoyi.common.core.enums.TransferAuditState;
import com.ruoyi.common.core.enums.TransferState;
import com.ruoyi.common.core.utils.ip.IpUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/transfer")
public class TransferController extends ApiBaseController  {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IPayOrderService payOrderService;

    @Autowired
    private ITransferOrderService transferOrderService;

    @PostMapping("/single/apple")
    public AjaxResult createOrder(@Valid @RequestBody TransferReqDto reqDto, HttpServletRequest request){

        logger.info("商户提现请求参数：{}", reqDto);

        MchInfo mchInfo = new MchInfo();
        mchInfo.setMchNo(reqDto.getMchNo());
        MchApp mchApp = new MchApp();
        mchApp.setAppId(reqDto.getAppId());

        //验证签名
        validSign(reqDto,mchInfo, mchApp);

        //查询订单是否存在
        TransferOrder queryTransfer = new TransferOrder();
        queryTransfer.setMchOrderNo(reqDto.getMchOrderNo());
        if(!CollectionUtil.isEmpty(transferOrderService.selectTransferOrderList(queryTransfer))){
            return error("商户订单["+reqDto.getMchOrderNo()+"]已存在，请勿重复提交");
        }

        TransferOrder transferOrder = new TransferOrder();

        transferOrder.setTransferId("TR" + DateUtil.format(new Date(), DatePattern.PURE_DATETIME_PATTERN) + RandomUtil.randomString(6));
        transferOrder.setMchNo(mchInfo.getMchNo());
        transferOrder.setAppId(mchApp.getAppId());
        transferOrder.setMchOrderNo(reqDto.getMchOrderNo());
        transferOrder.setMchName(mchInfo.getMchName());
        transferOrder.setAmount(reqDto.getAmount().longValue());
        transferOrder.setCurrency("CNY");
        transferOrder.setAccountNo(reqDto.getAccountNo());
        transferOrder.setAccountName(reqDto.getAccountName());
        transferOrder.setBankName(reqDto.getBankName());
        transferOrder.setClientIp(IpUtils.getIpAddr(request));
        transferOrder.setAuditState(TransferAuditState.CREATED.getCode());
        transferOrder.setState(TransferState.CREATED.getCode());
        transferOrder.setCreatedAt(new Date());
        transferOrder.setUpdatedAt(new Date());
        transferOrder.setNotifyUrl(reqDto.getNotifyUrl());

        boolean flag = transferOrderService.mchTransferHandle(transferOrder);
        if(!flag){
           return error("商户转账申请处理失败");
        }

        TransferOrder querOrder = transferOrderService.selectTransferOrderByTransferId(transferOrder.getTransferId());
        TransferRespDto respDto = new TransferRespDto();
        BeanUtils.copyProperties(querOrder, respDto);

        return AjaxResult.success(respDto);
    }

    @PostMapping("/query")
    public AjaxResult queryOrder(@Valid @RequestBody TransferReqDto reqDto){

        logger.info("商户提现查询参数：{}", reqDto);

        MchInfo mchInfo = new MchInfo();
        mchInfo.setMchNo(reqDto.getMchNo());
        MchApp mchApp = new MchApp();
        mchApp.setAppId(reqDto.getAppId());

        //验证签名
        validSign(reqDto,mchInfo, mchApp);

        TransferOrder transferOrder = new TransferOrder();
        transferOrder.setMchOrderNo(reqDto.getMchOrderNo());
        transferOrder.setTransferId(reqDto.getTransferId());
        List<TransferOrder> transferList = transferOrderService.selectTransferOrderList(transferOrder);
        if(CollectionUtil.isEmpty(transferList)){
            return error("转账订单不存在");
        }

        TransferRespDto respDto = new TransferRespDto();
        BeanUtils.copyProperties(transferList.get(0), respDto);

        return AjaxResult.success(respDto);

    }


}
