package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayWay;
import com.comm.pay.service.IPayWayService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 支付方式Controller
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
@RestController
@RequestMapping("/payWay")
public class PayWayController extends BaseController
{
    @Autowired
    private IPayWayService payWayService;

    /**
     * 查询支付方式列表
     */
    @GetMapping("/list")
    public TableDataInfo list(PayWay payWay)
    {
        startPage();
        List<PayWay> list = payWayService.selectPayWayList(payWay);
        return getDataTable(list);
    }

    /**
     * 导出支付方式列表
     */
    @RequiresPermissions("pay:payWay:export")
    @Log(title = "支付方式", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayWay payWay)
    {
        List<PayWay> list = payWayService.selectPayWayList(payWay);
        ExcelUtil<PayWay> util = new ExcelUtil<PayWay>(PayWay.class);
        util.exportExcel(response, list, "支付方式数据");
    }

    /**
     * 获取支付方式详细信息
     */
    @RequiresPermissions("pay:payWay:query")
    @GetMapping(value = "/{wayId}")
    public AjaxResult getInfo(@PathVariable("wayId") Long wayId)
    {
        return AjaxResult.success(payWayService.selectPayWayByWayId(wayId));
    }

    /**
     * 新增支付方式
     */
    @RequiresPermissions("pay:payWay:add")
    @Log(title = "支付方式", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayWay payWay)
    {
        return toAjax(payWayService.insertPayWay(payWay));
    }

    /**
     * 修改支付方式
     */
    @RequiresPermissions("pay:payWay:edit")
    @Log(title = "支付方式", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayWay payWay)
    {
        return toAjax(payWayService.updatePayWay(payWay));
    }


    /**
     * 获取商户的支付方式
     */
    @RequiresPermissions("pay:payWay:mchno")
    @DeleteMapping("/{mchNo}")
    public AjaxResult getPayWayByMchNo(@PathVariable String mchNo){

        return AjaxResult.success();
    }

}
