package com.comm.pay.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 渠道配置对象 t_pay_channel_config
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
public class PayChannelConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 渠道编号 */
    @Excel(name = "渠道编号")
    private Long channelId;

    /**
     * 渠道名称
     */
    private String chnlName;

    /** 是否支持提现 */
    @Excel(name = "是否支持提现")
    private String allowWithdraw;

    /** 交易开启时间 */
    @Excel(name = "交易开启时间", width = 30, dateFormat = "yyyy-MM-dd")
    private String exchangeBeginTime;

    /** 交易关闭时间 */
    @Excel(name = "交易关闭时间", width = 30, dateFormat = "yyyy-MM-dd")
    private String exchangeEndTime;

    /** api签名方式 */
    @Excel(name = "api签名方式")
    private String signType;

    /** 下单地址 */
    @Excel(name = "下单地址")
    private String createOrderUrl;

    /** 查询地址 */
    @Excel(name = "查询地址")
    private String queryOrderUrl;

    /** 发起提现地址 */
    @Excel(name = "发起提现地址")
    private String createWithdrawUrl;

    /** 查询提现地址 */
    @Excel(name = "查询提现地址")
    private String queryWithdrawUrl;

    /** 签名密钥 */
    @Excel(name = "签名密钥")
    private String signKey;

    /** RSA渠道签名公钥 */
    @Excel(name = "RSA渠道签名公钥")
    private String rsaChnlPublicKey;

    /** RSA系统公钥 */
    @Excel(name = "RSA系统公钥")
    private String rsaPublicKey;

    /** RSA 系统密钥 */
    @Excel(name = "RSA 系统密钥")
    private String rsaPrivateKey;

    /** 渠道回调白名单 */
    @Excel(name = "渠道回调白名单")
    private String callbackIpWhitelist;

    /** 渠道商户号 */
    @Excel(name = "渠道商户号")
    private String chnlMchNo;

    /** 渠道应用编号 */
    @Excel(name = "渠道应用编号")
    private String chnlAppId;

    private String active;

    /**
     * 代码路径或者bean名称
     */
    private String beanClass;

    private String returnText;

    public String getReturnText() {
        return returnText;
    }

    public void setReturnText(String returnText) {
        this.returnText = returnText;
    }

    public String getBeanClass() {
        return beanClass;
    }

    public void setBeanClass(String beanClass) {
        this.beanClass = beanClass;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChannelId(Long channelId) 
    {
        this.channelId = channelId;
    }

    public Long getChannelId() 
    {
        return channelId;
    }
    public void setAllowWithdraw(String allowWithdraw) 
    {
        this.allowWithdraw = allowWithdraw;
    }

    public String getAllowWithdraw() 
    {
        return allowWithdraw;
    }

    public String getExchangeBeginTime() {
        return exchangeBeginTime;
    }

    public void setExchangeBeginTime(String exchangeBeginTime) {
        this.exchangeBeginTime = exchangeBeginTime;
    }

    public String getExchangeEndTime() {
        return exchangeEndTime;
    }

    public void setExchangeEndTime(String exchangeEndTime) {
        this.exchangeEndTime = exchangeEndTime;
    }

    public void setSignType(String signType)
    {
        this.signType = signType;
    }

    public String getSignType() 
    {
        return signType;
    }
    public void setCreateOrderUrl(String createOrderUrl) 
    {
        this.createOrderUrl = createOrderUrl;
    }

    public String getCreateOrderUrl() 
    {
        return createOrderUrl;
    }
    public void setQueryOrderUrl(String queryOrderUrl) 
    {
        this.queryOrderUrl = queryOrderUrl;
    }

    public String getQueryOrderUrl() 
    {
        return queryOrderUrl;
    }
    public void setCreateWithdrawUrl(String createWithdrawUrl) 
    {
        this.createWithdrawUrl = createWithdrawUrl;
    }

    public String getCreateWithdrawUrl() 
    {
        return createWithdrawUrl;
    }
    public void setQueryWithdrawUrl(String queryWithdrawUrl) 
    {
        this.queryWithdrawUrl = queryWithdrawUrl;
    }

    public String getQueryWithdrawUrl() 
    {
        return queryWithdrawUrl;
    }
    public void setSignKey(String signKey) 
    {
        this.signKey = signKey;
    }

    public String getSignKey() 
    {
        return signKey;
    }
    public void setRsaChnlPublicKey(String rsaChnlPublicKey) 
    {
        this.rsaChnlPublicKey = rsaChnlPublicKey;
    }

    public String getRsaChnlPublicKey() 
    {
        return rsaChnlPublicKey;
    }
    public void setRsaPublicKey(String rsaPublicKey) 
    {
        this.rsaPublicKey = rsaPublicKey;
    }

    public String getRsaPublicKey() 
    {
        return rsaPublicKey;
    }
    public void setRsaPrivateKey(String rsaPrivateKey) 
    {
        this.rsaPrivateKey = rsaPrivateKey;
    }

    public String getRsaPrivateKey() 
    {
        return rsaPrivateKey;
    }
    public void setCallbackIpWhitelist(String callbackIpWhitelist) 
    {
        this.callbackIpWhitelist = callbackIpWhitelist;
    }

    public String getCallbackIpWhitelist() 
    {
        return callbackIpWhitelist;
    }
    public void setChnlMchNo(String chnlMchNo) 
    {
        this.chnlMchNo = chnlMchNo;
    }

    public String getChnlMchNo() 
    {
        return chnlMchNo;
    }
    public void setChnlAppId(String chnlAppId) 
    {
        this.chnlAppId = chnlAppId;
    }

    public String getChnlAppId() 
    {
        return chnlAppId;
    }

    public String getChnlName() {
        return chnlName;
    }

    public void setChnlName(String chnlName) {
        this.chnlName = chnlName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("channelId", getChannelId())
            .append("allowWithdraw", getAllowWithdraw())
            .append("exchangeBeginTime", getExchangeBeginTime())
            .append("exchangeEndTime", getExchangeEndTime())
            .append("signType", getSignType())
            .append("createOrderUrl", getCreateOrderUrl())
            .append("queryOrderUrl", getQueryOrderUrl())
            .append("createWithdrawUrl", getCreateWithdrawUrl())
            .append("queryWithdrawUrl", getQueryWithdrawUrl())
            .append("signKey", getSignKey())
            .append("rsaChnlPublicKey", getRsaChnlPublicKey())
            .append("rsaPublicKey", getRsaPublicKey())
            .append("rsaPrivateKey", getRsaPrivateKey())
            .append("callbackIpWhitelist", getCallbackIpWhitelist())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .append("chnlMchNo", getChnlMchNo())
            .append("chnlAppId", getChnlAppId())
            .toString();
    }
}
