package com.comm.pay.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.comm.merch.api.RemoteMerchService;
import com.comm.pay.api.channel.ChannelFactory;
import com.comm.pay.api.channel.IPayChnlService;
import com.comm.pay.domain.*;
import com.comm.pay.service.*;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.enums.AccountOrderType;
import com.ruoyi.common.core.enums.AccountUidType;
import com.ruoyi.common.core.enums.PayState;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.datascope.annotation.DataScope;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayOrderMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付订单Service业务层处理
 *
 * @author ruoyi
 * @date 2022-06-10
 */
@Service
public class PayOrderServiceImpl implements IPayOrderService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private PayOrderMapper payOrderMapper;

    @Autowired
    private IPayRateService payRateService;

    @Autowired
    private IPayWayService payWayService;

    @Autowired
    private IPayAccountService payAccountService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private IPayChannelWayService payChannelWayService;

    @Autowired
    private IPayChannelConfigService payChannelConfigService;

    @Autowired
    private IPayChannelService payChannelService;

    @Autowired
    private ChannelFactory channelFactory;

    @Override
    public PayOrder repayOrder(PayOrder payOrder){

        // 渠道支付代码
        PayChannelWay payChannelWay = null;

        if(StringUtils.isNotEmpty(payOrder.getChannelId())){

            PayChannelWay queryChnlWay = new PayChannelWay();
            queryChnlWay.setChannelId(Long.valueOf(payOrder.getChannelId()));
            queryChnlWay.setPayType(payOrder.getChannelPayCode());
            List<PayChannelWay> payChannelWayList = payChannelWayService.selectPayChannelWayList(queryChnlWay);
            if(payChannelWayList.isEmpty()){
                throw new RuntimeException("暂无可用支付渠道");
            }
            payChannelWay = payChannelWayList.get(0);
        }else{
            List<PayChannelWay> allChnlPayWay = payChannelWayService.getChannelWayCacheByAmount(payOrder.getWayCode(), payOrder.getRealAmount());
            logger.info("可用通道：{}", allChnlPayWay);
            if(CollectionUtil.isEmpty(allChnlPayWay)){
                throw new RuntimeException("暂无可用支付渠道");
            }

            //随机一个可用的支付渠道与编码
            payChannelWay = randomPayChannel(allChnlPayWay);
        }

        if(payChannelWay == null){
            throw new RuntimeException("当前渠道未开启");
        }

        PayChannel payChannel = payChannelService.selectPayChannelById(payChannelWay.getChannelId());


        payOrder.setChannelId(String.valueOf(payChannel.getId()));
        payOrder.setChannelName(payChannel.getCnlName());
        payOrder.setChannelPayCode(payChannelWay.getPayType());

        IPayChnlService payChnlService = channelFactory.getPayChnl(payChannel.getId());
        payChnlService.createOrder(payOrder);
        if(!StringUtils.equals(payOrder.getState(), PayState.PAYING.getCode())){
            logger.error("生成支付地址错误:{}[{}]",payOrder.getErrMsg(),payOrder.getErrCode());
            this.updatePayOrder(payOrder);
            throw new RuntimeException("生成支付地址错误:" + payOrder.getErrMsg());
        }else{
            this.updatePayOrder(payOrder);
        }
        return payOrder;
    }

    private  PayChannelWay randomPayChannel(List<PayChannelWay> allChnlPayWay){

        if(CollectionUtil.isEmpty(allChnlPayWay)){
            return null;
        }

        PayChannelWay chnlPayWay = allChnlPayWay.get(RandomUtil.randomInt(allChnlPayWay.size()));

         PayChannel payChannel = payChannelService.selectPayChannelById(chnlPayWay.getChannelId());
        int channelId = payChannel.getId().intValue();
        if(!StringUtils.equals(payChannel.getState(),"01")){

            List<PayChannelWay> fiterList = allChnlPayWay.stream().filter(item -> item.getChannelId().intValue() != channelId).collect(Collectors.toList());
            return randomPayChannel(fiterList);
        }
        return chnlPayWay;
    }

    /**
     * 手工处理订单
     * @param payOrder
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean manualReplenishment(PayOrder payOrder){

        BigDecimal amount = payOrder.getAmount().subtract(payOrder.getMchFeeAmount());

        boolean addBalanceFlag = payAccountService.addBalance(payOrder.getMchNo(), AccountUidType.MCH_TYPE.getCode(),
                payOrder.getPayOrderId(), AccountOrderType.PAY_ORDER.getCode() , amount);

        payOrder.setState(PayState.SUCCESS.getCode());
        payOrder.setUpdatedAt(new Date());
        payOrder.setUpdateBy(SecurityUtils.getUsername());
        payOrder.setSuccessTime(new Date());
        boolean updateOrderFlag = updatePayOrder(payOrder) > 0;
        if(!addBalanceFlag || !updateOrderFlag){
            throw new RuntimeException("订单处理异常");
        }


        return Boolean.TRUE;
    }

    @Override
    public PayOrder createOrder(PayOrder payOrder) {

        PayRate queryPayRate = new PayRate();
        queryPayRate.setMchNo(payOrder.getMchNo());
        queryPayRate.setWayCode(payOrder.getWayCode());
        queryPayRate.setActive(Constants.YES);
        List<PayRate> payRateList = payRateService.selectPayRateList(queryPayRate);
        if (CollectionUtil.isEmpty(payRateList)) {
            throw new RuntimeException("商户支付方式【" + payOrder.getWayCode() + "】未开通");
        }
        PayRate payRate = payRateList.get(0);
        // region 计算费率
        BigDecimal fee = BigDecimal.ZERO;
        if (StringUtils.equals(payRate.getRateType(), "01")) {
            fee = payRate.getRate();
        } else {
            fee = caluMchFee(payOrder.getAmount(), payRate.getRate());
        }
        payOrder.setMchFeeRate(payRate.getRate());
        payOrder.setMchFeeAmount(fee);
        //endregion
        //设置订单失效时间
        payOrder.setExpiredTime(DateUtils.addMinutes(new Date(),Constants.EXPIRED_TIME));
        //设置真实支付金额
        payOrder.setRealAmount(payOrder.getAmount());

        payOrder.setWayName(payWayService.selectPayWayByWayCode(payOrder.getWayCode()).getWayName());
        payOrder.setCreatedAt(new Date());
        payOrder.setUpdatedAt(new Date());
        boolean saveFlag = insertPayOrder(payOrder) > 0;
        if(saveFlag){
            return selectPayOrderByPayOrderId(payOrder.getPayOrderId());
        }
        return null;
    }

    /**
     * 计算手续费
     *
     * @param amount 单位分
     * @param rate   百分比 例如 20% 传入 20
     * @return
     */
    private BigDecimal caluMchFee(BigDecimal amount, BigDecimal rate) {

        //转换单位
        BigDecimal newAmount = amount.divide(new BigDecimal("100"));
        BigDecimal newRate = rate.divide(new BigDecimal("100"));
        return newAmount.multiply(newRate).multiply(new BigDecimal("100")).setScale(0, BigDecimal.ROUND_DOWN);
    }

    /**
     * 统计支付成功订单
     *
     * @param payOrder
     * @return
     */
    @Override
    @DataScope(dataType = "BUS")
    public PayOrderSum statisticOrders(PayOrder payOrder) {
        return payOrderMapper.statisticOrders(payOrder);
    }

    /**
     * 查询支付订单
     *
     * @param payOrderId 支付订单主键
     * @return 支付订单
     */
    @Override
    public PayOrder selectPayOrderByPayOrderId(String payOrderId) {
        return payOrderMapper.selectPayOrderByPayOrderId(payOrderId);
    }

    /**
     * 根据商户订单号查询支付订单
     *
     * @param payOrderId 支付订单主键
     * @return 支付订单
     */
    @Override
    public PayOrder selectPayOrderByMchOrderId(String mchOrderNo) {
        return payOrderMapper.selectPayOrderByMchOrderId(mchOrderNo);
    }

    /**
     * 根据商户订单号判断是否存在
     * @param mchOrderNo
     * @return
     */
    public boolean ifExistByMchOrderNo(String mchOrderNo){
        return payOrderMapper.ifExistByMchOrderNo(mchOrderNo) > 0;
    }

    /**
     * 查询支付订单列表
     *
     * @param payOrder 支付订单
     * @return 支付订单
     */
    @Override
    @DataScope(dataType="BUS")
    public List<PayOrder> selectPayOrderList(PayOrder payOrder) {
        return payOrderMapper.selectPayOrderList(payOrder);
    }

    /**
     * 新增支付订单
     *
     * @param payOrder 支付订单
     * @return 结果
     */
    @Override
    public int insertPayOrder(PayOrder payOrder) {
        return payOrderMapper.insertPayOrder(payOrder);
    }

    /**
     * 修改支付订单
     *
     * @param payOrder 支付订单
     * @return 结果
     */
    @Override
    public int updatePayOrder(PayOrder payOrder) {
        return payOrderMapper.updatePayOrder(payOrder);
    }

    /**
     * 批量删除支付订单
     *
     * @param payOrderIds 需要删除的支付订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByPayOrderIds(String[] payOrderIds) {
        return payOrderMapper.deletePayOrderByPayOrderIds(payOrderIds);
    }

    /**
     * 删除支付订单信息
     *
     * @param payOrderId 支付订单主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByPayOrderId(String payOrderId) {
        return payOrderMapper.deletePayOrderByPayOrderId(payOrderId);
    }
}
