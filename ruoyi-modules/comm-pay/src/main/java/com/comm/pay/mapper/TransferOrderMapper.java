package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.TransferOrder;

/**
 * 转账订单Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-03
 */
public interface TransferOrderMapper 
{
    /**
     * 查询转账订单
     * 
     * @param transferId 转账订单主键
     * @return 转账订单
     */
    public TransferOrder selectTransferOrderByTransferId(String transferId);

    /**
     * 查询转账订单列表
     * 
     * @param transferOrder 转账订单
     * @return 转账订单集合
     */
    public List<TransferOrder> selectTransferOrderList(TransferOrder transferOrder);

    /**
     * 新增转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int insertTransferOrder(TransferOrder transferOrder);

    /**
     * 修改转账订单
     * 
     * @param transferOrder 转账订单
     * @return 结果
     */
    public int updateTransferOrder(TransferOrder transferOrder);

    /**
     * 删除转账订单
     * 
     * @param transferId 转账订单主键
     * @return 结果
     */
    public int deleteTransferOrderByTransferId(String transferId);

    /**
     * 批量删除转账订单
     * 
     * @param transferIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTransferOrderByTransferIds(String[] transferIds);
}
