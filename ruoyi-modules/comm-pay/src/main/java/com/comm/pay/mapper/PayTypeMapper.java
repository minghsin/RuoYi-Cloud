package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayType;

/**
 * 支付类型Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
public interface PayTypeMapper 
{
    /**
     * 查询支付类型
     * 
     * @param wayType 支付类型主键
     * @return 支付类型
     */
    public PayType selectPayTypeByWayType(String wayType);

    /**
     * 查询支付类型列表
     * 
     * @param payType 支付类型
     * @return 支付类型集合
     */
    public List<PayType> selectPayTypeList(PayType payType);

    /**
     * 新增支付类型
     * 
     * @param payType 支付类型
     * @return 结果
     */
    public int insertPayType(PayType payType);

    /**
     * 修改支付类型
     * 
     * @param payType 支付类型
     * @return 结果
     */
    public int updatePayType(PayType payType);

    /**
     * 删除支付类型
     * 
     * @param wayType 支付类型主键
     * @return 结果
     */
    public int deletePayTypeByWayType(String wayType);

    /**
     * 批量删除支付类型
     * 
     * @param wayTypes 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayTypeByWayTypes(String[] wayTypes);
}
