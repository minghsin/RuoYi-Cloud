package com.comm.pay.mapper;

import java.util.List;
import com.comm.pay.domain.PayChannelConfig;

/**
 * 渠道配置Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
public interface PayChannelConfigMapper 
{

    /**
     * 查询渠道配置信息
     * @param chnlId
     * @return
     */
    PayChannelConfig selectPayChnlConfigByChnlId(Long chnlId);

    /**
     * 查询渠道配置
     * 
     * @param id 渠道配置主键
     * @return 渠道配置
     */
    public PayChannelConfig selectPayChannelConfigById(Long id);

    /**
     * 查询渠道配置列表
     * 
     * @param payChannelConfig 渠道配置
     * @return 渠道配置集合
     */
    public List<PayChannelConfig> selectPayChannelConfigList(PayChannelConfig payChannelConfig);

    /**
     * 新增渠道配置
     * 
     * @param payChannelConfig 渠道配置
     * @return 结果
     */
    public int insertPayChannelConfig(PayChannelConfig payChannelConfig);

    /**
     * 修改渠道配置
     * 
     * @param payChannelConfig 渠道配置
     * @return 结果
     */
    public int updatePayChannelConfig(PayChannelConfig payChannelConfig);

    /**
     * 删除渠道配置
     * 
     * @param id 渠道配置主键
     * @return 结果
     */
    public int deletePayChannelConfigById(Long id);

    /**
     * 批量删除渠道配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayChannelConfigByIds(Long[] ids);
}
