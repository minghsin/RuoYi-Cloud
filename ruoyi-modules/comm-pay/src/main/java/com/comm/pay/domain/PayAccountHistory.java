package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 账户记录历史对象 t_pay_account_history
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
public class PayAccountHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 账户ID */
    @Excel(name = "账户ID")
    private Long accountId;

    private String accountUid;
    private String accountName;

    /** ADD 加款 SUB 减款 */
    @Excel(name = "ADD 加款 SUB 减款")
    private String type;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 订单类型 01 支付订单 02 提现订单 03 退款订单 04 系统调账订单 */
    @Excel(name = "订单类型 01 支付订单 02 提现订单 03 退款订单 04 系统调账订单")
    private String orderType;

    /** 操作的金额 */
    @Excel(name = "操作的金额")
    private BigDecimal amount;

    /** 操作后的余额 */
    @Excel(name = "操作后的余额")
    private BigDecimal balance;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }


    public String getAccountUid() {
        return accountUid;
    }

    public void setAccountUid(String accountUid) {
        this.accountUid = accountUid;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAccountId(Long accountId) 
    {
        this.accountId = accountId;
    }

    public Long getAccountId() 
    {
        return accountId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setOrderType(String orderType) 
    {
        this.orderType = orderType;
    }

    public String getOrderType() 
    {
        return orderType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("accountId", getAccountId())
            .append("type", getType())
            .append("orderNo", getOrderNo())
            .append("orderType", getOrderType())
            .append("amount", getAmount())
            .append("balance", getBalance())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
