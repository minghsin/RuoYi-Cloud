package com.comm.pay.api.dto;

import com.ruoyi.common.core.enums.TransferAuditState;
import com.ruoyi.common.core.enums.TransferState;
import com.ruoyi.common.core.utils.ip.IpUtils;

import java.util.Date;

public class TransferRespDto extends BaseReqDto{

    private String mchOrderNo;
    private Long amount;
    private String currency;
    private String accountNo;
    private String accountName;
    private String bankName;
    private String openBankName;
    private String auditState;
    private String state;
    private Date createAt;

    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getOpenBankName() {
        return openBankName;
    }

    public void setOpenBankName(String openBankName) {
        this.openBankName = openBankName;
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "TransferRespDto{" +
                ", mchOrderNo='" + mchOrderNo + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", accountName='" + accountName + '\'' +
                ", bankName='" + bankName + '\'' +
                ", openBankName='" + openBankName + '\'' +
                ", auditState='" + auditState + '\'' +
                ", state='" + state + '\'' +
                ", createAt=" + createAt +
                '}';
    }
}
