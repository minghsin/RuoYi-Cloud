package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayType;
import com.comm.pay.service.IPayTypeService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 支付类型Controller
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
@RestController
@RequestMapping("/payType")
public class PayTypeController extends BaseController
{
    @Autowired
    private IPayTypeService payTypeService;

    /**
     * 查询支付类型列表
     */
    @RequiresPermissions("payType:payType:list")
    @GetMapping("/list")
    public TableDataInfo list(PayType payType)
    {
        startPage();
        List<PayType> list = payTypeService.selectPayTypeList(payType);
        return getDataTable(list);
    }

    /**
     * 导出支付类型列表
     */
    @RequiresPermissions("payType:payType:export")
    @Log(title = "支付类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayType payType)
    {
        List<PayType> list = payTypeService.selectPayTypeList(payType);
        ExcelUtil<PayType> util = new ExcelUtil<PayType>(PayType.class);
        util.exportExcel(response, list, "支付类型数据");
    }

    /**
     * 获取支付类型详细信息
     */
    @RequiresPermissions("payType:payType:query")
    @GetMapping(value = "/{wayType}")
    public AjaxResult getInfo(@PathVariable("wayType") String wayType)
    {
        return AjaxResult.success(payTypeService.selectPayTypeByWayType(wayType));
    }

    /**
     * 新增支付类型
     */
    @RequiresPermissions("payType:payType:add")
    @Log(title = "支付类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayType payType)
    {
        return toAjax(payTypeService.insertPayType(payType));
    }

    /**
     * 修改支付类型
     */
    @RequiresPermissions("payType:payType:edit")
    @Log(title = "支付类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayType payType)
    {
        return toAjax(payTypeService.updatePayType(payType));
    }

}
