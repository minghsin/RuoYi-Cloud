package com.comm.pay.controller;

import java.math.BigDecimal;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.constant.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayRate;
import com.comm.pay.service.IPayRateService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 商户支付费率Controller
 *
 * @author ruoyi
 * @date 2022-06-10
 */
@RestController
@RequestMapping("/rate")
public class PayRateController extends BaseController {
    @Autowired
    private IPayRateService payRateService;

    /**
     * 导出商户支付费率列表
     */
    @RequiresPermissions("pay:rate:mch:save")
    @Log(title = "商户支付费率", businessType = BusinessType.INSERT)
    @PostMapping("/saveMchRate")
    public AjaxResult saveMchRate(@RequestBody  List<PayRate> mchRate) {

        return payRateService.saveMchRate(mchRate) ? AjaxResult.success() : AjaxResult.error("保存商户费率失败");
    }

    /**
     * 查询商户支付费率列表
     */
    @RequiresPermissions("pay:rate:list")
    @GetMapping("/list")
    public TableDataInfo list(PayRate payRate) {
        startPage();
        List<PayRate> list = payRateService.selectPayRateList(payRate);
        return getDataTable(list);
    }

    /**
     * 查询商户支付费率列表
     */
    @RequiresPermissions("pay:rate:mch:list")
    @GetMapping("/mch/{mchNo}")
    public TableDataInfo list(@PathVariable("mchNo") String mchNo) {
        List<PayRate> list = payRateService.selectPayRateListByMchNo(mchNo);
        return getDataTable(list);
    }

    /**
     * 导出商户支付费率列表
     */
    @RequiresPermissions("pay:rate:export")
    @Log(title = "商户支付费率", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayRate payRate) {
        List<PayRate> list = payRateService.selectPayRateList(payRate);
        ExcelUtil<PayRate> util = new ExcelUtil<PayRate>(PayRate.class);
        util.exportExcel(response, list, "商户支付费率数据");
    }

    /**
     * 获取商户支付费率详细信息
     */
    @RequiresPermissions("pay:rate:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(payRateService.selectPayRateById(id));
    }

    /**
     * 新增商户支付费率
     */
    @RequiresPermissions("pay:rate:add")
    @Log(title = "商户支付费率", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayRate payRate) {
        return toAjax(payRateService.insertPayRate(payRate));
    }

    /**
     * 修改商户支付费率
     */
    @RequiresPermissions("pay:rate:edit")
    @Log(title = "商户支付费率", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayRate payRate) {
        return toAjax(payRateService.updatePayRate(payRate));
    }

}
