package com.comm.pay.api.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author zhengmingxin
 * @Description
 * @time 2020-10-05 12:13 AM
 */
public class TransferReqDto extends BaseReqDto {
    /**
     * 商户订单号
     */
    @NotNull(message = "商户订单号不能为空")
    private String mchOrderNo;

    /**
     * 开户银行
     */
    @NotNull(message = "银行名称不能为空")
    private String bankName;
    /**
     * 开户账号
     */
    @NotNull(message = "收款账号不能为空")
    private String accountNo;
    /**
     * 开户名
     */
    @NotNull(message = "收款人姓名不能为空")
    private String accountName;
    /**
     * 代付金额
     */
    @NotNull(message = "代付金额不能为空")
    private BigDecimal amount;
    /**
     * 开户支行
     */
    @NotNull(message = "开户支行不能为空")
    private String openBankName;
    /**
     * 省份
     */
    private String province;
    /**
     * 城市名称
     */
    private String cityName;
    /**
     * 账户属性  1对私 2对公
     */
    @NotNull(message = "账户属性不能为空")
    private String accountProper;


    private String transferId;

    @NotNull(message = "通知地址不能为空")
    private String notifyUrl;

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getOpenBankName() {
        return openBankName;
    }

    public void setOpenBankName(String openBankName) {
        this.openBankName = openBankName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAccountProper() {
        return accountProper;
    }

    public void setAccountProper(String accountProper) {
        this.accountProper = accountProper;
    }

    @Override
    public String toString() {
        return "TransferReqDto{" +
                "mchOrderNo='" + mchOrderNo + '\'' +
                ", bankName='" + bankName + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", accountName='" + accountName + '\'' +
                ", amount='" + amount + '\'' +
                ", openBankName='" + openBankName + '\'' +
                ", province='" + province + '\'' +
                ", cityName='" + cityName + '\'' +
                ", accountProper='" + accountProper + '\'' +
                '}';
    }
}
