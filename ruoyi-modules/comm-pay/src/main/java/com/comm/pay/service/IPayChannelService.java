package com.comm.pay.service;

import java.util.List;
import com.comm.pay.domain.PayChannel;

/**
 * 渠道信息Service接口
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
public interface IPayChannelService 
{
    /**
     * 查询渠道信息
     * 
     * @param id 渠道信息主键
     * @return 渠道信息
     */
    public PayChannel selectPayChannelById(Long id);

    /**
     * 查询渠道信息列表
     * 
     * @param payChannel 渠道信息
     * @return 渠道信息集合
     */
    public List<PayChannel> selectPayChannelList(PayChannel payChannel);

    /**
     * 新增渠道信息
     * 
     * @param payChannel 渠道信息
     * @return 结果
     */
    public int insertPayChannel(PayChannel payChannel);

    /**
     * 修改渠道信息
     * 
     * @param payChannel 渠道信息
     * @return 结果
     */
    public int updatePayChannel(PayChannel payChannel);

    /**
     * 批量删除渠道信息
     * 
     * @param ids 需要删除的渠道信息主键集合
     * @return 结果
     */
    public int deletePayChannelByIds(Long[] ids);

    /**
     * 删除渠道信息信息
     * 
     * @param id 渠道信息主键
     * @return 结果
     */
    public int deletePayChannelById(Long id);
}
