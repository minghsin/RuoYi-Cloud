package com.comm.pay.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayTypeMapper;
import com.comm.pay.domain.PayType;
import com.comm.pay.service.IPayTypeService;

/**
 * 支付类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
@Service
public class PayTypeServiceImpl implements IPayTypeService 
{
    @Autowired
    private PayTypeMapper payTypeMapper;

    /**
     * 查询支付类型
     * 
     * @param wayType 支付类型主键
     * @return 支付类型
     */
    @Override
    public PayType selectPayTypeByWayType(String wayType)
    {
        return payTypeMapper.selectPayTypeByWayType(wayType);
    }

    /**
     * 查询支付类型列表
     * 
     * @param payType 支付类型
     * @return 支付类型
     */
    @Override
    public List<PayType> selectPayTypeList(PayType payType)
    {
        return payTypeMapper.selectPayTypeList(payType);
    }

    /**
     * 新增支付类型
     * 
     * @param payType 支付类型
     * @return 结果
     */
    @Override
    public int insertPayType(PayType payType)
    {
        return payTypeMapper.insertPayType(payType);
    }

    /**
     * 修改支付类型
     * 
     * @param payType 支付类型
     * @return 结果
     */
    @Override
    public int updatePayType(PayType payType)
    {
        return payTypeMapper.updatePayType(payType);
    }

    /**
     * 批量删除支付类型
     * 
     * @param wayTypes 需要删除的支付类型主键
     * @return 结果
     */
    @Override
    public int deletePayTypeByWayTypes(String[] wayTypes)
    {
        return payTypeMapper.deletePayTypeByWayTypes(wayTypes);
    }

    /**
     * 删除支付类型信息
     * 
     * @param wayType 支付类型主键
     * @return 结果
     */
    @Override
    public int deletePayTypeByWayType(String wayType)
    {
        return payTypeMapper.deletePayTypeByWayType(wayType);
    }
}
