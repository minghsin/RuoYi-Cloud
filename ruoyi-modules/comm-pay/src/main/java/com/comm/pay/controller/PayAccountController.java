package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.collection.CollectionUtil;
import com.ruoyi.common.core.enums.AccountUidType;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayAccount;
import com.comm.pay.service.IPayAccountService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 账户信息Controller
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
@RestController
@RequestMapping("/account")
public class PayAccountController extends BaseController
{
    @Autowired
    private IPayAccountService payAccountService;


    @GetMapping("/getCurrentMchAccount")
    @RequiresPermissions("pay:account:query")
    public AjaxResult getCurrentMchAccount(){

        if(SecurityUtils.isAdmin(SecurityUtils.getUserId())){
            return error("管理员不允许为进行商户转账");
        }

        PayAccount queryAccount = new PayAccount();
        queryAccount.setUidType(AccountUidType.MCH_TYPE.getCode());
        List<PayAccount> payAccountList = payAccountService.selectPayAccountList(queryAccount);
        if(CollectionUtil.isEmpty(payAccountList)){
            return error("资金账户不存在，请稍后创建");
        }
        return AjaxResult.success(payAccountList.get(0));
    }

    /**
     * 查询账户信息列表
     */
    @RequiresPermissions("pay:account:list")
    @GetMapping("/list")
    public TableDataInfo list(PayAccount payAccount)
    {
        startPage();
        List<PayAccount> list = payAccountService.selectPayAccountList(payAccount);
        return getDataTable(list);
    }


    /**
     * 导出账户信息列表
     */
    @RequiresPermissions("pay:account:export")
    @Log(title = "账户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayAccount payAccount)
    {
        List<PayAccount> list = payAccountService.selectPayAccountList(payAccount);
        ExcelUtil<PayAccount> util = new ExcelUtil<PayAccount>(PayAccount.class);
        util.exportExcel(response, list, "账户信息数据");
    }

    /**
     * 获取账户信息详细信息
     */
    @RequiresPermissions("pay:account:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payAccountService.selectPayAccountById(id));
    }
}
