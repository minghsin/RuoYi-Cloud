package com.comm.pay.api.dto;

import java.math.BigDecimal;

public class CreateOrderReqDto extends BaseReqDto {




    /**
     * 商户订单号
     */
    private String mchOrderNo;

    /**
     * 订单金额, 单位分
     */
    private Long amount;

    /**
     * 支付编码
     */
    private String wayCode;

    /**
     * 货币编码，默认CNY
     */
    private String currency;

    /**
     * 客户端IP
     */
    private String clientIp;

    /**
     * 商品标题
     */
    private String subject;

    /**
     * 商品描述
     */
    private String body;

    /**
     * 通知地址
     */
    private String notifyUrl;

    /**
     * 跳转地址
     */
    private String returnUrl;

    /**
     * 签名类型 MD5, RSA 默认MD5
     */
    private String signType;

    private String sign;

    /**
     * 商户的用户ID
     */
    private String userid;


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getWayCode() {
        return wayCode;
    }

    public void setWayCode(String wayCode) {
        this.wayCode = wayCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    @Override
    public String toString() {
        return "CreateOrderReqDto{" +
                ", mchOrderNo='" + mchOrderNo + '\'' +
                ", amount=" + amount +
                ", wayCode='" + wayCode + '\'' +
                ", currency='" + currency + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", notifyUrl='" + notifyUrl + '\'' +
                ", returnUrl='" + returnUrl + '\'' +
                '}';
    }
}
