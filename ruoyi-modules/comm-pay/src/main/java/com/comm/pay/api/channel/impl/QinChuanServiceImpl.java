package com.comm.pay.api.channel.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.comm.pay.api.channel.AbstractPayChnlService;
import com.comm.pay.api.channel.dto.QinChuanCallbackDto;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.TransferOrder;
import com.ruoyi.common.core.enums.PayState;
import com.ruoyi.common.core.enums.TransferState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TreeMap;

public class QinChuanServiceImpl extends AbstractPayChnlService {

    private String callbackUrl = "http://www.baidu.com";
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public PayOrder createOrder(PayOrder payOrder) {

        try{
            TreeMap<String,Object> requestParam = new TreeMap<>();
            requestParam.put("partner",this.getConfig().getChnlMchNo());
            requestParam.put("type", payOrder.getChannelPayCode());
            requestParam.put("paymoney", payOrder.getRealAmount().toPlainString());
            requestParam.put("ordernumber", payOrder.getPayOrderId());
            requestParam.put("callbackurl", callbackUrl);

            String beforeSignStr = "partner="+ this.getConfig().getChnlMchNo()
                    +"&type="+ payOrder.getChannelOrderNo()
                    + "&paymoney=" + payOrder.getRealAmount().toPlainString()
                    +"&ordernumber="+ payOrder.getPayOrderId() +"&callbackurl="+callbackUrl + this.getConfig().getSignKey();

            String sign = MD5.create().digestHex(beforeSignStr);

            requestParam.put("sign",sign);
            logger.info("{}下单返回内容：{}", getConfig().getChnlName(),JSONUtil.toJsonPrettyStr(requestParam));
            String result = HttpUtil.post(this.getConfig().getCreateOrderUrl(), requestParam);
            logger.info("{}下单返回内容：{}",getConfig().getChnlName(),result);
            JSONObject resultObj = JSONObject.parseObject(result);
            if(resultObj.containsKey("success") && StrUtil.equals(resultObj.getString("success"),"true")){
                payOrder.setChannelPayUrl(resultObj.getString("url"));
                payOrder.setState(PayState.PAYING.getCode());
            }else{
                payOrder.setState(PayState.FAIL.getCode());
                payOrder.setErrCode(resultObj.getString("success"));
                payOrder.setErrMsg(resultObj.getString("respDesc"));
            }
        }catch (Exception e){
            payOrder.setState(PayState.FAIL.getCode());
            payOrder.setErrCode("SYSTEM_CREATE_ERROR");
            payOrder.setErrMsg(e.getMessage());
        }
        return payOrder;
    }

    @Override
    public PayOrder queryOrder(PayOrder payOrder) {
        return null;
    }

    @Override
    public PayOrder notityCallback(PayOrder payOrder,String notityStr) throws RuntimeException {

        QinChuanCallbackDto qinChuanCallbackDto = JSONUtil.toBean(notityStr, QinChuanCallbackDto.class);

        String beforeSignStr = "partner="+ qinChuanCallbackDto.getPartner()
                +"&ordernumber="+qinChuanCallbackDto.getOrdernumber()
                +"&paymoney="+qinChuanCallbackDto.getPaymoney()
                +"&attach="+qinChuanCallbackDto.getAttach()
                + this.getConfig().getSignKey();
        String sign = MD5.create().digestHex(beforeSignStr);
        if(!StrUtil.equals(sign,qinChuanCallbackDto.getSign())){
            throw new RuntimeException("签名错误");
        }

        //没有状态，签名通过之后默认成功
        payOrder.setState(PayState.SUCCESS.getCode());

        return payOrder;
    }


    @Override
    public TransferOrder createTransfer(TransferOrder transferOrder) {
        return null;
    }

    @Override
    public TransferOrder queryTransfer(TransferOrder transferOrder) {
        return null;
    }

    @Override
    public TransferOrder transferNotifyCallback(TransferOrder transferOrder, String notifyStr) {

        transferOrder.setState(TransferState.FAIL.getCode());

        return transferOrder;
    }
}
