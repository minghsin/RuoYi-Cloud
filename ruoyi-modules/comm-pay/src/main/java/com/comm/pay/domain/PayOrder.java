package com.comm.pay.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.utils.poi.AmountHandlerAdapter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 支付订单对象 t_pay_order
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
public class PayOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 支付订单号 */
    @Excel(name="系统订单号", sort = 6)
    private String payOrderId;

    /** 商户号 */
    @Excel(name = "商户号", sort = 0)
    private String mchNo;

    /** 渠道编号 */
    private String channelId;

    /** 渠道名称 */
    private String channelName;

    /** 应用ID */
    @Excel(name = "应用ID", sort = 3)
    private String appId;

    /** 商户名称 */
    @Excel(name = "商户名称",sort = 1)
    private String mchName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    @Excel(name = "商户类型", readConverterExp="1=普通商户,2=特约商户", sort = 2)
    private Long mchType;

    /** 商户订单号 */
    @Excel(name = "商户订单号",sort = 7)
    private String mchOrderNo;

    /** 支付接口代码 */
    private String ifCode;

    /** 支付方式代码 */
    @Excel(name = "支付代码",sort=4)
    private String wayCode;

    /**
     * 支付方式名称
     */
    @Excel(name = "支付名称",sort = 5)
    private String wayName;

    /** 支付金额,单位分 */
    private BigDecimal amount;

    /** 商户手续费费率快照 */
    @Excel(name = "商户手续费费率", sort = 8, suffix = "%")
    private BigDecimal mchFeeRate;

    /** 商户手续费,单位分 */
    @Excel(name = "商户手续费",handler = AmountHandlerAdapter.class,sort = 7)
    private BigDecimal mchFeeAmount;

    /** 三位货币代码,人民币:cny */
    private String currency;

    /** 支付状态: 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭 */
    @Excel(name = "支付状态", readConverterExp="0=订单生成,1=支付中,2=支付成功,3=支付失败,4=已撤销,5=已退款,6=订单关闭")
    private String state;

    /** 向下游回调状态, 0-未发送,  1-已发送 */
    @Excel(name = "回调状态",readConverterExp="0=未通知,1=发送中,2=发送成功,3=发送失败")
    private String notifyState;

    /** 客户端IP */
    @Excel(name = "客户端IP")
    private String clientIp;

    /** 商品标题 */
    @Excel(name = "商品标题")
    private String subject;

    /** 商品描述信息 */
    @Excel(name = "商品描述信息")
    private String body;

    /** 特定渠道发起额外参数 */
    private String channelExtra;

    /** 渠道用户标识,如微信openId,支付宝账号 */
    private String channelUser;

    /** 渠道订单号 */
    private String channelOrderNo;

    /** 退款状态: 0-未发生实际退款, 1-部分退款, 2-全额退款 */
    private Long refundState;

    /** 退款次数 */
    private Long refundTimes;

    /** 退款总金额,单位分 */
    private BigDecimal refundAmount;

    /** 订单分账模式：0-该笔订单不允许分账, 1-支付成功按配置自动完成分账, 2-商户手动分账(解冻商户金额) */
    private Long divisionMode;

    /** 订单分账状态：0-未发生分账, 1-等待分账任务处理, 2-分账处理中, 3-分账任务已结束(不体现状态) */
    private Long divisionState;

    /** 最新分账时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date divisionLastTime;

    /** 渠道支付错误码 */
    private String errCode;

    /** 渠道支付错误描述 */
    private String errMsg;

    /** 商户扩展参数 */
    private String extParam;

    /** 异步通知地址 */
    @Excel(name = "异步通知地址")
    private String notifyUrl;

    /** 页面跳转地址 */
    @Excel(name = "页面跳转地址")
    private String returnUrl;

    /** 订单失效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "订单失效时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date expiredTime;

    /** 订单支付成功时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "订单支付成功时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date successTime;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /** $column.columnComment */
    @Excel(name = "支付金额", sort = 6, handler = AmountHandlerAdapter.class)
    private BigDecimal realAmount;

    private String channelPayCode;

    private String channelPayUrl;

    public String getChannelPayUrl() {
        return channelPayUrl;
    }

    public void setChannelPayUrl(String channelPayUrl) {
        this.channelPayUrl = channelPayUrl;
    }

    public String getChannelPayCode() {
        return channelPayCode;
    }

    public void setChannelPayCode(String channelPayCode) {
        this.channelPayCode = channelPayCode;
    }

    public void setPayOrderId(String payOrderId)
    {
        this.payOrderId = payOrderId;
    }

    public String getPayOrderId() 
    {
        return payOrderId;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setChannelId(String channelId) 
    {
        this.channelId = channelId;
    }

    public String getChannelId() 
    {
        return channelId;
    }
    public void setChannelName(String channelName) 
    {
        this.channelName = channelName;
    }

    public String getChannelName() 
    {
        return channelName;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setMchType(Long mchType) 
    {
        this.mchType = mchType;
    }

    public Long getMchType() 
    {
        return mchType;
    }
    public void setMchOrderNo(String mchOrderNo) 
    {
        this.mchOrderNo = mchOrderNo;
    }

    public String getMchOrderNo() 
    {
        return mchOrderNo;
    }
    public void setIfCode(String ifCode) 
    {
        this.ifCode = ifCode;
    }

    public String getIfCode() 
    {
        return ifCode;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setMchFeeRate(BigDecimal mchFeeRate) 
    {
        this.mchFeeRate = mchFeeRate;
    }

    public BigDecimal getMchFeeRate() 
    {
        return mchFeeRate;
    }
    public void setCurrency(String currency) 
    {
        this.currency = currency;
    }

    public String getCurrency() 
    {
        return currency;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNotifyState() {
        return notifyState;
    }

    public void setNotifyState(String notifyState) {
        this.notifyState = notifyState;
    }
    public void setClientIp(String clientIp)
    {
        this.clientIp = clientIp;
    }

    public String getClientIp() 
    {
        return clientIp;
    }
    public void setSubject(String subject) 
    {
        this.subject = subject;
    }

    public String getSubject() 
    {
        return subject;
    }
    public void setBody(String body) 
    {
        this.body = body;
    }

    public String getBody() 
    {
        return body;
    }
    public void setChannelExtra(String channelExtra) 
    {
        this.channelExtra = channelExtra;
    }

    public String getChannelExtra() 
    {
        return channelExtra;
    }
    public void setChannelUser(String channelUser) 
    {
        this.channelUser = channelUser;
    }

    public String getChannelUser() 
    {
        return channelUser;
    }
    public void setChannelOrderNo(String channelOrderNo) 
    {
        this.channelOrderNo = channelOrderNo;
    }

    public String getChannelOrderNo() 
    {
        return channelOrderNo;
    }
    public void setRefundState(Long refundState) 
    {
        this.refundState = refundState;
    }

    public Long getRefundState() 
    {
        return refundState;
    }
    public void setRefundTimes(Long refundTimes) 
    {
        this.refundTimes = refundTimes;
    }

    public Long getRefundTimes() 
    {
        return refundTimes;
    }

    public void setDivisionMode(Long divisionMode) 
    {
        this.divisionMode = divisionMode;
    }

    public Long getDivisionMode() 
    {
        return divisionMode;
    }
    public void setDivisionState(Long divisionState) 
    {
        this.divisionState = divisionState;
    }

    public Long getDivisionState() 
    {
        return divisionState;
    }
    public void setDivisionLastTime(Date divisionLastTime) 
    {
        this.divisionLastTime = divisionLastTime;
    }

    public Date getDivisionLastTime() 
    {
        return divisionLastTime;
    }
    public void setErrCode(String errCode) 
    {
        this.errCode = errCode;
    }

    public String getErrCode() 
    {
        return errCode;
    }
    public void setErrMsg(String errMsg) 
    {
        this.errMsg = errMsg;
    }

    public String getErrMsg() 
    {
        return errMsg;
    }
    public void setExtParam(String extParam) 
    {
        this.extParam = extParam;
    }

    public String getExtParam() 
    {
        return extParam;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setReturnUrl(String returnUrl) 
    {
        this.returnUrl = returnUrl;
    }

    public String getReturnUrl() 
    {
        return returnUrl;
    }
    public void setExpiredTime(Date expiredTime) 
    {
        this.expiredTime = expiredTime;
    }

    public Date getExpiredTime() 
    {
        return expiredTime;
    }
    public void setSuccessTime(Date successTime) 
    {
        this.successTime = successTime;
    }

    public Date getSuccessTime() 
    {
        return successTime;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public BigDecimal getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(BigDecimal realAmount) {
        this.realAmount = realAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getMchFeeAmount() {
        return mchFeeAmount;
    }

    public void setMchFeeAmount(BigDecimal mchFeeAmount) {
        this.mchFeeAmount = mchFeeAmount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getWayName() {
        return wayName;
    }

    public void setWayName(String wayName) {
        this.wayName = wayName;
    }

    @Override
    public String toString() {
        return "PayOrder{" +
                "payOrderId='" + payOrderId + '\'' +
                ", mchNo='" + mchNo + '\'' +
                ", channelId='" + channelId + '\'' +
                ", channelName='" + channelName + '\'' +
                ", appId='" + appId + '\'' +
                ", mchName='" + mchName + '\'' +
                ", mchType=" + mchType +
                ", mchOrderNo='" + mchOrderNo + '\'' +
                ", ifCode='" + ifCode + '\'' +
                ", wayCode='" + wayCode + '\'' +
                ", amount=" + amount +
                ", mchFeeRate=" + mchFeeRate +
                ", mchFeeAmount=" + mchFeeAmount +
                ", currency='" + currency + '\'' +
                ", state=" + state +
                ", notifyState=" + notifyState +
                ", clientIp='" + clientIp + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", channelExtra='" + channelExtra + '\'' +
                ", channelUser='" + channelUser + '\'' +
                ", channelOrderNo='" + channelOrderNo + '\'' +
                ", refundState=" + refundState +
                ", refundTimes=" + refundTimes +
                ", refundAmount=" + refundAmount +
                ", divisionMode=" + divisionMode +
                ", divisionState=" + divisionState +
                ", divisionLastTime=" + divisionLastTime +
                ", errCode='" + errCode + '\'' +
                ", errMsg='" + errMsg + '\'' +
                ", extParam='" + extParam + '\'' +
                ", notifyUrl='" + notifyUrl + '\'' +
                ", returnUrl='" + returnUrl + '\'' +
                ", expiredTime=" + expiredTime +
                ", successTime=" + successTime +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", realAmount=" + realAmount +
                '}';
    }
}
