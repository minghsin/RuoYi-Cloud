package com.comm.pay.controller;

import java.math.BigDecimal;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayChannelWay;
import com.comm.pay.service.IPayChannelWayService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 渠道支付配置Controller
 * 
 * @author ruoyi
 * @date 2022-06-16
 */
@RestController
@RequestMapping("/config/way")
public class PayChannelWayController extends BaseController
{
    @Autowired
    private IPayChannelWayService payChannelWayService;



    /**
     * 查询渠道支付配置列表
     */
    @RequiresPermissions("pay:way:list")
    @GetMapping("/list")
    public TableDataInfo list(PayChannelWay payChannelWay)
    {
        startPage();
        List<PayChannelWay> list = payChannelWayService.selectPayChannelWayList(payChannelWay);
        return getDataTable(list);
    }

    /**
     * 导出渠道支付配置列表
     */
    @RequiresPermissions("pay:way:export")
    @Log(title = "渠道支付配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayChannelWay payChannelWay)
    {
        List<PayChannelWay> list = payChannelWayService.selectPayChannelWayList(payChannelWay);
        ExcelUtil<PayChannelWay> util = new ExcelUtil<PayChannelWay>(PayChannelWay.class);
        util.exportExcel(response, list, "渠道支付配置数据");
    }

    /**
     * 获取渠道支付配置详细信息
     */
    @RequiresPermissions("pay:way:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payChannelWayService.selectPayChannelWayById(id));
    }

    /**
     * 新增渠道支付配置
     */
    @RequiresPermissions("pay:way:add")
    @Log(title = "渠道支付配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayChannelWay payChannelWay)
    {
        // 金额转换
        payChannelWay.setMinAmount(payChannelWay.getMinAmount().multiply(new BigDecimal(100)));
        payChannelWay.setMaxAmount(payChannelWay.getMaxAmount().multiply(new BigDecimal(100)));
        return toAjax(payChannelWayService.insertPayChannelWay(payChannelWay));
    }

    /**
     * 修改渠道支付配置
     */
    @RequiresPermissions("pay:way:edit")
    @Log(title = "渠道支付配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayChannelWay payChannelWay)
    {
        // 金额转换
        payChannelWay.setMinAmount(payChannelWay.getMinAmount().multiply(new BigDecimal(100)));
        payChannelWay.setMaxAmount(payChannelWay.getMaxAmount().multiply(new BigDecimal(100)));
        return toAjax(payChannelWayService.updatePayChannelWay(payChannelWay));
    }

    /**
     * 删除渠道支付配置
     */
    @RequiresPermissions("pay:way:remove")
    @Log(title = "渠道支付配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payChannelWayService.deletePayChannelWayByIds(ids));
    }
}
