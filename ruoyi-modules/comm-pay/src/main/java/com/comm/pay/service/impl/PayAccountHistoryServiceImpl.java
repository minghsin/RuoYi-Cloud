package com.comm.pay.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.datascope.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayAccountHistoryMapper;
import com.comm.pay.domain.PayAccountHistory;
import com.comm.pay.service.IPayAccountHistoryService;

/**
 * 账户记录历史Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-13
 */
@Service
public class PayAccountHistoryServiceImpl implements IPayAccountHistoryService 
{
    @Autowired
    private PayAccountHistoryMapper payAccountHistoryMapper;

    /**
     * 查询账户记录历史
     * 
     * @param id 账户记录历史主键
     * @return 账户记录历史
     */
    @Override
    public PayAccountHistory selectPayAccountHistoryById(Long id)
    {
        return payAccountHistoryMapper.selectPayAccountHistoryById(id);
    }

    /**
     * 查询账户记录历史列表
     * 
     * @param payAccountHistory 账户记录历史
     * @return 账户记录历史
     */
    @Override
    @DataScope(dataType = "BUS")
    public List<PayAccountHistory> selectPayAccountHistoryList(PayAccountHistory payAccountHistory)
    {
        return payAccountHistoryMapper.selectPayAccountHistoryList(payAccountHistory);
    }

    /**
     * 新增账户记录历史
     * 
     * @param payAccountHistory 账户记录历史
     * @return 结果
     */
    @Override
    public int insertPayAccountHistory(PayAccountHistory payAccountHistory)
    {
        payAccountHistory.setCreateTime(DateUtils.getNowDate());
        return payAccountHistoryMapper.insertPayAccountHistory(payAccountHistory);
    }

    /**
     * 修改账户记录历史
     * 
     * @param payAccountHistory 账户记录历史
     * @return 结果
     */
    @Override
    public int updatePayAccountHistory(PayAccountHistory payAccountHistory)
    {
        payAccountHistory.setUpdateTime(DateUtils.getNowDate());
        return payAccountHistoryMapper.updatePayAccountHistory(payAccountHistory);
    }

    /**
     * 批量删除账户记录历史
     * 
     * @param ids 需要删除的账户记录历史主键
     * @return 结果
     */
    @Override
    public int deletePayAccountHistoryByIds(Long[] ids)
    {
        return payAccountHistoryMapper.deletePayAccountHistoryByIds(ids);
    }

    /**
     * 删除账户记录历史信息
     * 
     * @param id 账户记录历史主键
     * @return 结果
     */
    @Override
    public int deletePayAccountHistoryById(Long id)
    {
        return payAccountHistoryMapper.deletePayAccountHistoryById(id);
    }
}
