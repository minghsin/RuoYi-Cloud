package com.comm.pay.api.channel;

import com.comm.pay.domain.PayOrder;
import com.comm.pay.domain.TransferOrder;

/**
 * 支付渠道
 * auther ruoyi
 */
public interface IPayChnlService {


    /**
     * 创建渠道订单
     * @param payOrder
     * @return
     */
    public PayOrder createOrder(PayOrder payOrder);


    /**
     * 查询渠道订单
     * @param payOrder
     * @return
     */
    public PayOrder queryOrder(PayOrder payOrder);


    /**
     * 渠道回调处理
     * @param notityStr
     * @return
     */
    public PayOrder notityCallback(PayOrder payOrder,String notityStr);


    /**
     * 申请转账
     * @param transferOrder
     * @return
     */
    TransferOrder createTransfer(TransferOrder transferOrder);

    /**
     * 转账查询
     * @param transferOrder
     * @return
     */
    TransferOrder queryTransfer(TransferOrder transferOrder);

    /**
     * 转账回调接口
     * @param transferOrder
     * @param notifyStr
     * @return
     */
    TransferOrder transferNotifyCallback(TransferOrder transferOrder, String notifyStr);



}
