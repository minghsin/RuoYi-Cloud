package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 渠道信息对象 t_pay_channel
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
public class PayChannel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 渠道名称 */
    @Excel(name = "渠道名称")
    private String cnlName;

    /** 状态 01 开启 02 关闭 */
    @Excel(name = "状态 01 开启 02 关闭")
    private String state;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCnlName(String cnlName) 
    {
        this.cnlName = cnlName;
    }

    public String getCnlName() 
    {
        return cnlName;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cnlName", getCnlName())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
