package com.comm.pay.api.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.comm.merch.api.RemoteMerchAppService;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.domain.MchInfo;
import com.comm.pay.api.utils.CommonMd5Utils;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.redis.service.RedisService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.TreeMap;

public class ApiBaseController extends BaseController {

    @Autowired
    private RemoteMerchService merchService;

    @Autowired
    private RemoteMerchAppService merchAppService;

    /**
     * 验证签名
     * @return
     */
     public void validSign(Object params,MchInfo refMchInfo, MchApp refMchApp){



         TreeMap<String,Object> signMap = JSONUtil.toBean(JSONUtil.toJsonStr(params), TreeMap.class);

         String mchNo = signMap.get("mchNo")+"";
         String appId = signMap.get("appId")+"";

         R<MchInfo> result = merchService.getMchInfoByNo(mchNo, SecurityConstants.INNER);
         if(!result.ifSuccess()){
             logger.error("订单{}创建访问商户信息失败：{}", mchNo, result.getMsg());
             throw new RuntimeException("网络目前拥堵，请稍后重试");
         }
        MchInfo mchInfo = result.getData();
         if(mchInfo == null){
             throw new RuntimeException("商户不存在或已停用");
         }

         if(refMchInfo != null){
             BeanUtils.copyProperties(mchInfo, refMchInfo);
         }
         logger.info("请求的商户信息：" + result.getData());


         R<MchApp> appResult = merchAppService.getMchAppByAppid(appId, SecurityConstants.INNER);
         if(!appResult.ifSuccess()){
             logger.error("订单{}创建访问应用信息失败：{}", appId, result.getMsg());
             throw new RuntimeException("尝试访问商户应用，但网络目前拥堵，请稍后重试");
         }
         MchApp mchApp = appResult.getData();
         if(appResult.getData() == null){
             throw new RuntimeException("商户应用不存在或者已停用");
         }

         if(mchApp != null){
             BeanUtils.copyProperties(mchApp, refMchApp);

         }

         if(!StringUtils.equals(mchApp.getMchNo(), mchNo)){
             logger.error("商户{}与应用{}不匹配" ,mchNo, mchApp.getAppId());
             throw new RuntimeException("商户与应用不匹配");
         }

         String mchSign = (String)signMap.get("sign");
         signMap.remove("sign");
         String platformSign = CommonMd5Utils.signature(signMap,mchApp.getSecretMd5Key());
         System.out.println("platformSign = " + platformSign);
         boolean flag = StrUtil.equals(platformSign,mchSign);
         if(!flag){
             throw new RuntimeException("签名错误");
         }
     }

     public String createSign(String key, Object params){
         TreeMap<String,Object> signMap = JSONUtil.toBean(JSONUtil.toJsonStr(params), TreeMap.class);
         String platformSign = CommonMd5Utils.signature(signMap,key);
         return platformSign;
     }


}
