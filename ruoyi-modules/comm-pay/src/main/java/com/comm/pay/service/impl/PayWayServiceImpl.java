package com.comm.pay.service.impl;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayWayMapper;
import com.comm.pay.domain.PayWay;
import com.comm.pay.service.IPayWayService;

/**
 * 支付方式Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-02
 */
@Service
public class PayWayServiceImpl implements IPayWayService 
{
    @Autowired
    private PayWayMapper payWayMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询支付方式
     * 
     * @param wayId 支付方式主键
     * @return 支付方式
     */
    @Override
    public PayWay selectPayWayByWayId(Long wayId)
    {
        return payWayMapper.selectPayWayByWayId(wayId);
    }

    /**
     * 查询支付方式
     *
     * @param wayCode 支付方式主键
     * @return 支付方式
     */
    @Override
    public PayWay selectPayWayByWayCode(String wayCode)
    {
        PayWay payWay = redisService.getCacheObject(CacheConstants.WAY_CODE + wayCode);
        if(payWay == null){
            payWay = payWayMapper.selectPayWayByWayCode(wayCode);
            redisService.setCacheObject(CacheConstants.WAY_CODE + wayCode, payWay, CacheConstants.EXPIRATION, TimeUnit.MINUTES);
        }
        return payWay;
    }

    /**
     * 查询支付方式列表
     * 
     * @param payWay 支付方式
     * @return 支付方式
     */
    @Override
    public List<PayWay> selectPayWayList(PayWay payWay)
    {
        return payWayMapper.selectPayWayList(payWay);
    }

    /**
     * 新增支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    @Override
    public int insertPayWay(PayWay payWay)
    {
        return payWayMapper.insertPayWay(payWay);
    }

    /**
     * 修改支付方式
     * 
     * @param payWay 支付方式
     * @return 结果
     */
    @Override
    public int updatePayWay(PayWay payWay)
    {
        return payWayMapper.updatePayWay(payWay);
    }

    /**
     * 批量删除支付方式
     * 
     * @param wayIds 需要删除的支付方式主键
     * @return 结果
     */
    @Override
    public int deletePayWayByWayIds(Long[] wayIds)
    {
        return payWayMapper.deletePayWayByWayIds(wayIds);
    }

    /**
     * 删除支付方式信息
     * 
     * @param wayId 支付方式主键
     * @return 结果
     */
    @Override
    public int deletePayWayByWayId(Long wayId)
    {
        return payWayMapper.deletePayWayByWayId(wayId);
    }
}
