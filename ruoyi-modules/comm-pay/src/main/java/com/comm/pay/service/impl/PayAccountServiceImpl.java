package com.comm.pay.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.hutool.json.JSONUtil;
import com.comm.pay.domain.PayAccountHistory;
import com.comm.pay.mapper.PayAccountHistoryMapper;
import com.ruoyi.common.core.enums.AccountUidType;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.datascope.annotation.DataScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayAccountMapper;
import com.comm.pay.domain.PayAccount;
import com.comm.pay.service.IPayAccountService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 账户信息Service业务层处理
 *
 * @author ruoyi
 * @date 2022-06-13
 */
@Service
public class PayAccountServiceImpl implements IPayAccountService {
    private Logger log = LoggerFactory.getLogger(PayAccountServiceImpl.class);
    @Autowired
    private PayAccountMapper payAccountMapper;

    @Autowired
    private PayAccountHistoryMapper payAccountHistoryMapper;


    /**
     * 将代付中余额变更为已结清
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean subtractPayforToSettle(String accountUid, String accountType,
                                           String orderNo, String orderType, BigDecimal amount){

        PayAccount payAccount = payAccountMapper.selectPayAccountByAccountUid(accountUid, accountType);
        boolean balanceFlag = payAccountMapper.subtractPayforToSettle(payAccount.getId(), amount) > 0;
        boolean addHistoryFlag = false;
        if(balanceFlag){
            //记录操作历史
            PayAccountHistory history = new PayAccountHistory();
            history.setAccountId(payAccount.getId());
            history.setType("ADD");
            history.setOrderNo(orderNo);
            history.setOrderType(orderType);
            history.setAmount(amount);
            history.setCreateTime(new Date());
            history.setUpdateTime(new Date());
            addHistoryFlag = payAccountHistoryMapper.insertPayAccountHistory(history) > 0;
        }
        if(!balanceFlag || !addHistoryFlag){
            throw new RuntimeException("减少余额操作失败");
        }
        return Boolean.TRUE;
    }

    /**
     * 将代付中余额退回至可用余额
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean subtractPayforToBalance(String accountUid, String accountType,
                                           String orderNo, String orderType, BigDecimal amount){

        PayAccount payAccount = payAccountMapper.selectPayAccountByAccountUid(accountUid, accountType);
        boolean balanceFlag = payAccountMapper.subtractPayforToBalance(payAccount.getId(), amount) > 0;
        boolean addHistoryFlag = false;
        if(balanceFlag){
            //记录操作历史
            PayAccountHistory history = new PayAccountHistory();
            history.setAccountId(payAccount.getId());
            history.setType("ADD");
            history.setOrderNo(orderNo);
            history.setOrderType(orderType);
            history.setAmount(amount);
            history.setCreateTime(new Date());
            history.setUpdateTime(new Date());
            addHistoryFlag = payAccountHistoryMapper.insertPayAccountHistory(history) > 0;
        }
        if(!balanceFlag || !addHistoryFlag){
            throw new RuntimeException("减少余额操作失败");
        }
        return Boolean.TRUE;
    }

    /**
     * 减少余额到代付中余额
     * @param accountUid
     * @param accountType
     * @param orderNo
     * @param orderType
     * @param amount
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean subtractBalanceTopayfor(String accountUid, String accountType,
                          String orderNo, String orderType, BigDecimal amount){

        PayAccount payAccount = payAccountMapper.selectPayAccountByAccountUid(accountUid, accountType);
        boolean balanceFlag = payAccountMapper.subtractBalanceTopayfor(payAccount.getId(), amount) > 0;
        boolean addHistoryFlag = Boolean.FALSE;
        if(balanceFlag){
            //记录操作历史
            PayAccountHistory history = new PayAccountHistory();
            history.setAccountId(payAccount.getId());
            history.setType("SUB");
            history.setOrderNo(orderNo);
            history.setOrderType(orderType);
            history.setAmount(amount);
            history.setCreateTime(new Date());
            history.setUpdateTime(new Date());
            addHistoryFlag = payAccountHistoryMapper.insertPayAccountHistory(history) > 0;
        }
        if(!balanceFlag || !addHistoryFlag){
            throw new RuntimeException("减少余额操作失败");
        }
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addBalance(String accountUid, String accountType,
                                     String orderNo, String orderType, BigDecimal amount) {

        log.info("开始操作余额：订单号{} {} 增加余额 {}", orderNo, orderType, amount);

        PayAccount payAccount = payAccountMapper.selectPayAccountByAccountUid(accountUid, accountType);
        if(payAccount == null){
            payAccount = new PayAccount();
            payAccount.setAccountUid(accountUid);
            payAccount.setAccountName("");
            payAccount.setBalance(0L);
            payAccount.setUidType(AccountUidType.MCH_TYPE.getCode());
            payAccount.setSettleAmount(0L);
            payAccount.setWaitAmount(0L);
            payAccount.setFreezeAmount(0L);
            payAccount.setPayforAmount(0L);
            payAccountMapper.insertPayAccount(payAccount);
        }

        boolean addBalanceFlag = payAccountMapper.addBalance(payAccount.getId(), amount) > 0;
        boolean addHistoryFlag = Boolean.FALSE;
        if(addBalanceFlag){
            //记录操作历史
            PayAccountHistory history = new PayAccountHistory();
            history.setAccountId(payAccount.getId());
            history.setType("ADD");
            history.setOrderNo(orderNo);
            history.setOrderType(orderType);
            history.setAmount(amount);
            history.setCreateTime(new Date());
            history.setUpdateTime(new Date());
            addHistoryFlag = payAccountHistoryMapper.insertPayAccountHistory(history) > 0;
        }

        if(!addBalanceFlag || !addHistoryFlag){
            throw new RuntimeException("账务或者记录操作失败");
        }

        return Boolean.TRUE;
    }

    @Override
    public PayAccount selectPayAccountByAccountUid(String uid,String accountType){
        return payAccountMapper.selectPayAccountByAccountUid(uid, accountType);
    }

    /**
     * 查询账户信息
     *
     * @param id 账户信息主键
     * @return 账户信息
     */
    @Override
    public PayAccount selectPayAccountById(Long id) {
        return payAccountMapper.selectPayAccountById(id);
    }

    /**
     * 查询账户信息列表
     *
     * @param payAccount 账户信息
     * @return 账户信息
     */
    @Override
    @DataScope(dataType = "BUS")
    public List<PayAccount> selectPayAccountList(PayAccount payAccount) {
        return payAccountMapper.selectPayAccountList(payAccount);
    }

    /**
     * 新增账户信息
     *
     * @param payAccount 账户信息
     * @return 结果
     */
    @Override
    public int insertPayAccount(PayAccount payAccount) {
        payAccount.setCreateTime(DateUtils.getNowDate());
        return payAccountMapper.insertPayAccount(payAccount);
    }

    /**
     * 修改账户信息
     *
     * @param payAccount 账户信息
     * @return 结果
     */
    @Override
    public int updatePayAccount(PayAccount payAccount) {
        payAccount.setUpdateTime(DateUtils.getNowDate());
        return payAccountMapper.updatePayAccount(payAccount);
    }

    /**
     * 批量删除账户信息
     *
     * @param ids 需要删除的账户信息主键
     * @return 结果
     */
    @Override
    public int deletePayAccountByIds(Long[] ids) {
        return payAccountMapper.deletePayAccountByIds(ids);
    }

    /**
     * 删除账户信息信息
     *
     * @param id 账户信息主键
     * @return 结果
     */
    @Override
    public int deletePayAccountById(Long id) {
        return payAccountMapper.deletePayAccountById(id);
    }
}
