package com.comm.pay.controller;

import java.text.DateFormat;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayMchSettleBill;
import com.comm.pay.service.IPayMchSettleBillService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 商户对账单Controller
 * 
 * @author ruoyi
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/mchSettleBill")
public class PayMchSettleBillController extends BaseController
{
    @Autowired
    private IPayMchSettleBillService payMchSettleBillService;

    @RequiresPermissions("pay:mchSettleBill:list")
    @GetMapping("/gen/date")
    public AjaxResult genSettleBillByDate(String date){
        if(StringUtils.isEmpty(date)){
            date = DateUtil.yesterday().toString(DatePattern.NORM_DATE_PATTERN);
        }
        if(!payMchSettleBillService.createMchSettleBill(date)){
            return error("生成对账单失败");
        }
        return success();
    }

    /**
     * 查询商户对账单列表
     */
    @RequiresPermissions("pay:mchSettleBill:list")
    @GetMapping("/list")
    public TableDataInfo list(PayMchSettleBill payMchSettleBill)
    {
        startPage();
        List<PayMchSettleBill> list = payMchSettleBillService.selectPayMchSettleBillList(payMchSettleBill);
        return getDataTable(list);
    }

    /**
     * 导出商户对账单列表
     */
    @RequiresPermissions("pay:mchSettleBill:export")
    @Log(title = "商户对账单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayMchSettleBill payMchSettleBill)
    {
        List<PayMchSettleBill> list = payMchSettleBillService.selectPayMchSettleBillList(payMchSettleBill);
        ExcelUtil<PayMchSettleBill> util = new ExcelUtil<PayMchSettleBill>(PayMchSettleBill.class);
        util.exportExcel(response, list, "商户对账单数据");
    }

    /**
     * 获取商户对账单详细信息
     */
    @RequiresPermissions("pay:mchSettleBill:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payMchSettleBillService.selectPayMchSettleBillById(id));
    }

    /**
     * 新增商户对账单
     */
    @RequiresPermissions("pay:mchSettleBill:add")
    @Log(title = "商户对账单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayMchSettleBill payMchSettleBill)
    {
        return toAjax(payMchSettleBillService.insertPayMchSettleBill(payMchSettleBill));
    }

    /**
     * 修改商户对账单
     */
    @RequiresPermissions("pay:mchSettleBill:edit")
    @Log(title = "商户对账单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayMchSettleBill payMchSettleBill)
    {
        return toAjax(payMchSettleBillService.updatePayMchSettleBill(payMchSettleBill));
    }

    /**
     * 删除商户对账单
     */
    @RequiresPermissions("pay:mchSettleBill:remove")
    @Log(title = "商户对账单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payMchSettleBillService.deletePayMchSettleBillByIds(ids));
    }
}
