package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.comm.pay.api.channel.ChannelFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayChannelConfig;
import com.comm.pay.service.IPayChannelConfigService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 渠道配置Controller
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
@RestController
@RequestMapping("/config/base")
public class PayChannelConfigController extends BaseController
{
    @Autowired
    private IPayChannelConfigService payChannelConfigService;

    @Autowired
    private ChannelFactory channelFactory;


    /**
     * 设置渠道基础配置
     */
    @RequiresPermissions("pay:config:settleBaseChannel")
    @Log(title = "渠道配置", businessType = BusinessType.INSERT)
    @PostMapping("/settle")
    public AjaxResult settleChannel(@RequestBody PayChannelConfig payChannelConfig)
    {
        int flag;
        if(payChannelConfig.getId() == null){
            flag = payChannelConfigService.insertPayChannelConfig(payChannelConfig);
            if(flag > 0){
                channelFactory.registerChnlByConfig(payChannelConfig);
            }
        }else{
            flag =payChannelConfigService.updatePayChannelConfig(payChannelConfig);
            if(flag > 0){
                channelFactory.setChnlConfig(payChannelConfig);
            }
        }
        return toAjax(flag);
    }


    @RequiresPermissions("pay:config:byChnlId")
    @GetMapping(value = "/info/{chnlId}")
    public AjaxResult selectPayChnlConfigByChnlId(@PathVariable("chnlId") Long chnlId){
        return AjaxResult.success(payChannelConfigService.selectPayChnlConfigByChnlId(chnlId));
    }

    /**
     * 查询渠道配置列表
     */
    @RequiresPermissions("pay:config:list")
    @GetMapping("/list")
    public TableDataInfo list(PayChannelConfig payChannelConfig)
    {
        startPage();
        List<PayChannelConfig> list = payChannelConfigService.selectPayChannelConfigList(payChannelConfig);
        return getDataTable(list);
    }

    /**
     * 导出渠道配置列表
     */
    @RequiresPermissions("pay:config:export")
    @Log(title = "渠道配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayChannelConfig payChannelConfig)
    {
        List<PayChannelConfig> list = payChannelConfigService.selectPayChannelConfigList(payChannelConfig);
        ExcelUtil<PayChannelConfig> util = new ExcelUtil<PayChannelConfig>(PayChannelConfig.class);
        util.exportExcel(response, list, "渠道配置数据");
    }

    /**
     * 获取渠道配置详细信息
     */
    @RequiresPermissions("pay:config:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(payChannelConfigService.selectPayChannelConfigById(id));
    }

    /**
     * 新增渠道配置
     */
    @RequiresPermissions("pay:config:add")
    @Log(title = "渠道配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PayChannelConfig payChannelConfig)
    {
        return toAjax(payChannelConfigService.insertPayChannelConfig(payChannelConfig));
    }

    /**
     * 修改渠道配置
     */
    @RequiresPermissions("pay:config:edit")
    @Log(title = "渠道配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PayChannelConfig payChannelConfig)
    {
        return toAjax(payChannelConfigService.updatePayChannelConfig(payChannelConfig));
    }

    /**
     * 删除渠道配置
     */
    @RequiresPermissions("pay:config:remove")
    @Log(title = "渠道配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(payChannelConfigService.deletePayChannelConfigByIds(ids));
    }
}
