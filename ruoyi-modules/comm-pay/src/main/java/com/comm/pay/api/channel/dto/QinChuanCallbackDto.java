package com.comm.pay.api.channel.dto;

/**
 * @Description
 * @time 2020-10-11 10:04 PM
 */
public class QinChuanCallbackDto {

    private String partner;
    private String ordernumber;
    private String paymoney;
    private String attach;
    private String openid;
    private String sign;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getPaymoney() {
        return paymoney;
    }

    public void setPaymoney(String paymoney) {
        this.paymoney = paymoney;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        return "QinChuanCallbackDto{" +
                "partner='" + partner + '\'' +
                ", ordernumber='" + ordernumber + '\'' +
                ", paymoney='" + paymoney + '\'' +
                ", attach='" + attach + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
