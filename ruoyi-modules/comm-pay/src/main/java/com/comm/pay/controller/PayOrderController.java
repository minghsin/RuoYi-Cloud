package com.comm.pay.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import com.comm.pay.api.service.OrderNotifyServiceImpl;
import com.ruoyi.common.core.enums.PayState;
import com.ruoyi.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.comm.pay.domain.PayOrder;
import com.comm.pay.service.IPayOrderService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 支付订单Controller
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
@RestController
@RequestMapping("/order")
public class PayOrderController extends BaseController
{
    @Autowired
    private IPayOrderService payOrderService;

    @Autowired
    private OrderNotifyServiceImpl orderNotifyService;

    @Log(title = "订单重发通知", businessType = BusinessType.OTHER)
    @RequiresPermissions("pay:order:notifyMch")
    @GetMapping("/notifyMerch/{payOrderId}")
    public AjaxResult orderNotifyMch(@PathVariable("payOrderId") String payOrderId){
        boolean notifyFlag = orderNotifyService.notityMerchant(payOrderId);
        return notifyFlag ? success() : error("通知失败");
    }

    /**
     * 手动补单
     * @return
     */
    @Log(title = "订单手动补单", businessType = BusinessType.OTHER)
    @RequiresPermissions("pay:order:manualReplenishment")
    @PostMapping("/manualReplenishment")
    public AjaxResult manualReplenishment(@RequestBody PayOrder payOrder){

        PayOrder querOrder = payOrderService.selectPayOrderByPayOrderId(payOrder.getPayOrderId());
        if(StringUtils.equals(querOrder.getState(),PayState.SUCCESS.getCode())){
            return error("订单已支付成功，不允许重复操作");
        }

        if(payOrderService.manualReplenishment(querOrder)){
            return success();
        }
        return error("处理失败");
    }

    /**
     * 统计支付成功订单
     * @return
     */
    @RequiresPermissions("pay:order:data")
    @GetMapping("/statistic")
    public AjaxResult statisticOrders(PayOrder payOrder){
        return AjaxResult.success(payOrderService.statisticOrders(payOrder));
    }

    /**
     * 查询支付订单列表
     */
    @RequiresPermissions("pay:order:list")
    @GetMapping("/list")
    public TableDataInfo list(PayOrder payOrder)
    {
        startPage();
        List<PayOrder> list = payOrderService.selectPayOrderList(payOrder);
        return getDataTable(list);
    }

    /**
     * 导出支付订单列表
     */
    @RequiresPermissions("pay:order:export")
    @Log(title = "支付订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PayOrder payOrder)
    {
        List<PayOrder> list = payOrderService.selectPayOrderList(payOrder);
        ExcelUtil<PayOrder> util = new ExcelUtil<PayOrder>(PayOrder.class);
        util.exportExcel(response, list, "支付订单数据");
    }

    /**
     * 获取支付订单详细信息
     */
    @RequiresPermissions("pay:order:query")
    @GetMapping(value = "/{payOrderId}")
    public AjaxResult getInfo(@PathVariable("payOrderId") String payOrderId)
    {
        return AjaxResult.success(payOrderService.selectPayOrderByPayOrderId(payOrderId));
    }
}
