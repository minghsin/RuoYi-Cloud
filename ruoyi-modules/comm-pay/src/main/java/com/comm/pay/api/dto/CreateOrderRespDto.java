package com.comm.pay.api.dto;

public class CreateOrderRespDto extends CreateOrderReqDto {

    /**
     * 支付地址
     */
    private String payUrl;

    /**
     * 系统支付单号
     */
    private String payOrderId;

    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayUrl() {
        return payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl;
    }

    @Override
    public String toString() {
        return "CreateOrderRespDto{" +
                "payUrl='" + payUrl + '\'' +
                '}';
    }
}
