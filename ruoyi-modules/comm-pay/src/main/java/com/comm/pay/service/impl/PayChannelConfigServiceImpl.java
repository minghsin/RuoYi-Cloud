package com.comm.pay.service.impl;

import java.util.List;

import com.comm.pay.api.channel.ChannelFactory;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayChannelConfigMapper;
import com.comm.pay.domain.PayChannelConfig;
import com.comm.pay.service.IPayChannelConfigService;

/**
 * 渠道配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-15
 */
@Service
public class PayChannelConfigServiceImpl implements IPayChannelConfigService 
{
    @Autowired
    private PayChannelConfigMapper payChannelConfigMapper;

//    @Autowired
//    private ChannelFactory channelFactory;

    @Override
    public PayChannelConfig selectPayChnlConfigByChnlId(Long chnlId){
        return payChannelConfigMapper.selectPayChnlConfigByChnlId(chnlId);
    }

    /**
     * 查询渠道配置
     * 
     * @param id 渠道配置主键
     * @return 渠道配置
     */
    @Override
    public PayChannelConfig selectPayChannelConfigById(Long id)
    {
        return payChannelConfigMapper.selectPayChannelConfigById(id);
    }

    /**
     * 查询渠道配置列表
     * 
     * @param payChannelConfig 渠道配置
     * @return 渠道配置
     */
    @Override
    public List<PayChannelConfig> selectPayChannelConfigList(PayChannelConfig payChannelConfig)
    {
        return payChannelConfigMapper.selectPayChannelConfigList(payChannelConfig);
    }

    /**
     * 新增渠道配置
     * 
     * @param payChannelConfig 渠道配置
     * @return 结果
     */
    @Override
    public int insertPayChannelConfig(PayChannelConfig payChannelConfig)
    {
        payChannelConfig.setCreateTime(DateUtils.getNowDate());
        return payChannelConfigMapper.insertPayChannelConfig(payChannelConfig);
    }

    /**
     * 修改渠道配置
     * 
     * @param payChannelConfig 渠道配置
     * @return 结果
     */
    @Override
    public int updatePayChannelConfig(PayChannelConfig payChannelConfig)
    {
        payChannelConfig.setUpdateTime(DateUtils.getNowDate());
        int row = payChannelConfigMapper.updatePayChannelConfig(payChannelConfig) ;
        if(row > 0) {
//            channelFactory.setChnlConfig(payChannelConfig.getChannelId(), payChannelConfig);
        }
        return row;
    }

    /**
     * 批量删除渠道配置
     * 
     * @param ids 需要删除的渠道配置主键
     * @return 结果
     */
    @Override
    public int deletePayChannelConfigByIds(Long[] ids)
    {
        return payChannelConfigMapper.deletePayChannelConfigByIds(ids);
    }

    /**
     * 删除渠道配置信息
     * 
     * @param id 渠道配置主键
     * @return 结果
     */
    @Override
    public int deletePayChannelConfigById(Long id)
    {
        return payChannelConfigMapper.deletePayChannelConfigById(id);
    }
}
