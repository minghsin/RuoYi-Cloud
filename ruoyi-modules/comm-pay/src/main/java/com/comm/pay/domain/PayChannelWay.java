package com.comm.pay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 渠道支付配置对象 t_pay_channel_way
 * 
 * @author ruoyi
 * @date 2022-06-16
 */
public class PayChannelWay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 渠道编号 */
    @Excel(name = "渠道编号")
    private Long channelId;

    /** 支付编码 */
    @Excel(name = "支付编码")
    private String payType;

    /** 支付名称 */
    @Excel(name = "支付名称")
    private String payName;

    /** 系统支付编码 */
    @Excel(name = "系统支付编码")
    private String wayCode;

    /** 系统支付名称 */
    @Excel(name = "系统支付名称")
    private String wayName;

    /** 费率 */
    @Excel(name = "费率")
    private Long rate;

    /** 状态 Y/N */
    @Excel(name = "状态 Y/N")
    private String active;

    /** 最小支付金额 */
    @Excel(name = "最小支付金额")
    private BigDecimal minAmount;

    /** 最大支付金额 */
    @Excel(name = "最大支付金额")
    private BigDecimal maxAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setChannelId(Long channelId) 
    {
        this.channelId = channelId;
    }

    public Long getChannelId() 
    {
        return channelId;
    }
    public void setPayType(String payType) 
    {
        this.payType = payType;
    }

    public String getPayType() 
    {
        return payType;
    }
    public void setPayName(String payName) 
    {
        this.payName = payName;
    }

    public String getPayName() 
    {
        return payName;
    }
    public void setWayCode(String wayCode) 
    {
        this.wayCode = wayCode;
    }

    public String getWayCode() 
    {
        return wayCode;
    }
    public void setWayName(String wayName) 
    {
        this.wayName = wayName;
    }

    public String getWayName() 
    {
        return wayName;
    }
    public void setRate(Long rate) 
    {
        this.rate = rate;
    }

    public Long getRate() 
    {
        return rate;
    }
    public void setActive(String active) 
    {
        this.active = active;
    }

    public String getActive() 
    {
        return active;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("channelId", getChannelId())
            .append("payType", getPayType())
            .append("payName", getPayName())
            .append("wayCode", getWayCode())
            .append("wayName", getWayName())
            .append("rate", getRate())
            .append("active", getActive())
            .append("minAmount", getMinAmount())
            .append("maxAmount", getMaxAmount())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateTime", getUpdateTime())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
