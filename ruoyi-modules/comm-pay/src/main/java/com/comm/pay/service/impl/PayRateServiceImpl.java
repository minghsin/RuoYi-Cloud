package com.comm.pay.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.comm.pay.mapper.PayRateMapper;
import com.comm.pay.domain.PayRate;
import com.comm.pay.service.IPayRateService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商户支付费率Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-10
 */
@Service
public class PayRateServiceImpl implements IPayRateService 
{
    @Autowired
    private PayRateMapper payRateMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveMchRate(List<PayRate> payRateList){

        payRateList.forEach((item) -> {
            if(StrUtil.equals(item.getRateType(), Constants.RATE_FIX_TYPE)){
                item.setRate(item.getRate().multiply(new BigDecimal(100)));
            }
            if(item.getId() == null){
                item.setCreateBy(SecurityUtils.getUsername());
                item.setUpdateBy(SecurityUtils.getUsername());
                payRateMapper.insertPayRate(item);
            }else{
                item.setCreateAt(new Date());
                item.setUpdateAt(new Date());
                payRateMapper.updatePayRate(item);
            }
        });

        return true;
    }

    /**
     * 按商户号查询费率
     * @param mchNo
     * @return
     */
    @Override
    public List<PayRate> selectPayRateListByMchNo(String mchNo){
        return payRateMapper.selectPayRateListByMchNo(mchNo);
    }

    /**
     * 按商户号查询费率
     * @param mchNo
     * @return
     */
    @Override
    public PayRate selectPayRateListByMchNo(String mchNo, String wayCode){
        return payRateMapper.selectPayRateListByMchWayCode(mchNo,wayCode);
    }

    /**
     * 查询商户支付费率
     * 
     * @param id 商户支付费率主键
     * @return 商户支付费率
     */
    @Override
    public PayRate selectPayRateById(Long id)
    {
        return payRateMapper.selectPayRateById(id);
    }

    /**
     * 查询商户支付费率列表
     * 
     * @param payRate 商户支付费率
     * @return 商户支付费率
     */
    @Override
    public List<PayRate> selectPayRateList(PayRate payRate)
    {
        return payRateMapper.selectPayRateList(payRate);
    }

    /**
     * 新增商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    @Override
    public int insertPayRate(PayRate payRate)
    {
        return payRateMapper.insertPayRate(payRate);
    }

    /**
     * 修改商户支付费率
     * 
     * @param payRate 商户支付费率
     * @return 结果
     */
    @Override
    public int updatePayRate(PayRate payRate)
    {
        return payRateMapper.updatePayRate(payRate);
    }

    /**
     * 批量删除商户支付费率
     * 
     * @param ids 需要删除的商户支付费率主键
     * @return 结果
     */
    @Override
    public int deletePayRateByIds(Long[] ids)
    {
        return payRateMapper.deletePayRateByIds(ids);
    }

    /**
     * 删除商户支付费率信息
     * 
     * @param id 商户支付费率主键
     * @return 结果
     */
    @Override
    public int deletePayRateById(Long id)
    {
        return payRateMapper.deletePayRateById(id);
    }
}
