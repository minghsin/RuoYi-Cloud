package com.comm.pay;

import cn.hutool.core.io.IoUtil;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DownloadSettleFileTest {

    public static void main(String[] args) {
        Workbook wb = new SXSSFWorkbook(500);
        try{


            Sheet sheet = wb.createSheet();
            wb.setSheetName(0, "账单对账单");

            List<Object[]> sumTitle = new ArrayList<>();
            Object[] files = new Object[]{"测试1","测试2","测试3","测试4"};
            sumTitle.add(files);
//            createTitle(sheet,sumTitle);
            File file = new File("/Users/zhengmingxin/Downloads/ruoyi-2/test.xlsx");
            OutputStream outputStream = new FileOutputStream(file);
            try{
                wb.write(outputStream);
            }catch (Exception e){
                outputStream.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            IOUtils.closeQuietly(wb);
        }

    }

    public static void createTitle(Sheet sheet, List<Object[]> fields){

        Row titleRow = sheet.createRow(0);
        titleRow.setHeightInPoints(30);
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue("测试");
        sheet.addMergedRegion(new CellRangeAddress(titleRow.getRowNum(), titleRow.getRowNum(), titleRow.getRowNum(),
                fields.size() - 1));

    };

    /**
     * 创建写入数据到Sheet
     */
    public void writeSheet(Workbook wb, Sheet sheet, List<Object> list, int sheetSize)
    {
        // 取出一共有多少个sheet.
        int sheetNo = Math.max(1, (int) Math.ceil(list.size() * 1.0 / sheetSize));
        for (int index = 0; index < sheetNo; index++)
        {

            // 产生一行
            Row row = sheet.createRow(0);
            for(int i = 0 ; i < list.size(); i++){
                // 创建列
                Cell cell = row.createCell(i);
                // 写入列信息
                cell.setCellValue((String)list.get(i));

            }

        }
    }


}
