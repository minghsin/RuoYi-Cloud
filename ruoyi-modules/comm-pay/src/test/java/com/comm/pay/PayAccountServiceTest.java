package com.comm.pay;


import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.RandomUtil;
import com.comm.pay.service.IPayAccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest(classes = {CommPayApplication.class})
public class PayAccountServiceTest {

    @Autowired
    private IPayAccountService payAccountService;

    @Test
    public void testAddBalance(){

        String accountUid = "MCHVROALIC1";
        String accountType = "01";
        String orderType = "01";

        for(int j = 0 ; j < 10; j++){

            int index = j;
            ThreadUtil.execAsync(new Runnable() {
                @Override
                public void run() {

                    BigDecimal totalAmount = new BigDecimal(0);
                    for(int i = 0 ; i < 20; i ++){
                        String orderNo = RandomUtil.randomString(16);
                        BigDecimal randAmount =  RandomUtil.randomBigDecimal(new BigDecimal(10)).setScale(0,BigDecimal.ROUND_DOWN);
                        BigDecimal amount = new BigDecimal(String.valueOf(randAmount)).multiply(new BigDecimal(100));
                        try{
                            boolean flag = payAccountService.addBalance(accountUid,accountType,orderNo,orderType,amount);
                            if(flag){
                                totalAmount = totalAmount.add(amount);
                            }
                        }catch (Exception e){

                            e.printStackTrace();
                        }

                    }
                    System.out.println("线程"+index+"总金额：" + totalAmount.toPlainString());
                }
            });

        }


        try{
            System.out.println("线程已执行完成");
            Thread.sleep(60 * 2 * 1000);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
