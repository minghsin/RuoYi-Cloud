package com.comm.pay;

import com.comm.pay.rmq.producer.PayOrderNotityMchMQProducer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {CommPayApplication.class})
public class MqTest {

    @Autowired
    private PayOrderNotityMchMQProducer producer;

    @Test
    public void testSend(){

        producer.send("20220622203842gargl");

    }

}
