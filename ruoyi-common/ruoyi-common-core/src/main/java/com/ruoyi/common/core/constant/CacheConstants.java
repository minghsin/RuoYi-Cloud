package com.ruoyi.common.core.constant;

/**
 * 缓存的key 常量
 * 
 * @author ruoyi
 */
public class CacheConstants
{
    /**
     * 缓存有效期，默认720（分钟）
     */
    public final static long EXPIRATION = 720;

    /**
     * 缓存刷新时间，默认120（分钟）
     */
    public final static long REFRESH_TIME = 120;

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 商户应用信息
     */
    public final static String MCH_APP = "MCH_APP_";

    /**
     * 商户信息
     */
    public final static String MCH_INFO = "MCH_INFO_";

    /**
     * 支付编码
     */
    public final static String WAY_CODE = "WAY_CODE_";

    /**
     * 渠道所有可用支付方式
     */
    public final static String CHNL_ALL_PAYTYPE = "CHNL_ALL_PAYTYPE_";

}
