package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum AccountOrderType {

    PAY_ORDER("01", "支付订单"),
    WITH_ORDER("02", "提现订单"),
    REFUND_ORDER("03", "退款订单"),
    SYSTEM_ORDER("04", "系统调账订单"),
    WITH_SETTLE_ORDER("05", "转账结清"),
    WITH_FAIL_BACK_ORDER("06", "转账失败退回"),
    ;

    private final String code;
    private final String info;

    AccountOrderType(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
