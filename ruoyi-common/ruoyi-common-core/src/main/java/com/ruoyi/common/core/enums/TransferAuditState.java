package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum TransferAuditState {

    CREATED("01", "待审核"),
    PAYING("02", "审核中"),
    SUCCESS("03", "审核通过"),
    FAIL("04", "审核拒绝"),
    ;

    private final String code;
    private final String info;

    TransferAuditState(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
