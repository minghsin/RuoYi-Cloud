package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum PayState {

    CREATED("0", "订单创建"),
    PAYING("1", "支付中"),
    SUCCESS("2", "支付成功"),
    FAIL("3", "支付失败"),
    REVOKED("4", "已撤销"),
    REFUNDED ("5", "已退款"),
    CLOSE("6", "订单关闭");

    private final String code;
    private final String info;

    PayState(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
