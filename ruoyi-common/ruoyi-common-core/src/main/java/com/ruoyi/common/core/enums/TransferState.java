package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum TransferState {

    CREATED("0", "订单生成"),
    PAYING("1", "转账中"),
    SUCCESS("2", "转账成功"),
    FAIL("3", "转账失败"),
    REVOKED("4", "订单关闭")
    ;

    private final String code;
    private final String info;

    TransferState(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
