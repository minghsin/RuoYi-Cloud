package com.ruoyi.common.core.utils.poi;

import com.ruoyi.common.core.utils.StringUtils;

import java.math.BigDecimal;

public class AmountHandlerAdapter implements ExcelHandlerAdapter{

    @Override
    public Object format(Object value, String[] args) {

        if(StringUtils.isNotNull(value)){

            BigDecimal amount = new BigDecimal(String.valueOf(value));
            amount = amount.divide(new BigDecimal(100));
            return amount.stripTrailingZeros().toPlainString();
        }else{
            return "";
        }

    }
}
