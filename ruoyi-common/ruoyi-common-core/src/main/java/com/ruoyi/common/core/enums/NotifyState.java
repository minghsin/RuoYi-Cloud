package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum NotifyState {



    NO_SEND("0", "未发送"),
    SEND_ING("1", "发送中"),
    SUCCESS("2", "发送成功"),
    FAIL("3", "发送失败");

    private final String code;
    private final String info;

    NotifyState(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
