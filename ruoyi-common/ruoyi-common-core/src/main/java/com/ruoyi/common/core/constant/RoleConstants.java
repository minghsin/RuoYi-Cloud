package com.ruoyi.common.core.constant;

/**
 * 角色变量
 */
public class RoleConstants {

    /**
     * 超级管理员
     */
    public final static String ADMIN = "ADMIN";

    /**
     * 普通角色
     */
    public final static String COMMON = "common";

    /**
     * 商户管理员
     */
    public final static String MERCH_ADMIN = "merch_admin";

}
