package com.ruoyi.common.core.enums;

/**
 * 订单状态
 * @author ruoyi
 */
public enum AccountUidType {

    MCH_TYPE("01", "商户"),
    PAYING("02", "代理")


    ;

    private final String code;
    private final String info;

    AccountUidType(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
