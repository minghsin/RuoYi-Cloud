package com.comm.merch.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 商户列表对象 t_mch_info
 * 
 * @author test
 * @date 2022-05-29
 */
public class MchInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户号 */
    private String mchNo;

    private String mchName;

    private String mchShortName;

    /** 类型: 1-普通商户, 2-特约商户(服务商模式) */
    private Long type;

    /** 服务商号 */
    private String isvNo;

    /** 联系人姓名 */
    private String contactName;

    /** 联系人手机号 */
    private String contactTel;

    /** 联系人邮箱 */
    private String contactEmail;

    /** 商户状态: 0-停用, 1-正常 */
    private Long state;

    /** 初始用户ID（创建商户时，允许商户登录的用户） */
    private Long initUserId;
    /**
     * 初始化用户名称
     */
    private String initUserName;

    /** 创建者用户ID */
    private Long createdUid;

    /** 创建者姓名 */
    private String createdBy;

    /** 创建时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    /** 更新时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    /**
     * 所属部门
     */
    private Long deptId;

    /**
     * 所属部门名称
     */
    private String deptName;

    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setMchShortName(String mchShortName) 
    {
        this.mchShortName = mchShortName;
    }

    public String getMchShortName() 
    {
        return mchShortName;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setIsvNo(String isvNo) 
    {
        this.isvNo = isvNo;
    }

    public String getIsvNo() 
    {
        return isvNo;
    }
    public void setContactName(String contactName) 
    {
        this.contactName = contactName;
    }

    public String getContactName() 
    {
        return contactName;
    }
    public void setContactTel(String contactTel) 
    {
        this.contactTel = contactTel;
    }

    public String getContactTel() 
    {
        return contactTel;
    }
    public void setContactEmail(String contactEmail) 
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() 
    {
        return contactEmail;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }
    public void setInitUserId(Long initUserId) 
    {
        this.initUserId = initUserId;
    }

    public Long getInitUserId() 
    {
        return initUserId;
    }
    public void setCreatedUid(Long createdUid) 
    {
        this.createdUid = createdUid;
    }

    public Long getCreatedUid() 
    {
        return createdUid;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    public String getInitUserName() {
        return initUserName;
    }

    public void setInitUserName(String initUserName) {
        this.initUserName = initUserName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return "MchInfo{" +
                "mchNo='" + mchNo + '\'' +
                ", mchName='" + mchName + '\'' +
                ", mchShortName='" + mchShortName + '\'' +
                ", type=" + type +
                ", isvNo='" + isvNo + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactTel='" + contactTel + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", state=" + state +
                ", initUserId=" + initUserId +
                ", initUserName='" + initUserName + '\'' +
                ", createdUid=" + createdUid +
                ", createdBy='" + createdBy + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                '}';
    }
}
