package com.comm.merch.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 商户应用对象 t_mch_app
 * 
 * @author ruoyi
 * @date 2022-05-30
 */
public class MchApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 应用ID */
    private String appId;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String appName;

    /** 商户号 */
    @Excel(name = "商户号")
    private String mchNo;

    /** 应用状态 */
    @Excel(name = "应用状态")
    private Long state;

    /** 是否默认 */
    @Excel(name = "是否默认")
    private String appDefault;

    /** MD5签名 */
    @Excel(name = "MD5签名")
    private String secretMd5;

    /** MD5密钥 */
    @Excel(name = "MD5密钥")
    private String secretMd5Key;

    /** RSA加密 */
    @Excel(name = "RSA加密")
    private String secretRsa;

    /** RSA密钥 */
    @Excel(name = "RSA密钥")
    private String secretRsaKey;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String mchName;

    /** 创建者用户ID */
    @Excel(name = "创建者用户ID")
    private Long createdUid;

    /** 创建者姓名 */
    @Excel(name = "创建者姓名")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdAt;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedAt;

    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setAppName(String appName) 
    {
        this.appName = appName;
    }

    public String getAppName() 
    {
        return appName;
    }
    public void setMchNo(String mchNo) 
    {
        this.mchNo = mchNo;
    }

    public String getMchNo() 
    {
        return mchNo;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    public String getAppDefault() {
        return appDefault;
    }

    public void setAppDefault(String appDefault) {
        this.appDefault = appDefault;
    }

    public String getSecretMd5() {
        return secretMd5;
    }

    public void setSecretMd5(String secretMd5) {
        this.secretMd5 = secretMd5;
    }

    public void setSecretMd5Key(String secretMd5Key)
    {
        this.secretMd5Key = secretMd5Key;
    }

    public String getSecretMd5Key()
    {
        return secretMd5Key;
    }

    public String getSecretRsa() {
        return secretRsa;
    }

    public void setSecretRsa(String secretRsa) {
        this.secretRsa = secretRsa;
    }

    public void setSecretRsaKey(String secretRsaKey)
    {
        this.secretRsaKey = secretRsaKey;
    }

    public String getSecretRsaKey()
    {
        return secretRsaKey;
    }
    public void setMchName(String mchName) 
    {
        this.mchName = mchName;
    }

    public String getMchName() 
    {
        return mchName;
    }
    public void setCreatedUid(Long createdUid) 
    {
        this.createdUid = createdUid;
    }

    public Long getCreatedUid() 
    {
        return createdUid;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedAt(Date createdAt) 
    {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() 
    {
        return createdAt;
    }
    public void setUpdatedAt(Date updatedAt) 
    {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() 
    {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appId", getAppId())
            .append("appName", getAppName())
            .append("mchNo", getMchNo())
            .append("state", getState())
            .append("appDefault", getAppDefault())
            .append("secretMd5", getSecretMd5())
            .append("secretMd5Key", getSecretMd5Key())
            .append("secretRsa", getSecretRsa())
            .append("secretRsaKey", getSecretRsaKey())
            .append("mchName", getMchName())
            .append("remark", getRemark())
            .append("createdUid", getCreatedUid())
            .append("createdBy", getCreatedBy())
            .append("createdAt", getCreatedAt())
            .append("updatedAt", getUpdatedAt())
            .toString();
    }
}
