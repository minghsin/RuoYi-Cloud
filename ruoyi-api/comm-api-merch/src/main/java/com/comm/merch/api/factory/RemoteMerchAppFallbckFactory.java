package com.comm.merch.api.factory;


import com.comm.merch.api.RemoteMerchAppService;
import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.domain.MchInfo;
import com.ruoyi.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 商户应用服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteMerchAppFallbckFactory implements FallbackFactory<RemoteMerchAppService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteMerchAppFallbckFactory.class);


    @Override
    public RemoteMerchAppService create(Throwable throwable) {
        log.error("商户应用服务调用失败:{}", throwable.getMessage());
        return new RemoteMerchAppService() {
            @Override
            public R<MchApp> getMchAppByAppid(String merchNo, String source) {
                return R.fail("获取商户应用服务失败:" + throwable.getMessage());
            }
        };
    }

}
