package com.comm.merch.api;

import com.comm.merch.api.domain.MchInfo;
import com.comm.merch.api.factory.RemoteMerchFallbckFactory;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 商户服务
 * @author ruoyi
 */
@FeignClient(contextId = "remoteMerchService", value = ServiceNameConstants.MCH_SERVICE, fallbackFactory = RemoteMerchFallbckFactory.class)
public interface RemoteMerchService {

    /**
     * 查询商户信息
     *
     * @param merchNo 商户号
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/merch/base/{merchNo}")
    public R<MchInfo> getMchInfoByNo(@PathVariable("merchNo") String merchNo, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
