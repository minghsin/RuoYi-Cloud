package com.comm.merch.api;


import com.comm.merch.api.domain.MchApp;
import com.comm.merch.api.factory.RemoteMerchAppFallbckFactory;
import com.comm.merch.api.factory.RemoteMerchFallbckFactory;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * 商户应用服务
 * @author ruoyi
 */
@FeignClient(contextId = "remoteMerchAppService", value = ServiceNameConstants.MCH_SERVICE, fallbackFactory = RemoteMerchAppFallbckFactory.class)
public interface RemoteMerchAppService {


    /**
     * 查询商户信息
     *
     * @param appid 商户号
     * @param source 请求来源
     * @return 结果
     */
    @GetMapping("/app/innerAuth/{appid}")
    public R<MchApp> getMchAppByAppid(@PathVariable("appid") String appid, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
