package com.comm.merch.api.factory;

import com.comm.merch.api.RemoteMerchService;
import com.comm.merch.api.domain.MchInfo;
import com.ruoyi.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 商户服务降级处理
 *
 * @author ruoyi
 */
@Component
public class RemoteMerchFallbckFactory implements FallbackFactory<RemoteMerchService>  {

    private static final Logger log = LoggerFactory.getLogger(RemoteMerchFallbckFactory.class);


    @Override
    public RemoteMerchService create(Throwable throwable) {
        log.error("商户服务调用失败:{}", throwable.getMessage());
        return new RemoteMerchService() {
            @Override
            public R<MchInfo> getMchInfoByNo(String merchNo, String source) {
                return R.fail("获取商户失败:" + throwable.getMessage());
            }
        };
    }
}
